const JobtypeRoute = require("express").Router();
const { jobtype_validate } = require("../../validators");
const { jobtypeController } = require('../../controllers');

JobtypeRoute.post("/", jobtype_validate.create, jobtypeController.create);
JobtypeRoute.get("/", jobtype_validate.get, jobtypeController.get);
JobtypeRoute.get("/:jtype_id", jobtypeController.findByPk);
JobtypeRoute.put("/update/:jtype_id", jobtype_validate.update, jobtypeController.update);
JobtypeRoute.put("/updateStatus", jobtype_validate.updateStatus, jobtypeController.updateStatus);
JobtypeRoute.put("/updatePosition", jobtype_validate.updatePosition, jobtypeController.updatePosition);
JobtypeRoute.delete("/delete", jobtype_validate.delete, jobtypeController.delete);

module.exports = JobtypeRoute;