const EnquiryRoute = require("express").Router();
const { enquiry_validate } = require("../../validators");
const { enquiryController } = require('../../controllers');

EnquiryRoute.post("/", enquiry_validate.create, enquiryController.create);
EnquiryRoute.get("/", enquiry_validate.get, enquiryController.get);
EnquiryRoute.get("/:enq_id", enquiryController.findByPk);
EnquiryRoute.put("/update/:enq_id", enquiry_validate.update, enquiryController.update);
EnquiryRoute.delete("/delete", enquiry_validate.delete, enquiryController.delete);

module.exports = EnquiryRoute;