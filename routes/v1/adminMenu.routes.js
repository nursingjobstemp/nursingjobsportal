const AdminMenuRoute = require("express").Router()
const { adminMenuController } = require('../../controllers')
const { verifyToken } = require('../../middleware')
const { adminMenu_validate } = require('../../validators')

AdminMenuRoute.get("/", verifyToken.validateToken, adminMenuController.get)
AdminMenuRoute.get("/count", adminMenu_validate.getAllCount, adminMenuController.getAllCount)
AdminMenuRoute.get('/countByOp', verifyToken.validateToken, adminMenu_validate.getCountById, adminMenuController.getSpecOp)

module.exports = AdminMenuRoute