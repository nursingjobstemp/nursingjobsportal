const EmpofficialDetailRoute = require("express").Router();
const { empoffdetails_validate } = require("../../validators");
const { empoffdetailsController } = require('../../controllers');

EmpofficialDetailRoute.post("/", empoffdetails_validate.create, empoffdetailsController.create);
EmpofficialDetailRoute.get("/", empoffdetails_validate.get, empoffdetailsController.get);
EmpofficialDetailRoute.get("/:wrk_id", empoffdetailsController.findByPk);
EmpofficialDetailRoute.put("/update/:wrk_id", empoffdetails_validate.update, empoffdetailsController.update);
EmpofficialDetailRoute.put("/updateStatus", empoffdetails_validate.updateStatus, empoffdetailsController.updateStatus);
EmpofficialDetailRoute.delete("/delete", empoffdetails_validate.delete, empoffdetailsController.delete);

module.exports = EmpofficialDetailRoute;