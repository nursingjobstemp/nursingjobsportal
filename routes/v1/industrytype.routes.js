const IndustrytypeRoute = require("express").Router();
const { industrytype_validate } = require("../../validators");
const { industrytypeController } = require('../../controllers');

IndustrytypeRoute.post("/", industrytype_validate.create, industrytypeController.create);
IndustrytypeRoute.get("/", industrytype_validate.get, industrytypeController.get);
IndustrytypeRoute.get("/:indust_id", industrytypeController.findByPk);
IndustrytypeRoute.put("/update/:indust_id", industrytype_validate.update, industrytypeController.update);
IndustrytypeRoute.put("/updateStatus", industrytype_validate.updateStatus, industrytypeController.updateStatus);
IndustrytypeRoute.put("/updatePosition", industrytype_validate.updatePosition, industrytypeController.updatePosition);
IndustrytypeRoute.delete("/delete", industrytype_validate.delete, industrytypeController.delete);

module.exports = IndustrytypeRoute;