const push_notify_Routes = require('express').Router()
const { push_notifyController } = require('../../controllers')
const { push_notify_validate } = require('../../validators')

push_notify_Routes.post('/', push_notify_validate.create, push_notifyController.create)
push_notify_Routes.get('/', push_notify_validate.get, push_notifyController.get)
push_notify_Routes.post('/sendNotify', push_notify_validate.sendNotify, push_notifyController.sendNotify)
push_notify_Routes.get('/:notify_id', push_notify_validate.get, push_notifyController.findByPk)
push_notify_Routes.put('/update/:notify_id', push_notify_validate.update, push_notifyController.update)
push_notify_Routes.put('/updateStatus', push_notify_validate.updateStatus, push_notifyController.updateStatus)
push_notify_Routes.delete('/delete', push_notify_validate.delete, push_notifyController.delete)

module.exports = push_notify_Routes