const FestivalsRoute = require("express").Router();
const { festivals_validate } = require("../../validators");
const { festivalsController } = require('../../controllers');

FestivalsRoute.post("/", festivals_validate.create, festivalsController.create);
FestivalsRoute.get("/", festivals_validate.get, festivalsController.get);
FestivalsRoute.get("/:fest_id", festivalsController.findByPk);
FestivalsRoute.put("/update/:fest_id", festivals_validate.update, festivalsController.update);
FestivalsRoute.put("/updateStatus", festivals_validate.updateStatus, festivalsController.updateStatus);
FestivalsRoute.put("/updatePosition", festivals_validate.updatePosition, festivalsController.updatePosition);
FestivalsRoute.delete("/delete", festivals_validate.delete, festivalsController.delete);

module.exports = FestivalsRoute;