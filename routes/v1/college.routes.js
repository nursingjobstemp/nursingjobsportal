const CollegeRoute = require("express").Router();
const { college_validate } = require("../../validators");
const { collegeController } = require('../../controllers');

CollegeRoute.post("/", college_validate.create, collegeController.create);
CollegeRoute.get("/", college_validate.get, collegeController.get);
CollegeRoute.get("/:colg_id", collegeController.findByPk);
CollegeRoute.put("/update/:colg_id", college_validate.update, collegeController.update);
CollegeRoute.put("/updateStatus", college_validate.updateStatus, collegeController.updateStatus);
CollegeRoute.put("/updatePosition", college_validate.updatePosition, collegeController.updatePosition);
CollegeRoute.delete("/delete", college_validate.delete, collegeController.delete);

module.exports = CollegeRoute
