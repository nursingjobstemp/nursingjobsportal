const NotificationRoute = require("express").Router();
const { notification_validate } = require("../../validators");
const { notificationController } = require('../../controllers');

NotificationRoute.post("/", notification_validate.create, notificationController.create);
NotificationRoute.get("/", notification_validate.get, notificationController.get);
NotificationRoute.get("/:notify_id", notificationController.findByPk);
NotificationRoute.put("/update/:notify_id", notification_validate.update, notificationController.update);
NotificationRoute.put("/updateStatus", notification_validate.updateStatus, notificationController.updateStatus);
NotificationRoute.delete("/delete", notification_validate.delete, notificationController.delete);

module.exports = NotificationRoute;