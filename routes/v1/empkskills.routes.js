const EmpkskilssRoute = require("express").Router();
const { empkskills_validate } = require("../../validators");
const { empkskillsController } = require('../../controllers');

EmpkskilssRoute.post("/", empkskills_validate.create, empkskillsController.create);
EmpkskilssRoute.get("/", empkskills_validate.get, empkskillsController.get);
EmpkskilssRoute.get("/:empkskil_id", empkskillsController.findByPk);
EmpkskilssRoute.put("/update/:empkskil_id", empkskills_validate.update, empkskillsController.update);
EmpkskilssRoute.put("/updateStatus", empkskills_validate.updateStatus, empkskillsController.updateStatus);
EmpkskilssRoute.delete("/delete", empkskills_validate.delete, empkskillsController.delete);

module.exports = EmpkskilssRoute;