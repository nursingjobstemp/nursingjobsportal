const KeyskillsRoute = require("express").Router();
const { keyskills_validate } = require("../../validators");
const { keyskillsController } = require('../../controllers');

KeyskillsRoute.post("/", keyskills_validate.create, keyskillsController.create);
KeyskillsRoute.get("/", keyskills_validate.get, keyskillsController.get);
KeyskillsRoute.get("/:keysk_id", keyskillsController.findByPk);
KeyskillsRoute.put("/update/:keysk_id", keyskills_validate.update, keyskillsController.update);
KeyskillsRoute.put("/updateStatus", keyskills_validate.updateStatus, keyskillsController.updateStatus);
KeyskillsRoute.put("/updatePosition", keyskills_validate.updatePosition, keyskillsController.updatePosition);
KeyskillsRoute.delete("/delete", keyskills_validate.delete, keyskillsController.delete);

module.exports = KeyskillsRoute



