const ResumescoreRoute = require("express").Router();
const { resumescore_validate } = require("../../validators");
const { resumescoreController } = require('../../controllers');

ResumescoreRoute.post("/", resumescore_validate.create, resumescoreController.create);
ResumescoreRoute.get("/", resumescore_validate.get, resumescoreController.get);
ResumescoreRoute.get("/:resume_id", resumescoreController.findByPk);
ResumescoreRoute.put("/update/:resume_id", resumescore_validate.update, resumescoreController.update);
ResumescoreRoute.delete("/delete", resumescore_validate.delete, resumescoreController.delete);

module.exports = ResumescoreRoute;