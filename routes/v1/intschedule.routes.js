const IntscheduleRoute = require("express").Router();
const { intschedule_validate } = require("../../validators");
const { intscheduleController } = require('../../controllers');

IntscheduleRoute.post("/", intschedule_validate.create, intscheduleController.create);
IntscheduleRoute.get("/", intschedule_validate.get, intscheduleController.get);
IntscheduleRoute.get("/:intsch_id", intscheduleController.findByPk);
IntscheduleRoute.put("/update/:intsch_id", intschedule_validate.update, intscheduleController.update);
IntscheduleRoute.put("/updateStatus", intschedule_validate.updateStatus, intscheduleController.updateStatus);
IntscheduleRoute.delete("/delete", intschedule_validate.delete, intscheduleController.delete);

module.exports = IntscheduleRoute;