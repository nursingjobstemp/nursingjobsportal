const SalaryRoute = require("express").Router();
const { salary_validate } = require("../../validators");
const { salaryController } = require('../../controllers');

SalaryRoute.post("/", salary_validate.create, salaryController.create);
SalaryRoute.get("/", salary_validate.get, salaryController.get);
SalaryRoute.get("/:sal_id", salaryController.findByPk);
SalaryRoute.put("/update/:sal_id", salary_validate.update, salaryController.update);
SalaryRoute.put("/updateStatus", salary_validate.updateStatus, salaryController.updateStatus);
SalaryRoute.put("/updatePosition", salary_validate.updatePosition, salaryController.updatePosition);
SalaryRoute.delete("/delete", salary_validate.delete, salaryController.delete);

module.exports = SalaryRoute;