const OperatorRoute = require("express").Router()
const { operator_validate } = require("../../validators")
const { operatorController } = require('../../controllers')

OperatorRoute.post("/", operator_validate.create, operatorController.create)
OperatorRoute.get("/", operator_validate.get, operatorController.get)
OperatorRoute.get("/data", operator_validate.data, operatorController.data)
OperatorRoute.get("/:op_id", operatorController.findByPk)
OperatorRoute.put("/update/:op_id", operator_validate.update, operatorController.update)
OperatorRoute.put("/updateStatus", operator_validate.updateStatus, operatorController.updateStatus)
OperatorRoute.put("/changePassword", operator_validate.changePassword, operatorController.changePassword)
OperatorRoute.delete("/delete", operator_validate.delete, operatorController.delete)

module.exports = OperatorRoute