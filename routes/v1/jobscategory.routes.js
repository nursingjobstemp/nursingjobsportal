const JobscategoryRoute = require("express").Router();
const { jobcategory_validate } = require("../../validators");
const { jobscategoryController } = require('../../controllers');

JobscategoryRoute.post("/", jobcategory_validate.create, jobscategoryController.create);
JobscategoryRoute.get("/", jobcategory_validate.get, jobscategoryController.get);
JobscategoryRoute.get("/:jcat_id", jobscategoryController.findByPk);
JobscategoryRoute.put("/update/:jcat_id", jobcategory_validate.update, jobscategoryController.update);
JobscategoryRoute.put("/updateStatus", jobcategory_validate.updateStatus, jobscategoryController.updateStatus);
JobscategoryRoute.put("/updatePosition", jobcategory_validate.updatePosition, jobscategoryController.updatePosition);
JobscategoryRoute.delete("/delete", jobcategory_validate.delete, jobscategoryController.delete);

module.exports = JobscategoryRoute;