const express = require("express");
const seekerRoutes = express.Router();
const { seekerController } = require("../../controllers");
const { verifyToken } = require("../../middleware");
const { seeker_validate } = require("../../validators");

seekerRoutes.post("/", verifyToken.validateToken,seeker_validate.create, seekerController.create);
seekerRoutes.get("/", seeker_validate.get, seekerController.get);
seekerRoutes.get("/:emp_id",verifyToken.validateToken, seekerController.findByPk);
seekerRoutes.put("/update/:emp_id",verifyToken.validateToken, seeker_validate.update, seekerController.update);
seekerRoutes.put("/updateStatus",verifyToken.validateToken, seeker_validate.updateStatus, seekerController.updateStatus);
seekerRoutes.delete("/delete",verifyToken.validateToken, seeker_validate.delete, seekerController.delete);

module.exports = seekerRoutes;
