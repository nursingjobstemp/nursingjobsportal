const FileRoute = require("express").Router();
const multer = require("multer");

const { fileController } = require('../../controllers');
let { RESUME_FOLDER_PATH } = process.env;

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, '/tmp');
    },
    filename: (req, file, callBack) => {
        callBack(null, `file-${Date.now()}${file.originalname}`);
    },
});

const upload = multer({
    storage: storage
});

const resumeStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, RESUME_FOLDER_PATH);
    },
    filename: (req, file, callBack) => {
        callBack(null, file.originalname);
    },
});

const resumeUpload = multer({
    storage: resumeStorage
});

FileRoute.post("/resume", resumeUpload.single("file"), fileController.resume);
FileRoute.post("/", upload.single("file"), fileController.create);
FileRoute.post("/delete", fileController.delete);

module.exports = FileRoute;