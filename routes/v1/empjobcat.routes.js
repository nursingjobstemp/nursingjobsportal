const EmpjobcatRoute = require("express").Router();
const { empjobcat_validate } = require("../../validators");
const { empjobcatController } = require('../../controllers');

EmpjobcatRoute.post("/", empjobcat_validate.create, empjobcatController.create);
EmpjobcatRoute.get("/", empjobcat_validate.get, empjobcatController.get);
EmpjobcatRoute.get("/:mjcat_id", empjobcatController.findByPk);
EmpjobcatRoute.put("/update/:mjcat_id", empjobcat_validate.update, empjobcatController.update);
EmpjobcatRoute.put("/updateStatus", empjobcat_validate.updateStatus, empjobcatController.updateStatus);
EmpjobcatRoute.delete("/delete", empjobcat_validate.delete, empjobcatController.delete);

module.exports = EmpjobcatRoute;