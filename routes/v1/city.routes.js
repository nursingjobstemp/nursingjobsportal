const CityRoute = require("express").Router();
const { city_validate } = require("../../validators");
const { cityController } = require('../../controllers');

CityRoute.post("/", city_validate.create, cityController.create);
CityRoute.get("/", city_validate.get, cityController.get);
CityRoute.get("/:city_id", cityController.findByPk);
CityRoute.put("/update/:city_id", city_validate.update, cityController.update);
CityRoute.put("/updateStatus", city_validate.updateStatus, cityController.updateStatus);
CityRoute.delete("/delete", city_validate.delete, cityController.delete);

module.exports = CityRoute;