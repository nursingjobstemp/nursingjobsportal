const ContactResumeRoutes = require("express").Router();
const { contactresume_validate } = require("../../validators");
const { contactresumeController } = require('../../controllers');

ContactResumeRoutes.post("/", contactresume_validate.create, contactresumeController.create);
ContactResumeRoutes.get("/", contactresume_validate.get, contactresumeController.get);
ContactResumeRoutes.get("/:cont_id", contactresumeController.findByPk);
ContactResumeRoutes.put("/update/:cont_id", contactresume_validate.update, contactresumeController.update);
ContactResumeRoutes.delete("/delete", contactresume_validate.delete, contactresumeController.delete);

module.exports = ContactResumeRoutes;