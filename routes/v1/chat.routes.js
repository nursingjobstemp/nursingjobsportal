const ChatRoute = require("express").Router();
const { chat_validate } = require("../../validators");
const { chatController } = require('../../controllers');

ChatRoute.post("/", chat_validate.create, chatController.create);
ChatRoute.get("/", chat_validate.get, chatController.get);
ChatRoute.get("/:chat_id", chatController.findByPk);
ChatRoute.put("/update/:chat_id", chat_validate.update, chatController.update);
ChatRoute.put("/updateStatus", chat_validate.updateStatus, chatController.updateStatus);
ChatRoute.delete("/delete", chat_validate.delete, chatController.delete);

module.exports = ChatRoute;