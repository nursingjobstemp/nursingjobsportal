const Edu_courseRoute = require("express").Router()
const { edu_course_validate } = require("../../validators")
const { edu_courseController } = require('../../controllers')

Edu_courseRoute.post("/", edu_course_validate.create, edu_courseController.create)
Edu_courseRoute.get("/", edu_course_validate.get, edu_courseController.get)
Edu_courseRoute.get("/mast", edu_course_validate.getMastcat, edu_courseController.getMastcat)
Edu_courseRoute.get("/main", edu_course_validate.getMaincat, edu_courseController.getMaincat)
Edu_courseRoute.get("/sub", edu_course_validate.getSubcat, edu_courseController.getSubcat)
Edu_courseRoute.get("/:ecat_id", edu_courseController.findByPk)
Edu_courseRoute.put("/update/:ecat_id", edu_course_validate.update, edu_courseController.update)
Edu_courseRoute.put("/updateStatus", edu_course_validate.updateStatus, edu_courseController.updateStatus)
Edu_courseRoute.put("/updatePosition", edu_course_validate.updatePosition, edu_courseController.updatePosition)
Edu_courseRoute.delete("/delete", edu_course_validate.delete, edu_courseController.delete)

module.exports = Edu_courseRoute