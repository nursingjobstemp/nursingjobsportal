const EmpedudetailRoute = require("express").Router();
const { empedudetail_validate } = require("../../validators");
const { empedudetailController } = require('../../controllers');

EmpedudetailRoute.post("/", empedudetail_validate.create, empedudetailController.create);
EmpedudetailRoute.get("/", empedudetail_validate.get, empedudetailController.get);
EmpedudetailRoute.get("/:edu_id", empedudetailController.findByPk);
EmpedudetailRoute.put("/update/:edu_id", empedudetail_validate.update, empedudetailController.update);
EmpedudetailRoute.delete("/delete", empedudetail_validate.delete, empedudetailController.delete);

module.exports = EmpedudetailRoute;