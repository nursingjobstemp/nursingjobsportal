const express = require("express");
const employerRoutes = express.Router();
const { employerController } = require("../../controllers");
const { employer_validate } = require("../../validators");

employerRoutes.post("/", employer_validate.create, employerController.create);
employerRoutes.get("/", employer_validate.get, employerController.get);
employerRoutes.get("/:recut_id", employerController.findByPk);
employerRoutes.put("/update/:recut_id", employer_validate.update, employerController.update);
employerRoutes.put("/updateStatus", employer_validate.updateStatus, employerController.updateStatus);
employerRoutes.delete("/delete", employer_validate.delete, employerController.delete);

module.exports = employerRoutes;