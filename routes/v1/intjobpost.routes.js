const Intjobpost = require("express").Router();
const { intjobpost_validate } = require("../../validators");
const { intjobpostController } = require('../../controllers');

Intjobpost.post("/", intjobpost_validate.create, intjobpostController.create);
Intjobpost.post("/duplicate", intjobpost_validate.createDuplicate, intjobpostController.createDuplicate);
Intjobpost.get("/", intjobpost_validate.get, intjobpostController.get);
Intjobpost.get("/getCompany", intjobpost_validate.getCompanyJobs, intjobpostController.getCompanyJobs);
Intjobpost.get("/expJob", intjobpost_validate.getExpJob, intjobpostController.getExpJob);
Intjobpost.get("/duplicateJob", intjobpost_validate.getDuplicateJob, intjobpostController.getDuplicateJob);
Intjobpost.get("/:unrst_jid", intjobpostController.findByPk);
Intjobpost.put("/update/:unrst_jid", intjobpost_validate.update, intjobpostController.update);
Intjobpost.put("/updateStatus", intjobpost_validate.updateStatus, intjobpostController.updateStatus);
Intjobpost.put("/updatePosition", intjobpost_validate.updatePosition, intjobpostController.updatePosition);
Intjobpost.delete("/delete", intjobpost_validate.delete, intjobpostController.delete);

module.exports = Intjobpost;