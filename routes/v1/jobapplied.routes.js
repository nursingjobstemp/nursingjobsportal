const applied_idRoute = require("express").Router();
const { jobapplied_validate } = require("../../validators");
const { jobappliedController } = require('../../controllers');

applied_idRoute.post("/", jobapplied_validate.create, jobappliedController.create);
applied_idRoute.get("/", jobapplied_validate.get, jobappliedController.get);
applied_idRoute.get("/:applied_id", jobappliedController.findByPk);
applied_idRoute.put("/update/:applied_id", jobapplied_validate.update, jobappliedController.update);
applied_idRoute.put("/updateStatus", jobapplied_validate.updateStatus, jobappliedController.updateStatus);
applied_idRoute.delete("/delete", jobapplied_validate.delete, jobappliedController.delete);

module.exports = applied_idRoute;