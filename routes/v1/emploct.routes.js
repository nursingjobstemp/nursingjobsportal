const EmploctRoute = require("express").Router();
const { emploct_validate } = require("../../validators");
const { emploctController } = require('../../controllers');

EmploctRoute.post("/", emploct_validate.create, emploctController.create);
EmploctRoute.get("/", emploct_validate.get, emploctController.get);
EmploctRoute.get("/:emplocat_id", emploctController.findByPk);
EmploctRoute.put("/update/:emplocat_id", emploct_validate.update, emploctController.update);
EmploctRoute.put("/updateStatus", emploct_validate.updateStatus, emploctController.updateStatus);
EmploctRoute.delete("/delete", emploct_validate.delete, emploctController.delete);

module.exports = EmploctRoute;