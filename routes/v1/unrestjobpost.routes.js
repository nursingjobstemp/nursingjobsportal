const UnrestjobpostRoute = require("express").Router()
const { unrestjobpost_validate } = require("../../validators")
const { unrestjobpost } = require('../../controllers')

UnrestjobpostRoute.post("/", unrestjobpost_validate.create, unrestjobpost.create)
UnrestjobpostRoute.post("/duplicate", unrestjobpost_validate.createDuplicate, unrestjobpost.createDuplicate)
UnrestjobpostRoute.get("/", unrestjobpost_validate.get, unrestjobpost.get)
UnrestjobpostRoute.get("/getCompany", unrestjobpost_validate.getCompanyJobs, unrestjobpost.getCompanyJobs)
UnrestjobpostRoute.get("/expJob", unrestjobpost_validate.getExpJob, unrestjobpost.getExpJob)
UnrestjobpostRoute.get("/duplicateJob", unrestjobpost_validate.getDuplicateJob, unrestjobpost.getDuplicateJob)
UnrestjobpostRoute.get("/:unrst_jid", unrestjobpost.findByPk)
UnrestjobpostRoute.put("/update/:unrst_jid", unrestjobpost_validate.update, unrestjobpost.update)
UnrestjobpostRoute.put("/updateStatus", unrestjobpost_validate.updateStatus, unrestjobpost.updateStatus)
UnrestjobpostRoute.delete("/delete", unrestjobpost_validate.delete, unrestjobpost.delete)

module.exports = UnrestjobpostRoute