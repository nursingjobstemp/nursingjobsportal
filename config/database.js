"use strict";

const { logger } = require('../helper');
const dbConfig = require("../config/db.config")();
const path = require('path');
const fs = require('fs');

class Database {

  constructor(port, host, dbname, username, password) {
    this.mongoose = require("mongoose");
    this._connect(port, host, dbname, username, password);
  }

  _connect(port, host, dbname, username, password) {
    this.mongoose.Promise = global.Promise;
    this.mongoose.connect(`mongodb://${host}:${port}/${dbname}`);

    const { connection } = this.mongoose;

    connection.on("connected", () => {
      console.log('db connect  : ', dbConfig.mongo.dbname, '\n');
    });
    connection.on("error", (err) =>
      logger.error("Database Connection Failed" + err)
    );
    connection.on("disconnected", (err) =>
      logger.error("Database Connection Disconnected" + err)
    );
    process.on("SIGINT", () => {
      connection.close();
      logger.info("Database Connection closed due to NodeJs process termination");
      process.exit(0);
    });

    // initialize Model
    fs.readdirSync("models").forEach((file) => {
      require(path.join(__dirname, "..", "models", file));
    });
  }
}

module.exports = Database;
