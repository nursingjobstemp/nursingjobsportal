'use strict';
const { festivalsService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class FestivalsController { }
FestivalsController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['fest_title'] = req.body.fest_title;
      payload['fest_image'] = req.body.fest_image;
      payload['fest_pos'] = req.body.fest_pos;
      payload['fest_status'] = 'Y';
      payload['live_status'] = 'N';
      payload['view_type'] = req.body.view_type;
      payload['fest_date'] = req.body.fest_date;
      payload['lastupdate'] = new Date();
      let findwhere = {
         $and: [
            { fest_status: { $ne: "D" } },
            {
               $or: [
                  { fest_title: req.body.fest_title },
                  { fest_image: req.body.fest_image }
               ]
            }
         ]
      };
      const founded = await festivalsService.findOne(findwhere);
      if (!founded) {
         const created = await festivalsService.create(payload);
         if (created && created.fest_id) {
            logger.info(loggerMessage.createdSuccess.festival);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.festival);
         } else {
            logger.error(loggerMessage.createdFailure.festival);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.festival);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.festival);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.Festival);
   }
};

FestivalsController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['fest_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { fest_title: new RegExp(req.query.search, 'i') },
               { fest_image: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await festivalsService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.festival);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.festival);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.festival);
      } else {
         logger.error(loggerMessage.getDataFailure.festival);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.Festival);
   }
};

FestivalsController.findByPk = async (req, res) => {
   try {
      let { fest_id } = req.params;
      if (!fest_id) throw createError.BadRequest();
      const founded = await festivalsService.findByPk(fest_id);
      if (founded && founded.fest_id) {
         logger.info(loggerMessage.getIdSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.festival);
      } else {
         logger.info(loggerMessage.notFound.festival);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.Festival);
   }
};

FestivalsController.update = async (req, res) => {
   try {
      let { fest_id } = req.params;
      if (!fest_id) throw createError.BadRequest();
      const update = await festivalsService.update(fest_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await festivalsService.bulkUpdateLastupdate([Number(fest_id)]);
         logger.info(loggerMessage.updateDataSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.festival);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.festival);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.festival);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.festival);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.festival);
      } else {
         logger.error(loggerMessage.updateDataFailure.festival);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.Festival);
   }
};

FestivalsController.updateStatus = async (req, res) => {
   try {
      let { fest_ids, status } = req.body;
      const update = await festivalsService.bulkUpdateStatus(fest_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == fest_ids.length && update.modifiedCount == fest_ids.length) {
         await festivalsService.bulkUpdateLastupdate(fest_ids);
         logger.info(loggerMessage.updateStatusSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.festival);
      } else if (update && update.acknowledged == true && update.matchedCount == fest_ids.length && update.modifiedCount < fest_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.festival);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.festival);
      } else if (update && update.acknowledged == true && update.matchedCount < fest_ids.length) {
         logger.error(loggerMessage.notFound.festival);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.festival);
      } else {
         logger.error(loggerMessage.updateStatusFailure.festival);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.Festival);
   }
};

FestivalsController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await festivalsService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.festival);
      } else {
         logger.error(loggerMessage.updatePositionFailure.festival);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.Festival);
   }
};

FestivalsController.delete = async (req, res) => {
   try {
      let { fest_id } = req.query;
      if (!fest_id) throw createError.BadRequest();
      const deleted = await festivalsService.delete(fest_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.festival);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.festival);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.festival);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.festival);
      } else {
         logger.error(loggerMessage.deleteFailure.festival);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.festival);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.Festival);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.Festival);
   }
};

module.exports = FestivalsController;
