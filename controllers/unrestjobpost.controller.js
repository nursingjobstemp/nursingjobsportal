'use strict';
const { unrestjobpostService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class UnrestJobPostController { }
UnrestJobPostController.create = async (req, res) => {
   try {
      let payload = req.body;

      let findwhere = {
         $and: [
            { posted_status: { $ne: "D" } },
            {
               $and: [
                  { unrest_jcode: req.body.unrest_jcode },
                  { unrest_jcat: req.body.unrest_jcat },
                  { unrest_jsubcat: req.body.unrest_jsubcat }
               ]
            }
         ]
      };
      const founded = await unrestjobpostService.findOne(findwhere);
      if (!founded) {
         payload['duplicate_from'] = null;
         payload['jcat_type'] = req.body.jcat_type;
         payload['unrest_jcat'] = req.body.unrest_jcat;
         payload['unrest_jsubcat'] = req.body.unrest_jsubcat;
         payload['unrest_jcode'] = req.body.unrest_jcode;
         payload['verify'] = req.body.verify;
         payload['unrest_jdesc'] = req.body.unrest_jdesc;
         payload['unrest_jquali'] = req.body.unrest_jquali;
         payload['unrest_jrequ'] = req.body.unrest_jrequ;
         payload['high_qualif'] = req.body.high_qualif;
         payload['high_course'] = req.body.high_course ? req.body.high_course : [];
         payload['high_special'] = req.body.high_special;
         payload['unrest_jallow'] = req.body.unrest_jallow;
         payload['sal_id'] = req.body.sal_id;
         payload['unrest_jname'] = req.body.unrest_jname;
         payload['jtype_id'] = req.body.jtype_id;
         payload['jtype_id_new'] = req.body.jtype_id_new;
         payload['job_type'] = req.body.job_type;
         payload['key_skills'] = req.body.key_skills ? req.body.key_skills : null;
         payload['job_exp'] = req.body.job_exp;
         payload['country_id'] = req.body.country_id;
         payload['state'] = req.body.state;
         payload['unrest_jloct'] = req.body.unrest_jloct;
         payload['unrest_jcompany'] = req.body.unrest_jcompany;
         payload['comp_detail'] = req.body.comp_detail ? req.body.comp_detail : null;
         payload['unrest_jemail'] = req.body.unrest_jemail ? req.body.unrest_jemail : null;
         payload['unrest_jphoneold'] = req.body.unrest_jphoneold;
         payload['unrest_jphone'] = req.body.unrest_jphone ? req.body.unrest_jphone : null;
         payload['unrest_landline'] = req.body.unrest_landline ? req.body.unrest_landline : null;
         payload['unrest_sal'] = req.body.unrest_sal;
         payload['comp_address'] = req.body.comp_address ? req.body.comp_address : null;
         payload['apply'] = req.body.apply;
         payload['ip_address'] = req.body.ip_address;
         payload['posted_id'] = req.body.posted_id;
         payload['posted_by'] = req.body.posted_by;
         payload['posted_name'] = req.body.posted_name;
         payload['posted_pos'] = req.body.posted_pos;
         payload['exp_date'] = new Date(req.body.exp_date);
         payload['posted_status'] = req.body.posted_status ? req.body.posted_status : null;
         payload['comp_website'] = req.body.comp_website ? req.body.comp_website : null;
         payload['field_exp'] = req.body.field_exp;
         payload['nationality'] = req.body.nationality ? req.body.nationality : null;
         payload['no_of_openings'] = req.body.no_of_openings ? req.body.no_of_openings : null;
         payload['gender'] = req.body.gender ? req.body.gender : null;
         payload['posted_date'] = new Date();
         payload['posted_lastupdate'] = new Date();

         const created = await unrestjobpostService.create(payload);

         if (created && created.unrst_jid) {
            logger.info(loggerMessage.createdSuccess.unrestJobPost);

            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.unrestJobPost);
         } else {

            logger.error(loggerMessage.createdFailure.unrestJobPost);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.unrestJobPost);
         }
      } else {

         logger.error(loggerMessage.alreadyExisting.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.unrestJobPost);
      }
   } catch (error) {

      logger.error(loggerMessage.errorInCreate.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.unrestJobPost);
   }
};

UnrestJobPostController.createDuplicate = async (req, res) => {
   try {
      let payload = req.body;
      payload['duplicate_from'] = req.body.duplicate_from;
      payload['jcat_type'] = req.body.jcat_type;
      payload['unrest_jcat'] = req.body.unrest_jcat;
      payload['unrest_jsubcat'] = req.body.unrest_jsubcat;
      payload['unrest_jcode'] = req.body.unrest_jcode;
      payload['verify'] = req.body.verify;
      payload['unrest_jdesc'] = req.body.unrest_jdesc;
      payload['unrest_jquali'] = req.body.unrest_jquali;
      payload['unrest_jrequ'] = req.body.unrest_jrequ;
      payload['high_qualif'] = req.body.high_qualif;
      payload['high_course'] = req.body.high_course ? req.body.high_course : [];
      payload['high_special'] = req.body.high_special;
      payload['unrest_jallow'] = req.body.unrest_jallow;
      payload['sal_id'] = req.body.sal_id;
      payload['unrest_jname'] = req.body.unrest_jname;
      payload['jtype_id'] = req.body.jtype_id;
      payload['jtype_id_new'] = req.body.jtype_id_new;
      payload['job_type'] = req.body.job_type;
      payload['key_skills'] = req.body.key_skills ? req.body.key_skills : null;
      payload['job_exp'] = req.body.job_exp;
      payload['country_id'] = req.body.country_id;
      payload['state'] = req.body.state;
      payload['unrest_jloct'] = req.body.unrest_jloct;
      payload['unrest_jcompany'] = req.body.unrest_jcompany;
      payload['comp_detail'] = req.body.comp_detail ? req.body.comp_detail : null;
      payload['unrest_jemail'] = req.body.unrest_jemail ? req.body.unrest_jemail : null;
      payload['unrest_jphoneold'] = req.body.unrest_jphoneold;
      payload['unrest_jphone'] = req.body.unrest_jphone ? req.body.unrest_jphone : null;
      payload['unrest_landline'] = req.body.unrest_landline ? req.body.unrest_landline : null;
      payload['unrest_sal'] = req.body.unrest_sal;
      payload['comp_address'] = req.body.comp_address ? req.body.comp_address : null;
      payload['apply'] = req.body.apply;
      payload['ip_address'] = req.body.ip_address;
      payload['posted_id'] = req.body.posted_id;
      payload['posted_by'] = req.body.posted_by;
      payload['posted_name'] = req.body.posted_name;
      payload['posted_pos'] = req.body.posted_pos;
      payload['exp_date'] = new Date(req.body.exp_date);
      payload['posted_status'] = req.body.posted_status ? req.body.posted_status : null;
      payload['comp_website'] = req.body.comp_website ? req.body.comp_website : null;
      payload['field_exp'] = req.body.field_exp;
      payload['nationality'] = req.body.nationality ? req.body.nationality : null;
      payload['no_of_openings'] = req.body.no_of_openings ? req.body.no_of_openings : null;
      payload['gender'] = req.body.gender ? req.body.gender : null;
      payload['posted_date'] = new Date();
      payload['posted_lastupdate'] = new Date();

      let findwhere = {
         $and: [
            { posted_status: { $ne: "D" } },
            {
               $and: [
                  { unrest_jcode: req.body.unrest_jcode },
                  { unrest_jcat: req.body.unrest_jcat },
                  { unrest_jsubcat: req.body.unrest_jsubcat }
               ]
            }
         ]
      };
      const founded = await unrestjobpostService.findOne(findwhere);
      if (!founded) {
         const created = await unrestjobpostService.create(payload);
         if (created && created.unrst_jid) {
            logger.info(loggerMessage.createdSuccess.unrestJobPost);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.unrestJobPost);
         } else {
            logger.error(loggerMessage.createdFailure.unrestJobPost);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.unrestJobPost);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.unrestJobPost);
   }
};

UnrestJobPostController.get = async (req, res) => {
   try {
      let where = {};
      let dateRange = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { posted_status: req.query.status } : {};
      let posted_id = req.query && req.query.posted_id ? { posted_id: Number(req.query.posted_id) } : {};
      if (req.query && req.query.dateRange) {
         dateRange = {
            posted_date: {
               $gte: new Date(req.query.dateRange.split(',')[0]),
               $lte: new Date(req.query.dateRange.split(',')[1])
            }
         };
      }
      let totalWhere = {
         $and: [
            status,
            dateRange,
            posted_id,
            { country_id: 100 },
            { exp_date: { $gt: new Date() } },
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { unrest_jcode: new RegExp(req.query.search, 'i') },
               { unrest_name: new RegExp(req.query.search, 'i') },
               { unrest_jphone: new RegExp(req.query.search, 'i') },
               { unrest_jcompany: new RegExp(req.query.search, 'i') },
               { unrest_jemail: new RegExp(req.query.search, 'i') },
               { 'jobsCat.jcat_name': new RegExp(req.query.search, 'i') },
               { 'subCat.jcat_name': new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await unrestjobpostService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.unrestJobPost);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.unrestJobPost);
      } else {
         logger.error(loggerMessage.getDataFailure.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.unrestJobPost);
   }
};

UnrestJobPostController.getCompanyJobs = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { posted_status: req.query.status } : { posted_status: 'Y' };
      let expired = req.query && req.query.expired && req.query.expired == true ? { exp_date: { $lt: new Date() } } : { exp_date: { $gt: new Date() } };
      let posted_id = req.query && req.query.posted_id ? { posted_id: Number(req.query.posted_id) } : {};
      let totalWhere = {
         $and: [
            expired,
            posted_id,
            status,
            { country_id: 100 },
            { posted_by: 'C' }
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { unrest_jcompany: new RegExp(req.query.search, 'i') },
               { unrest_jcode: new RegExp(req.query.search, 'i') },
               { unrest_jemail: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await unrestjobpostService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.companyJobs);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.companyJobs);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.companyJobs);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.companyJobs);
      } else {
         logger.error(loggerMessage.getDataFailure.companyJobs);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.companyJobs);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.companyJobs);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.companyJobs);
   }
};

UnrestJobPostController.getExpJob = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { posted_status: req.query.status } : {};
      let posted_id = req.query && req.query.posted_id ? { posted_id: Number(req.query.posted_id) } : {};
      let totalWhere = {
         $and: [
            status,
            posted_id,
            { country_id: 100 },
            { exp_date: { $lt: new Date() } }
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { unrest_jdesc: new RegExp(req.query.search, 'i') },
               { unrest_jquali: new RegExp(req.query.search, 'i') },
               { unrest_jcode: new RegExp(req.query.search, 'i') },
               { unrest_jcompany: new RegExp(req.query.search, 'i') },
               { unrest_jquali: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await unrestjobpostService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.expJob);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.expJob);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.expJob);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.expJob);
      } else {
         logger.error(loggerMessage.getDataFailure.expJob);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.expJob);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.expJob);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.expJob);
   }
};

UnrestJobPostController.getDuplicateJob = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { posted_status: req.query.status } : {};
      let posted_id = req.query && req.query.posted_id ? { posted_id: Number(req.query.posted_id) } : {};
      let totalWhere = {
         $and: [
            { duplicate_from: { $ne: null } },
            { country_id: 100 },
            posted_id,
            status,
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { unrest_jname: new RegExp(req.query.search, 'i') },
               { unrest_jcode: new RegExp(req.query.search, 'i') },
               { unrest_jquali: new RegExp(req.query.search, 'i') },
               { unrest_jcompany: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await unrestjobpostService.getDupJob(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDuplicateJob.privateJob);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDuplicateJob.privateJob);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.duplicateJob.privateJob);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.duplicateJob.privateJob);
      } else {
         logger.error(loggerMessage.getDuplicateJobFailure.privateJob);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDuplicateJobFailure.privateJob);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.duplicateJob);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.duplicateJob);
   }
};

UnrestJobPostController.findByPk = async (req, res) => {
   try {
      let { unrst_jid } = req.params;
      if (!unrst_jid) throw createError.BadRequest();
      const founded = await unrestjobpostService.findByPk(unrst_jid);
      if (founded && founded.unrst_jid) {
         logger.info(loggerMessage.getIdSuccess.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.unrestJobPost);
      } else {
         logger.info(loggerMessage.notFound.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.notFound.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.unrestJobPost);
   }
};

UnrestJobPostController.update = async (req, res) => {
   try {
      let { unrst_jid } = req.params;
      if (!unrst_jid) throw createError.BadRequest();
      const update = await unrestjobpostService.update(unrst_jid, req.body);
      if (update && update.acknowledged == true && update.modifiedCount == 1) {
         await unrestjobpostService.bulkUpdateLastupdate([Number(unrst_jid)]);
         logger.info(loggerMessage.updateDataSuccess.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.unrestJobPost);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.unrestJobPost);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.unrestJobPost);
      } else {
         logger.error(loggerMessage.updateDataFailure.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.unrestJobPost);
   }
};

UnrestJobPostController.updateStatus = async (req, res) => {
   try {
      let { unrst_jids, status } = req.body;
      if (!unrst_jids || !unrst_jids.length) throw createError.BadRequest();
      const update = await unrestjobpostService.bulkUpdateStatus(unrst_jids, status);
      if (update && update.acknowledged == true && update.modifiedCount == unrst_jids.length) {
         await unrestjobpostService.bulkUpdateLastupdate(unrst_jids);
         logger.info(loggerMessage.updateStatusSuccess.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.unrestJobPost);
      } else if (update && update.acknowledged == true && update.matchedCount == unrst_jids.length && update.modifiedCount < unrst_jids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.unrestJobPost);
      } else if (update && update.acknowledged == true && update.matchedCount < unrst_jids.length) {
         logger.error(loggerMessage.notFound.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.unrestJobPost);
      } else {
         logger.error(loggerMessage.updateStatusFailure.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.unrestJobPost);
   }
};

UnrestJobPostController.delete = async (req, res) => {
   try {
      let { unrst_jid } = req.query;
      if (!unrst_jid) throw createError.BadRequest();
      const deleted = await unrestjobpostService.delete(unrst_jid);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.unrestJobPost);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.unrestJobPost);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.unrestJobPost);
      } else {
         logger.error(loggerMessage.deleteFailure.unrestJobPost);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.unrestJobPost);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.unrestJobPost);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.unrestJobPost);
   }
};

module.exports = UnrestJobPostController;