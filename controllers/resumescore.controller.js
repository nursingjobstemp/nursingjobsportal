'use strict';
const { resumescoreService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class ResumeScoreController { }
ResumeScoreController.create = async (req, res) => {
   try {
      let payload = req.body;
     
         payload['resume_title'] = req.body.resume_title,
         payload['resume_fdesc'] = req.body.resume_fdesc,
         payload['resume_sdesc'] = req.body.resume_sdesc,
         payload['first_image'] = req.body.first_image,
         payload['seo_title'] = req.body.seo_title,
         payload['seo_description'] = req.body.seo_description,
         payload['seo_keywords'] = req.body.seo_keywords,
         payload['second_image'] = req.body.second_image,
         payload['resume_date'] = new Date(),
         payload['lastupdate'] = new Date()
    
      let findwhere = {
         $or: [
            { resume_title: req.body.resume_title },
            { seo_title: req.body.seo_title }
         ]
      };
      const founded = await resumescoreService.findOne(findwhere);
      if (!founded) {
         const created = await resumescoreService.create(payload);
         if (created && created.resume_id) {
            logger.info(loggerMessage.createdSuccess.resumeScore);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.resumeScore);
         } else {
            logger.error(loggerMessage.createdFailure.resumeScore);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.resumeScore);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.resumeScore);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.resumeScore);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.resumeScore);
   }
};

ResumeScoreController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.search) {
         where = {
            $or: [
               { resume_title: new RegExp(req.query.search, 'i') },
               { seo_title: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await resumescoreService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.resumeScore);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.resumeScore);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.resumeScore);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.resumeScore);
      } else {
         logger.error(loggerMessage.getDataFailure.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.resumeScore);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.resumeScore);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.resumeScore);
   }
};

ResumeScoreController.findByPk = async (req, res) => {
   try {
      let { resume_id } = req.params;
      if (!resume_id) throw createError.BadRequest();
      const founded = await resumescoreService.findByPk(resume_id);
      if (founded && founded.resume_id) {
         logger.info(loggerMessage.getIdSuccess.resumeScore);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.resumeScore);
      } else {
         logger.info(loggerMessage.notFound.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.resumeScore);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.resumeScore);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.resumeScore);
   }
};

ResumeScoreController.update = async (req, res) => {
   try {
      let { resume_id } = req.params;
      if (!resume_id) throw createError.BadRequest();
      const update = await resumescoreService.update(resume_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await resumescoreService.bulkUpdateLastupdate([Number(resume_id)]);
         logger.info(loggerMessage.updateDataSuccess.resumeScore);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.resumeScore);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.resumeScore);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.resumeScore);
      } else {
         logger.error(loggerMessage.updateDataFailure.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.resumeScore);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.resumeScore);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.resumeScore);
   }
};

ResumeScoreController.delete = async (req, res) => {
   try {
      let { resume_id } = req.query;
      if (!resume_id) throw createError.BadRequest();
      const deleted = await resumescoreService.delete(resume_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.resumeScore);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.resumeScore);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.resumeScore);
      } else {
         logger.error(loggerMessage.deleteFailure.resumeScore);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.resumeScore);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.resumeScore);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.resumeScore);
   }
};

module.exports = ResumeScoreController;
