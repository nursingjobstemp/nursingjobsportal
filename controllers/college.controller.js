'use strict';
const { collegeService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class CollegeController { }
CollegeController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['colg_name'] =  req.body.colg_name,
      payload['colg_slug'] =  req.body.colg_slug,
      payload['colg_pos'] =  req.body.colg_pos,
      payload['colg_status'] =  "Y",
      payload['colg_date'] =  new Date(),
      payload['lastupdate'] =  new Date()
   
      let findwhere = {
         $and: [
            { colg_status: { $ne: "D" } },
            {
               $or: [
                  { colg_name: req.body.colg_name },
                  { colg_slug: req.body.colg_slug }
               ]
            }
         ]
      };
      const founded = await collegeService.findOne(findwhere);
      if (!founded) {
         const created = await collegeService.create(payload);
         if (created && created.colg_id) {
            logger.info(loggerMessage.createdSuccess.college);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.college);
         } else {
            logger.error(loggerMessage.createdFailure.college);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.college);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.college);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.college);
   }
};

CollegeController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['colg_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { colg_name: new RegExp(req.query.search, 'i') },
               { colg_slug: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await collegeService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.college);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.college);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.college);
      } else {
         logger.error(loggerMessage.getDataFailure.college);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.college);
   }
};

CollegeController.findByPk = async (req, res) => {
   try {
      let { colg_id } = req.params;
      if (!colg_id) throw createError.BadRequest();
      const founded = await collegeService.findByPk(colg_id);
      if (founded && founded.colg_id) {
         logger.info(loggerMessage.getIdSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.college);
      } else {
         logger.info(loggerMessage.notFound.college);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.college);
   }
};

CollegeController.update = async (req, res) => {
   try {
      let { colg_id } = req.params;
      if (!colg_id) throw createError.BadRequest();
      const update = await collegeService.update(colg_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await collegeService.bulkUpdateLastupdate([Number(colg_id)]);
         logger.info(loggerMessage.updateDataSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.college);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.college);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.college);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.college);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.college);
      } else {
         logger.error(loggerMessage.updateDataFailure.college);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.college);
   }
};

CollegeController.updateStatus = async (req, res) => {
   try {
      let { colg_ids, status } = req.body;
      const update = await collegeService.bulkUpdateStatus(colg_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == colg_ids.length && update.modifiedCount == colg_ids.length) {
         await collegeService.bulkUpdateLastupdate(colg_ids);
         logger.info(loggerMessage.updateStatusSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.college);
      } else if (update && update.acknowledged == true && update.matchedCount == colg_ids.length && update.modifiedCount < colg_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.college);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.college);
      } else if (update && update.acknowledged == true && update.matchedCount < colg_ids.length) {
         logger.error(loggerMessage.notFound.college);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.college);
      } else {
         logger.error(loggerMessage.updateStatusFailure.college);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.college);
   }
};

CollegeController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await collegeService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.college);
      } else {
         logger.error(loggerMessage.updatePositionFailure.college);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.college);
   }
};

CollegeController.delete = async (req, res) => {
   try {
      let { colg_id } = req.query;
      if (!colg_id) throw createError.BadRequest();
      const deleted = await collegeService.delete(colg_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.college);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.college);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.college);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.college);
      } else {
         logger.error(loggerMessage.deleteFailure.college);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.college);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.college);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.college);
   }
};

module.exports = CollegeController;
