'use strict';
const { notificationService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class NotificationController { }
NotificationController.create = async (req, res) => {
   try {
      let payload = req.body;

         payload['noti_title'] = req.body.noti_title,
         payload['noti_msg'] = req.body.noti_msg,
         payload['chat_id'] = req.body.chat_id,
         payload['noti_type'] = req.body.noti_type,
         payload['job_applyid'] = req.body.job_applyid,
         payload['noti_from'] = req.body.noti_from,
         payload['noti_ftype'] = req.body.noti_ftype,
         payload['noti_to'] = req.body.noti_to,
         payload['noti_date'] = new Date(),
         payload['noti_status'] = "Y",
         payload['noti_read'] = "Y",
         payload['lastupdate'] = new Date()

      let findwhere = {
         $and: [
            { noti_status: { $ne: "D" } },
            { noti_title: req.body.noti_title }
         ]
      };
      const founded = await notificationService.findOne(findwhere);
      if (!founded) {
         const created = await notificationService.create(payload);
         if (created && created.notify_id) {
            logger.info(loggerMessage.createdSuccess.notification);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.notification);
         } else {
            logger.error(loggerMessage.createdFailure.notification);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.notification);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.notification);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.notification);
   }
};

NotificationController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['noti_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { noti_title: new RegExp(req.query.search, 'i') },
               { noti_msg: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await notificationService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.notification);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.notification);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.notification);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.notification);
      } else {
         logger.error(loggerMessage.getDataFailure.notification);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.notification);
   }
};

NotificationController.findByPk = async (req, res) => {
   try {
      let { notify_id } = req.params;
      if (!notify_id) throw createError.BadRequest();
      const founded = await notificationService.findByPk(notify_id);
      if (founded && founded.notify_id) {
         logger.info(loggerMessage.getIdSuccess.notification);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.notification);
      } else {
         logger.info(loggerMessage.notFound.notification);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.notification);
   }
};

NotificationController.update = async (req, res) => {
   try {
      let { notify_id } = req.params;
      if (!notify_id) throw createError.BadRequest();
      const update = await notificationService.update(notify_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await notificationService.bulkUpdateLastupdate([Number(notify_id)]);
         logger.info(loggerMessage.updateDataSuccess.notification);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.notification);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.notification);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.notification);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.notification);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.notification);
      } else {
         logger.error(loggerMessage.updateDataFailure.notification);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.notification);
   }
};

NotificationController.updateStatus = async (req, res) => {
   try {
      let { notify_ids, status } = req.body;
      const update = await notificationService.bulkUpdateStatus(notify_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == notify_ids.length && update.modifiedCount == notify_ids.length) {
         await notificationService.bulkUpdateLastupdate(notify_ids);
         logger.info(loggerMessage.updateStatusSuccess.notification);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.notification);
      } else if (update && update.acknowledged == true && update.matchedCount == notify_ids.length && update.modifiedCount < notify_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.notification);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.notification);
      } else if (update && update.acknowledged == true && update.matchedCount < notify_ids.length) {
         logger.error(loggerMessage.notFound.notification);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.notification);
      } else {
         logger.error(loggerMessage.updateStatusFailure.notification);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.notification);
   }
};

NotificationController.delete = async (req, res) => {
   try {
      let { notify_id } = req.query;
      if (!notify_id) throw createError.BadRequest();
      const deleted = await notificationService.delete(notify_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.notification);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.notification);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.notification);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.notification);
      } else {
         logger.error(loggerMessage.deleteFailure.notification);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.notification);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.notification);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.notification);
   }
};

module.exports = NotificationController;
