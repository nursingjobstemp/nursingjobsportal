'use strict';
const { chatService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class ChatController { }
ChatController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['job_id'] = req.body.job_id,
      payload['applyid'] = req.body.applyid,
      payload['chat_from'] = req.body.chat_from,
      payload['chat_fname'] = req.body.chat_fname,
      payload['chat_ftype'] = req.body.chat_ftype,
      payload['chat_to'] = req.body.chat_to,
      payload['chat_tname'] = req.body.chat_tname,
      payload['chat_ttype'] = req.body.chat_ttype,
      payload['chat_msg'] = req.body.chat_msg,
      payload['chat_status'] = req.body.chat_status,
      payload['chat_date'] = req.body.chat_date,
      payload['read_status'] = req.body.read_status,
      payload['read_date'] = req.body.read_date,
      payload['ipaddr'] = req.body.ipaddr,
      payload['lastupdate'] = new Date()
     
      let findWhere = {
         $and: [
            { chat_status: { $ne: "D" } },
            {
               $and: [
                  { chat_from: req.body.chat_from },
                  { chat_fname: req.body.chat_fname },
                  { chat_to: req.body.chat_to },
                  { chat_tname: req.body.chat_tname },
                  { chat_ttype: req.body.chat_ttype }
               ]
            }
         ]
      };
      const founded = await chatService.findOne(findWhere);
      if (!founded) {
         const created = await chatService.create(payload);
         if (created && created.chat_id) {
            logger.info(loggerMessage.createdSuccess.chat);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.chat);
         } else {
            logger.error(loggerMessage.createdFailure.chat);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.chat);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.chat);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.chat);
   }
};

ChatController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['chat_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         var search = req.query.search;
         where = {
            $or: [
               { chat_fname: new RegExp(search, 'i') },
               { chat_tname: new RegExp(search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await chatService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.chat);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.chat);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.chat);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.chat);
      } else {
         logger.error(loggerMessage.getDataFailure.chat);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.chat);
   }
};

ChatController.findByPk = async (req, res) => {
   try {
      let { chat_id } = req.params;
      if (!chat_id) throw createError.BadRequest();
      const founded = await chatService.findByPk(chat_id);
      if (founded && founded.chat_id) {
         logger.info(loggerMessage.getIdSuccess.chat);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.chat);
      } else {
         logger.info(loggerMessage.notFound.chat);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.chat);
   }
};

ChatController.update = async (req, res) => {
   try {
      let { chat_id } = req.params;
      if (!chat_id) throw createError.BadRequest();
      const update = await chatService.update(chat_id, req.body);
      if (update && update.acknowledged == true && update.modifiedCount == 1) {
         await chatService.bulkUpdateLastupdate([Number(chat_id)]);
         logger.info(loggerMessage.updateDataSuccess.chat);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.chat);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.chat);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.chat);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.chat);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.chat);
      } else {
         logger.error(loggerMessage.updateDataFailure.chat);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.chat);
   }
};

ChatController.updateStatus = async (req, res) => {
   try {
      let { chat_ids, status } = req.body;
      const update = await chatService.bulkUpdateStatus(chat_ids, status);
      if (update && update.acknowledged == true && update.modifiedCount == chat_ids.length) {
         await chatService.bulkUpdateLastupdate(chat_ids);
         logger.info(loggerMessage.updateStatusSuccess.chat);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.chat);
      } else if (update && update.acknowledged == true && update.matchedCount == chat_ids.length && update.modifiedCount < chat_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.chat);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.chat);
      } else if (update && update.acknowledged == true && update.matchedCount < chat_ids.length) {
         logger.error(loggerMessage.notFound.chat);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.chat);
      } else {
         logger.error(loggerMessage.updateStatusFailure.chat);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.chat);
   }
};

ChatController.delete = async (req, res) => {
   try {
      let { chat_id } = req.query;
      const deleted = await chatService.delete(chat_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.chat);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.chat);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.chat);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.chat);
      } else {
         logger.error(loggerMessage.deleteFailure.chat);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.chat);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.chat);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.chat);
   }
};

module.exports = ChatController;
