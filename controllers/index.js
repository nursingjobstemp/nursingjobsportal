
const adminMenuController = require("./adminMenu.controller")
const chatController = require('./chat.controller')
const cityController = require('./city.controller')
const collegeController = require('./college.controller')
const contactresumeController = require('./contactresume.controller')
const edu_courseController = require('./edu_course.controller')
const empedudetailController = require('./empedudetail.controller')
const empjobcatController = require('./empjobcat.controller')
const empkskillsController = require('./empkskills.controller')
const emploctController = require('./emploct.controller')
const employerController = require("./employer.controller")
const empoffdetailsController = require('./empoffdetails.controller')
const enquiryController = require('./enquiry.controller')
const fileController = require('./file.controller')
const industrytypeController = require('./industrytype.controller')
const intjobpostController = require('./intjobpost.controller')
const intscheduleController = require('./intschedule.controller')
const jobappliedController = require('./jobapplied.controller')
const jobpostingController = require('./jobposting.controller')
const jobscategoryController = require('./jobscategory.controller')
const jobtypeController = require('./jobtype.controller')
const keyskillsController = require('./keyskills.controller')
const notificationController = require('./notification.controller')
const operatorController = require('./operator.controller')
const resumescoreController = require('./resumescore.controller')
const salaryController = require('./salary.controller')
const seekerController = require("./seeker.controller")
const push_notifyController = require('./push_notify.controller')
const unrestjobpost = require('./unrestjobpost.controller')
const festivalsController = require('./festivals.controller')


module.exports = {
   adminMenuController,
   chatController,
   cityController,
   collegeController,
   contactresumeController,
   edu_courseController,
   empedudetailController,
   empjobcatController,
   empkskillsController,
   emploctController,
   employerController,
   empoffdetailsController,
   enquiryController,
   fileController,
   industrytypeController,
   intjobpostController,
   intscheduleController,
   jobappliedController,
   jobpostingController,
   jobscategoryController,
   jobtypeController,
   keyskillsController,
   notificationController,
   operatorController,
   resumescoreController,
   seekerController,
   salaryController,
   push_notifyController,
   unrestjobpost,
   festivalsController,
}