'use strict';
const { empjobcatService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EmpJobCatController { }
EmpJobCatController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] =  req.body.emp_id,
      payload['cat_id'] =  req.body.cat_id,
      payload['subcat_id'] =  req.body.subcat_id,
      payload['mjcat_status'] =  "Y",
      payload['mjcat_date'] =  new Date(),
      payload['lastupdate'] =  new Date()
     
      let findwhere = {
         $and: [
            { mjcat_status: { $ne: "D" } },
            {
               $and: [
                  { emp_id: req.body.emp_id },
                  { cat_id: req.body.cat_id },
                  { subcat_id: req.body.subcat_id }
               ]
            }
         ]
      };
      const founded = await empjobcatService.findOne(findwhere);
      if (!founded) {
         const created = await empjobcatService.create(payload);
         if (created && created.mjcat_id) {
            logger.info(loggerMessage.createdSuccess.emp_Job_Category);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.emp_Job_Category);
         } else {
            logger.error(loggerMessage.createdFailure.emp_Job_Category);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.emp_Job_Category);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.emp_Job_Category);
   }
};

EmpJobCatController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['mjcat_status'] = req.query.status;
      };
      const { rows, count, totalCount } = await empjobcatService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.emp_Job_Category);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.emp_Job_Category);
      } else {
         logger.error(loggerMessage.getDataFailure.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.emp_Job_Category);
   }
};

EmpJobCatController.findByPk = async (req, res) => {
   try {
      let { mjcat_id } = req.params;
      if (!mjcat_id) throw createError.BadRequest();
      const founded = await empjobcatService.findByPk(mjcat_id);
      if (founded && founded.mjcat_id) {
         logger.info(loggerMessage.getDataSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getDataSuccess.emp_Job_Category);
      } else {
         logger.info(loggerMessage.notFound.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.emp_Job_Category);
   }
};

EmpJobCatController.update = async (req, res) => {
   try {
      let { mjcat_id } = req.params;
      if (!mjcat_id) throw createError.BadRequest();
      const update = await empjobcatService.update(mjcat_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await empjobcatService.bulkUpdateLastupdate([Number(mjcat_id)]);
         logger.info(loggerMessage.updateDataSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.emp_Job_Category);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_Job_Category);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Job_Category);
      } else {
         logger.error(loggerMessage.updateDataFailure.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.emp_Job_Category);
   }
};

EmpJobCatController.updateStatus = async (req, res) => {
   try {
      let { mjcat_ids, status } = req.body;
      const update = await empjobcatService.bulkUpdateStatus(mjcat_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == mjcat_ids.length && update.modifiedCount == mjcat_ids.length) {
         await empjobcatService.bulkUpdateLastupdate(mjcat_ids);
         logger.info(loggerMessage.updateStatusSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.emp_Job_Category);
      } else if (update && update.acknowledged == true && update.matchedCount == mjcat_ids.length && update.modifiedCount < mjcat_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_Job_Category);
      } else if (update && update.acknowledged == true && update.matchedCount < mjcat_ids.length) {
         logger.error(loggerMessage.notFound.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Job_Category);
      } else {
         logger.error(loggerMessage.updateStatusFailure.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.emp_Job_Category);
   }
};

EmpJobCatController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await empjobcatService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.emp_Job_Category);
      } else {
         logger.error(loggerMessage.updatePositionFailure.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.emp_Job_Category);
   }
};

EmpJobCatController.delete = async (req, res) => {
   try {
      let { mjcat_id } = req.query;
      if (!mjcat_id) throw createError.BadRequest();
      const deleted = await empjobcatService.delete(mjcat_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.emp_Job_Category);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.emp_Job_Category);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Job_Category);
      } else {
         logger.error(loggerMessage.deleteFailure.emp_Job_Category);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.emp_Job_Category);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.emp_Job_Category);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.emp_Job_Category);
   }
};

module.exports = EmpJobCatController;
