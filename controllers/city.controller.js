'use strict';
const { cityService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class CityController { }
CityController.create = async (req, res) => {
   try {
      const city_id = await cityService.find();
      let payload = req.body;
      payload['state_id'] = req.body.state_id,
         payload['country_id'] = req.body.country_id,
         payload['city_name'] = req.body.city_name,
         payload['city_slug'] = req.body.city_slug,
         payload['city_code'] = city_id && city_id.rows.length > 0 ? Number(city_id.rows[0].city_code) + 1 : 1,
         payload['city_image'] = req.body.city_image,
         payload['city_status'] = "Y",
         payload['foot_status'] = "N",
         payload['lastupdate'] = new Date();
      let findWhere = {
         $and: [
            { city_status: { $ne: "D" } },
            {
               $and: [
                  { city_name: req.body.city_name },
                  { city_slug: req.body.city_slug },
                  { state_id: req.body.state_id },
                  { country_id: req.body.country_id }
               ]
            }
         ]
      };
      let founded = await cityService.findOne(findWhere);
      if (!founded) {
         const created = await cityService.create(payload);
         if (created && created.city_id) {
            logger.info(loggerMessage.createdSuccess.city);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.city);
         } else {
            logger.error(loggerMessage.createdFailure.city);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.city);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.city);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.city);
   }
};

CityController.get = async (req, res) => {
   try {
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { city_status: req.query.status } : {};
      let id = req.query && req.query.id ? { city_id: Number(req.query.id) } : {};
      let sortBy = req.query && (req.query.stateId || req.query.id) ? { city_name: 1 } : req.query.countryId ? { 'stateData.state_name': 1, 'cityData.city_name': 1 } : { 'countryData.country_name': 1 };
      let stateId = req.query && req.query.stateId ? { state_id: Number(req.query.stateId) } : {};
      let countryId = req.query && req.query.countryId ? { country_id: Number(req.query.countryId) } : {};
      let totalWhere = {
         $and: [
            { city_id: { $ne: 0 } },
            status,
            id,
            stateId,
            countryId
         ]
      };
      let queryWhere = {};
      if (req.query && req.query.search) {
         queryWhere = {
            $or: [
               { city_name: new RegExp(req.query.search, 'i') },
               { city_slug: new RegExp(req.query.search, 'i') },
               { 'stateData.state_name': new RegExp(req.query.search, 'i') },
               { 'countryData.country_name': new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await cityService.findAllAndCount(_start, _limit, totalWhere, queryWhere, sortBy);
      
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.city);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.city);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.city);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.city);
      } else {
         logger.error(loggerMessage.getDataFailure.city);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.city);
   }
};

CityController.findByPk = async (req, res) => {
   try {
      let { city_id } = req.params;
      if (!city_id) throw createError.BadRequest();
      const founded = await cityService.findByPk(city_id);
      if (founded && founded.city_id) {
         logger.info(loggerMessage.getIdSuccess.city);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.city);
      } else {
         logger.info(loggerMessage.notFound.city);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.city);
   }
};

CityController.update = async (req, res) => {
   try {
      let { city_id } = req.params;
      if (!city_id) throw createError.BadRequest();
      const update = await cityService.update(city_id, req.body);
      if (update && update.acknowledged == true && update.modifiedCount == 1) {
         await cityService.bulkUpdateLastupdate([Number(city_id)]);
         logger.info(loggerMessage.updateDataSuccess.city);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.city);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.city);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.city);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.city);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.city);
      } else {
         logger.error(loggerMessage.updateDataFailure.city);
         return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.updateDataFailure.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.city);
   }
};

CityController.updateStatus = async (req, res) => {
   try {
      let { city_ids, status } = req.body;
      if (!city_ids || !city_ids.length) throw createError.BadRequest();
      const update = await cityService.bulkUpdateStatus(city_ids, status);
      if (update && update.acknowledged == true && update.modifiedCount == city_ids.length) {
         await cityService.bulkUpdateLastupdate(city_ids);
         logger.info(loggerMessage.updateStatusSuccess.city);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.city);
      } else if (update && update.acknowledged == true && update.matchedCount == city_ids.length && update.modifiedCount < city_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.city);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.city);
      } else if (update && update.acknowledged == true && update.matchedCount < city_ids.length) {
         logger.error(loggerMessage.notFound.city);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.city);
      } else {
         logger.error(loggerMessage.updateStatusFailure.city);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.city);
   }
};

CityController.delete = async (req, res) => {
   try {
      let { city_id } = req.query;
      const deleted = await cityService.delete(city_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.city);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.city);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.city);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.city);
      } else {
         logger.error(loggerMessage.deleteFailure.city);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.city);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.city);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.city);
   }
};

module.exports = CityController;
