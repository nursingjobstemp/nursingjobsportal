'use strict';
const { enquiryService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EnquiryController { }
EnquiryController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['enq_name'] = req.body.enq_name,
      payload['enq_email'] = req.body.enq_email,
      payload['enq_mobile'] = Number(req.body.enq_mobile),
      payload['enq_msg'] = req.body.enq_msg,
      payload['enq_date'] = new Date(req.body.enq_date),
      payload['ipaddress'] = req.body.ipaddress,
      payload['enq_altmobile'] = req.body.enq_altmobile,
      payload['maincat'] = req.body.maincat,
      payload['type_home'] = req.body.type_home,
      payload['type_bhk'] = req.body.type_bhk,
      payload['enq_loc'] = req.body.enq_loc,
      payload['lastupdate'] = new Date()
   
      let findwhere = {
         $or: [
            { enq_name: req.body.enq_name },
            { enq_loc: req.body.enq_loc }
         ]
      };
      const founded = await enquiryService.findOne(findwhere);
      if (!founded) {
         const created = await enquiryService.create(payload);
         if (created && created.enq_id) {
            logger.info(loggerMessage.createdSuccess.enquiry);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.enquiry);
         } else {
            logger.error(loggerMessage.createdFailure.enquiry);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.enquiry);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.enquiry);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.enquiry);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.enquiry);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.enquiry);
   }
};

EnquiryController.get = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.search) {
         where = {
            $or: [
               { enq_name: new RegExp(req.query.search, 'i') },
               { enq_loc: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await enquiryService.findAllAndCount(where, _start, _limit);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.enquiry);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.enquiry);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.enquiry);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.enquiry);
      } else {
         logger.error(loggerMessage.getDataFailure.enquiry);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.enquiry);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.enquiry);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.enquiry);
   }
};

EnquiryController.findByPk = async (req, res) => {
   try {
      let { enq_id } = req.params;
      if (!enq_id) throw createError.BadRequest();
      const founded = await enquiryService.findByPk(enq_id);
      if (founded && founded.enq_id) {
         logger.info(loggerMessage.getDataSuccess.enquiry);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getDataSuccess.enquiry);
      } else {
         logger.info(loggerMessage.notFound.enquiry);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.enquiry);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.enquiry);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.enquiry);
   }
};

EnquiryController.update = async (req, res) => {
   try {
      let { enq_id } = req.params;
      if (!enq_id) throw createError.BadRequest();
      const update = await enquiryService.update(enq_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await enquiryService.bulkUpdateLastupdate([Number(enq_id)]);
         logger.info(loggerMessage.updateDataSuccess.enquiry);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.enquiry);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.enquiry);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.enquiry);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.enquiry);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.enquiry);
      } else {
         logger.error(loggerMessage.updateDataFailure.enquiry);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.enquiry);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.enquiry);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.enquiry);
   }
};

EnquiryController.delete = async (req, res) => {
   try {
      let { enq_id } = req.query;
      if (!enq_id) throw createError.BadRequest();
      const deleted = await enquiryService.delete(enq_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.enquiry);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.enquiry);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.enquiry);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.enquiry);
      } else {
         logger.error(loggerMessage.deleteFailure.enquiry);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.enquiry);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.enquiry);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.enquiry);
   }
};

module.exports = EnquiryController;
