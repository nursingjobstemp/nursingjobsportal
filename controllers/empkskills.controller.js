'use strict';
const { empkskillsService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EmpSkillsController { }
EmpSkillsController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] = req.body.emp_id,
      payload['keysk_id'] = req.body.keysk_id,
      payload['keysk_name'] = req.body.keysk_name,
      payload['empkskil_status'] = "Y",
      payload['empkskil_date'] = new Date(),
      payload['lastupdate'] = new Date()
   
      let findwhere = {
         $and: [
            { empkskil_status: { $ne: "D" } },
            { keysk_name: req.body.keysk_name }
         ]
      };
      const founded = await empkskillsService.findOne(findwhere);
      if (!founded) {
         const created = await empkskillsService.create(payload);
         if (created && created.empkskil_id) {
            logger.info(loggerMessage.createdSuccess.emp_KeySkills);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.emp_KeySkills);
         } else {
            logger.error(loggerMessage.createdFailure.emp_KeySkills);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.emp_KeySkills);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.emp_KeySkills);
   }
};

EmpSkillsController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['empkskil_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            keysk_name: new RegExp(req.query.search, 'i')
         };
      };
      const { rows, count, totalCount } = await empkskillsService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.emp_KeySkills);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.emp_KeySkills);
      } else {
         logger.error(loggerMessage.getDataFailure.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.emp_KeySkills);
   }
};

EmpSkillsController.findByPk = async (req, res) => {
   try {
      let { empkskil_id } = req.params;
      if (!empkskil_id) throw createError.BadRequest();
      const founded = await empkskillsService.findByPk(empkskil_id);
      if (founded && founded.empkskil_id) {
         logger.info(loggerMessage.getIdSuccess.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.emp_KeySkills);
      } else {
         logger.info(loggerMessage.notFound.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.emp_KeySkills);
   }
};

EmpSkillsController.update = async (req, res) => {
   try {
      let { empkskil_id } = req.params;
      if (!empkskil_id) throw createError.BadRequest();
      const update = await empkskillsService.update(empkskil_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await empkskillsService.bulkUpdateLastupdate([Number(empkskil_id)]);
         logger.info(loggerMessage.updateDataSuccess.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.emp_KeySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_KeySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_KeySkills);
      } else {
         logger.error(loggerMessage.updateDataFailure.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.emp_KeySkills);
   }
};

EmpSkillsController.updateStatus = async (req, res) => {
   try {
      let { empkskil_ids, status } = req.body;
      const update = await empkskillsService.bulkUpdateStatus(empkskil_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == empkskil_ids.length && update.modifiedCount == empkskil_ids.length) {
         await empkskillsService.bulkUpdateLastupdate(empkskil_ids);
         logger.info(loggerMessage.updateStatusSuccess.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.emp_KeySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == empkskil_ids.length && update.modifiedCount < empkskil_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_KeySkills);
      } else if (update && update.acknowledged == true && update.matchedCount < empkskil_ids.length) {
         logger.error(loggerMessage.notFound.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_KeySkills);
      } else {
         logger.error(loggerMessage.updateStatusFailure.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.emp_KeySkills);
   }
};

EmpSkillsController.delete = async (req, res) => {
   try {
      let { empkskil_id } = req.query;
      const deleted = await empkskillsService.delete(empkskil_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.emp_KeySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.emp_KeySkills);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_KeySkills);
      } else {
         logger.error(loggerMessage.deleteFailure.emp_KeySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.emp_KeySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.emp_KeySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.emp_KeySkills);
   }
};

module.exports = EmpSkillsController;
