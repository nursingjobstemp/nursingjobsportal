'use strict'
const { logger } = require('../helper')
const { responseMessage, loggerMessage, statusCodes } = require('../constants')
const { response } = require('../middleware')
const { push_notifyService, seekerService } = require('../services')
const admin = require("firebase-admin")
admin.initializeApp({
    credential: admin.credential.cert(require("../jobslink-b1814-firebase-adminsdk-bhmip-9cec5f074a.json"))
})

class push_notifyController { }
push_notifyController.create = async (req, res) => {
    try {
        let findwhere = {
            $and: [
                { notify_title: req.body.notify_title },
                { notify_msg: req.body.notify_msg },
                {
                    $or: [
                        { seeker_id: req.body.seeker_id },
                        { employer_id: req.body.employer_id }
                    ]
                }
            ]
        }
        const founded = await push_notifyService.findOne(findwhere)
        if (!founded) {
            const obj = {
                notify_title: req.body.notify_title,
                notify_msg: req.body.notify_msg,
                seeker_id: req.body.seeker_id,
                employer_id: req.body.employer_id,
                user_status: "A",
                notify_status: "Y",
                notify_time: req.body.notify_time,
                created_date: new Date(),
                last_update: new Date()
            }
            const created = await push_notifyService.create(obj)
            if (created && created.notify_id) {
                logger.info(loggerMessage.createdSuccess.push_notify)
                return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.push_notify)
            } else {
                logger.error(loggerMessage.createdFailure.push_notify)
                return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.push_notify)
            }
        } else {
            logger.error(loggerMessage.alreadyExisting.push_notify)
            return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.push_notify)
        }
    } catch (err) {
        logger.error(loggerMessage.errorInCreate.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.push_notify)
    }
}

push_notifyController.get = async (req, res) => {
    try {
        let where = {}
        let totalWhere = {}
        let _start = req.query && req.query._start ? Number(req.query._start) : 0
        let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
        if (req.query && req.query.status) {
            totalWhere['notify_status'] = req.query.status
        };
        if (req.query && req.query.notifyId) {
            totalWhere['notify_id'] = req.query.notifyId
        }
        if (req.query && req.query.search) {
            where = {
                $or: [
                    { noti_title: new RegExp(req.query.search, 'i') },
                    { noti_msg: new RegExp(req.query.search, 'i') }
                ]
            }
        };
        const { rows, count, totalCount } = await push_notifyService.findAllAndCount(where, _start, _limit, totalWhere)
        if (rows && count > 0) {
            logger.info(loggerMessage.getDataSuccess.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.push_notify)
        } else if (rows && count == 0) {
            logger.warn(loggerMessage.noDataFound.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.push_notify)
        } else {
            logger.error(loggerMessage.getDataFailure.push_notify)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.push_notify)
        }
    } catch (error) {
        logger.error(loggerMessage.errorInGetAllData.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.push_notify)
    }
}

push_notifyController.sendNotify = async (req, res) => {
    try {
        const { user_id, body, title, image_url, tokens } = req.body
        const founded = user_id ? await seekerService.deviceToken(user_id) : null
        if ((founded && founded.length > 0) || tokens) {
            let token = founded && founded.length > 0 ? founded : tokens
            const notify = await admin.messaging().sendToDevice(token, {
                notification: {
                    title: title,
                    body: body,
                    color: '#9f0e27',
                    smallIcon: image_url,
                    sound: "default"
                }
            })
            if (notify && notify.successCount > 0) {
                logger.info('Notification Sended Successfully')
                return response.success(req, res, statusCodes.HTTP_OK, notify, 'Notification Sended Successfully')
            } else {
                logger.error('Notification Sending Failure')
                return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, 'Notification Sending Failure')
            }
        } else {
            logger.error(`Given User doesn't have access to send Notification`)
            return response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, `Given User doesn't have access to send Notification`)
        }
    } catch (e) {
        logger.error('Error in Sending Notification')
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, 'Error in Sending Notification')
    }
}

push_notifyController.findByPk = async (req, res) => {
    try {
        let { notify_id } = req.params
        const founded = await push_notifyService.findByPk(notify_id)
        if (founded && founded.notify_id) {
            logger.info(loggerMessage.getIdSuccess.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.push_notify)
        } else {
            logger.info(loggerMessage.notFound.push_notify)
            return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.push_notify)
        }
    } catch (error) {
        logger.error(loggerMessage.errorInFindingId.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.push_notify)
    }
}

push_notifyController.update = async (req, res) => {
    try {
        let { notify_id } = req.params
        const update = await push_notifyService.update(notify_id, req.body)
        if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await push_notifyService.bulkUpdateLastupdate([Number(notify_id)])
            logger.info(loggerMessage.updateDataSuccess.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.push_notify)
        } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
            logger.warn(loggerMessage.alreadyExistingValue.push_notify)
            return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.push_notify)
        } else if (update && update.acknowledged == true && update.matchedCount == 0) {
            logger.error(loggerMessage.notFound.push_notify)
            return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.push_notify)
        } else {
            logger.error(loggerMessage.updateDataFailure.push_notify)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.push_notify)
        }
    } catch (error) {
        logger.error(loggerMessage.errorInUpdating.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.push_notify)
    }
}

push_notifyController.updateStatus = async (req, res) => {
    try {
        let { notify_ids, status } = req.body
        const update = await push_notifyService.bulkUpdateStatus(notify_ids, status)
        if (update && update.acknowledged == true && update.matchedCount == notify_ids.length && update.modifiedCount == notify_ids.length) {
            await push_notifyService.bulkUpdateLastupdate(notify_ids)
            logger.info(loggerMessage.updateStatusSuccess.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.push_notify)
        } else if (update && update.acknowledged == true && update.matchedCount == notify_ids.length && update.modifiedCount < notify_ids.length) {
            logger.warn(loggerMessage.alreadyExistingValue.push_notify)
            return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.push_notify)
        } else if (update && update.acknowledged == true && update.matchedCount < notify_ids.length) {
            logger.error(loggerMessage.notFound.push_notify)
            return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.push_notify)
        } else {
            logger.error(loggerMessage.updateStatusFailure.push_notify)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.push_notify)
        }
    } catch (error) {
        logger.error(loggerMessage.errorInUpdatingStatus.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.push_notify)
    }
}

push_notifyController.delete = async (req, res) => {
    try {
        let { notify_id } = req.query
        const deleted = await push_notifyService.delete(notify_id)
        if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
            logger.info(loggerMessage.deleteSuccess.push_notify)
            return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.push_notify)
        } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
            logger.error(loggerMessage.notFound.push_notify)
            return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.push_notify)
        } else {
            logger.error(loggerMessage.deleteFailure.push_notify)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.push_notify)
        }
    } catch (error) {
        logger.error(loggerMessage.errorInDeleting.push_notify)
        return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.push_notify)
    }
}

module.exports = push_notifyController