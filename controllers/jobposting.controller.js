'use strict';
const { jobpostingService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class JobPostingController { }
JobPostingController.create = async (req, res) => {
   try {
      let payload = req.body;
    
      payload['job_code'] =  req.body.job_code,
      payload['posted_by'] =  req.body.posted_by,
      payload['jcat_id'] =  req.body.jcat_id,
      payload['jsub_id'] =  req.body.jsub_id,
      payload['cont_mail'] =  req.body.cont_mail,
      payload['cont_mob'] =  req.body.cont_mob,
      payload['sal_range'] =  req.body.sal_range,
      payload['indust_id'] =  req.body.indust_id,
      payload['empl_type'] =  req.body.empl_type,
      payload['emp_educ'] =  req.body.emp_educ,
      payload['emp_exp'] =  req.body.emp_exp,
      payload['emp_specal'] =  req.body.emp_specal,
      payload['job_desc'] =  req.body.job_desc,
      payload['posted_type'] =  'Employer',
      payload['job_status'] =  "Y",
      payload['ipaddress'] =  req.body.ipaddress,
      payload['job_expdate'] =  new Date(),
      payload['lastupdate'] =  new Date()
     
      let findwhere = {
         $and: [
            { job_status: { $ne: "D" } },
            {
               $and: [
                  { jcat_id: req.body.jcat_id },
                  { jsub_id: req.body.jsub_id }
               ]
            }
         ]
      };
      const founded = await jobpostingService.findOne(findwhere);
      if (!founded) {
         const created = await jobpostingService.create(payload);
         if (created && created.job_id) {
            logger.info(loggerMessage.createdSuccess.jobPosting);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.jobPosting);
         } else {
            logger.error(loggerMessage.createdFailure.jobPosting);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.jobPosting);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.jobPosting);
   }
};

JobPostingController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['job_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = { job_code: new RegExp(req.query.search) };
      }
      const { rows, count, totalCount } = await jobpostingService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.jobPosting);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.jobPosting);
      } else {
         logger.error(loggerMessage.getDataFailure.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.jobPosting);
   }
};

JobPostingController.findByPk = async (req, res) => {
   try {
      let { job_id } = req.params;
      if (!job_id) throw createError.BadRequest();
      const founded = await jobpostingService.findByPk(job_id);
      if (founded && founded.job_id) {
         logger.info(loggerMessage.getIdSuccess.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.jobPosting);
      } else {
         logger.info(loggerMessage.notFound.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.jobPosting);
   }
};

JobPostingController.update = async (req, res) => {
   try {
      let { job_id } = req.params;
      if (!job_id) throw createError.BadRequest();
      const update = await jobpostingService.update(job_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await jobpostingService.bulkUpdateLastupdate([Number(job_id)]);
         logger.info(loggerMessage.updateDataSuccess.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.jobPosting);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobPosting);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobPosting);
      } else {
         logger.error(loggerMessage.updateDataFailure.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.jobPosting);
   }
};

JobPostingController.updateStatus = async (req, res) => {
   try {
      let { job_ids, status } = req.body;
      const update = await jobpostingService.bulkUpdateStatus(job_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == job_ids.length && update.modifiedCount == job_ids.length) {
         await jobpostingService.bulkUpdateLastupdate(job_ids);
         logger.info(loggerMessage.updateStatusSuccess.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.jobPosting);
      } else if (update && update.acknowledged == true && update.matchedCount == job_ids.length && update.modifiedCount < job_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobPosting);
      } else if (update && update.acknowledged == true && update.matchedCount < job_ids.length) {
         logger.error(loggerMessage.notFound.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobPosting);
      } else {
         logger.error(loggerMessage.updateStatusFailure.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.jobPosting);
   }
};

JobPostingController.delete = async (req, res) => {
   try {
      let { job_id } = req.query;
      if (!job_id) throw createError.BadRequest();
      const deleted = await jobpostingService.delete(job_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.jobPosting);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.jobPosting);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobPosting);
      } else {
         logger.error(loggerMessage.deleteFailure.jobPosting);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.jobPosting);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.jobPosting);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.jobPosting);
   }
};

module.exports = JobPostingController;
