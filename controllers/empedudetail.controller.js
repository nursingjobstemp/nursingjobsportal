'use strict';
const { empedudetailService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class emp_Education_DetailsController { }
emp_Education_DetailsController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] = req.body.emp_id,
      payload['high_qualif'] = req.body.high_qualif,
      payload['high_course'] = req.body.high_course,
      payload['high_special'] = req.body.high_special,
      payload['high_college'] = req.body.high_college,
      payload['colg_name'] = req.body.colg_name,
      payload['high_pass_yr'] = req.body.high_pass_yr,
      payload['edudate'] = new Date()
     
      let findwhere = {
         $and: [
            { high_qualif: req.body.high_qualif },
            { high_course: req.body.high_course },
            { edu_id: req.body.edu_id }
         ]
      };
      const founded = await empedudetailService.findOne(findwhere);
      if (!founded) {
         const created = await empedudetailService.create(payload);
         if (created && created.edu_id) {
            logger.info(loggerMessage.createdSuccess.emp_Education_Details);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.emp_Education_Details);
         } else {
            logger.error(loggerMessage.createdFailure.emp_Education_Details);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.emp_Education_Details);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.emp_Education_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.emp_Education_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.emp_Education_Details);
   }
};

emp_Education_DetailsController.get = async (req, res) => {
   try {
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      const { rows, count, totalCount } = await empedudetailService.findAllAndCount(_start, _limit);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.emp_Education_Details);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.emp_Education_Details);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.emp_Education_Details);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.emp_Education_Details);
      } else {
         logger.error(loggerMessage.getDataFailure.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.emp_Education_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.emp_Education_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.emp_Education_Details);
   }
};

emp_Education_DetailsController.findByPk = async (req, res) => {
   try {
      let { edu_id } = req.params;
      if (!edu_id) throw createError.BadRequest();
      const founded = await empedudetailService.findByPk(edu_id);
      if (founded && founded.edu_id) {
         logger.info(loggerMessage.getIdSuccess.emp_Education_Details);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.emp_Education_Details);
      } else {
         logger.info(loggerMessage.notFound.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.emp_Education_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.emp_Education_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.emp_Education_Details);
   }
};

emp_Education_DetailsController.update = async (req, res) => {
   try {
      let { edu_id } = req.params;
      if (!edu_id) throw createError.BadRequest();
      const update = await empedudetailService.update(edu_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await empedudetailService.bulkUpdateLastupdate([Number(edu_id)]);
         logger.info(loggerMessage.updateDataSuccess.emp_Education_Details);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.emp_Education_Details);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_Education_Details);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Education_Details);
      } else {
         logger.error(loggerMessage.updateDataFailure.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.emp_Education_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.emp_Education_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.emp_Education_Details);
   }
};

emp_Education_DetailsController.delete = async (req, res) => {
   try {
      let { edu_id } = req.query;
      if (!edu_id) throw createError.BadRequest();
      const deleted = await empedudetailService.delete(edu_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.emp_Education_Details);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.emp_Education_Details);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Education_Details);
      } else {
         logger.error(loggerMessage.deleteFailure.emp_Education_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.emp_Education_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.emp_Education_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.emp_Education_Details);
   }
};

module.exports = emp_Education_DetailsController;
