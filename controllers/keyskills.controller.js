'use strict';
const { keyskillsService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class KeySkillsController { }
KeySkillsController.create = async (req, res) => {
   try {
      let payload = req.body;

      payload['keysk_name'] = req.body.keysk_name,
      payload['keysk_slug'] = req.body.keysk_slug,
      payload['keysk_pos'] = req.body.keysk_pos,
      payload['keysk_status'] = "Y",
      payload['keysk_dt'] = new Date(),
      payload['keysk_lastupdate'] = new Date(),
      payload['keysk_code'] = 0
      
      let findwhere = {
         $and: [
            { keysk_status: { $ne: "D" } },
            {
               $or: [
                  { keysk_name: req.body.keysk_name },
                  { keysk_slug: req.body.keysk_slug }
               ]
            }
         ]
      };
      const founded = await keyskillsService.findOne(findwhere);
      if (!founded) {
         const created = await keyskillsService.create(payload);
         if (created && created.keysk_id) {
            logger.info(loggerMessage.createdSuccess.keySkills);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.keySkills);
         } else {
            logger.error(loggerMessage.createdFailure.keySkills);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.keySkills);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.keySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.keySkills);
   }
};

KeySkillsController.get = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { keysk_status: req.query.status } : {};
      let id = req.query && req.query.id ? { keysk_id: Number(req.query.id) } : {};
      let totalWhere = {
         $and: [
            status,
            id
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { keysk_name: new RegExp(req.query.search, 'i') },
               { keysk_slug: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await keyskillsService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.keySkills);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.keySkills);
      } else {
         logger.error(loggerMessage.getDataFailure.keySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.keySkills);
   }
};

KeySkillsController.findByPk = async (req, res) => {
   try {
      let { keysk_id } = req.params;
      if (!keysk_id) throw createError.BadRequest();
      const founded = await keyskillsService.findByPk(keysk_id);
      if (founded && founded.keysk_id) {
         logger.info(loggerMessage.getIdSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.keySkills);
      } else {
         logger.info(loggerMessage.notFound.keySkills);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.keySkills);
   }
};

KeySkillsController.update = async (req, res) => {
   try {
      let { keysk_id } = req.params;
      if (!keysk_id) throw createError.BadRequest();
      const update = await keyskillsService.update(keysk_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await keyskillsService.bulkUpdateLastupdate([Number(keysk_id)]);
         logger.info(loggerMessage.updateDataSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.keySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.keySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.keySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.keySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.keySkills);
      } else {
         logger.error(loggerMessage.updateDataFailure.keySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.keySkills);
   }
};

KeySkillsController.updateStatus = async (req, res) => {
   try {
      let { keysk_ids, status } = req.body;
      const update = await keyskillsService.bulkUpdateStatus(keysk_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == keysk_ids.length && update.modifiedCount == keysk_ids.length) {
         await keyskillsService.bulkUpdateLastupdate(keysk_ids);
         logger.info(loggerMessage.updateStatusSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.keySkills);
      } else if (update && update.acknowledged == true && update.matchedCount == keysk_ids.length && update.modifiedCount < keysk_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.keySkills);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.keySkills);
      } else if (update && update.acknowledged == true && update.matchedCount < keysk_ids.length) {
         logger.error(loggerMessage.notFound.keySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.keySkills);
      } else {
         logger.error(loggerMessage.updateStatusFailure.keySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.keySkills);
   }
};

KeySkillsController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await keyskillsService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.keySkills);
      } else {
         logger.error(loggerMessage.updatePositionFailure.keySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.keySkills);
   }
};

KeySkillsController.delete = async (req, res) => {
   try {
      let { keysk_id } = req.query;
      if (!keysk_id) throw createError.BadRequest();
      const deleted = await keyskillsService.delete(keysk_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.keySkills);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.keySkills);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.keySkills);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.keySkills);
      } else {
         logger.error(loggerMessage.deleteFailure.keySkills);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.keySkills);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.keySkills);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.keySkills);
   }
};

module.exports = KeySkillsController;
