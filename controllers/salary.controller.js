'use strict';
const { salaryService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class SalaryController { }
SalaryController.create = async (req, res) => {
   try {
      let payload = req.body;
      
      payload['sal_name'] = req.body.sal_name,
      payload['sal_slug'] = req.body.sal_slug,
      payload['sal_pos'] = req.body.sal_pos,
      payload['min_sal'] = 0,
      payload['max_sal'] = 0,
      payload['sal_status'] = "Y",
      payload['sal_date'] = new Date(),
      payload['lastupdate'] = new Date()
      
      let findwhere = {
         $and: [
            { sal_status: { $ne: "D" } },
            {
               $or: [
                  { sal_name: req.body.sal_name },
                  { sal_slug: req.body.sal_slug }
               ]
            }
         ]
      };
      const founded = await salaryService.findOne(findwhere);
      if (!founded) {
         const created = await salaryService.create(payload);
         if (created && created.sal_id) {
            logger.info(loggerMessage.createdSuccess.salary);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.salary);
         } else {
            logger.error(loggerMessage.createdFailure.salary);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.salary);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.salary);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.salary);
   }
};

SalaryController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['sal_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { sal_name: new RegExp(req.query.search, 'i') },
               { sal_slug: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await salaryService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.salary);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.salary);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.salary);
      } else {
         logger.error(loggerMessage.getDataFailure.salary);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.salary);
   }
};

SalaryController.findByPk = async (req, res) => {
   try {
      let { sal_id } = req.params;
      if (!sal_id) throw createError.BadRequest();
      const founded = await salaryService.findByPk(sal_id);
      if (founded && founded.sal_id) {
         logger.info(loggerMessage.getIdSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.salary);
      } else {
         logger.info(loggerMessage.notFound.salary);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.salary);
   }
};

SalaryController.update = async (req, res) => {
   try {
      let { sal_id } = req.params;
      if (!sal_id) throw createError.BadRequest();
      const update = await salaryService.update(sal_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await salaryService.bulkUpdateLastupdate([Number(sal_id)]);
         logger.info(loggerMessage.updateDataSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.salary);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.salary);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.salary);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.salary);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.salary);
      } else {
         logger.error(loggerMessage.updateDataFailure.salary);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.salary);
   }
};

SalaryController.updateStatus = async (req, res) => {
   try {
      let { sal_ids, status } = req.body;
      const update = await salaryService.bulkUpdateStatus(sal_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == sal_ids.length && update.modifiedCount == sal_ids.length) {
         await salaryService.bulkUpdateLastupdate(sal_ids);
         logger.info(loggerMessage.updateStatusSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.salary);
      } else if (update && update.acknowledged == true && update.matchedCount == sal_ids.length && update.modifiedCount < sal_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.salary);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.salary);
      } else if (update && update.acknowledged == true && update.matchedCount < sal_ids.length) {
         logger.error(loggerMessage.notFound.salary);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.salary);
      } else {
         logger.error(loggerMessage.updateStatusFailure.salary);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.salary);
   }
};

SalaryController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await salaryService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.salary);
      } else {
         logger.error(loggerMessage.updatePositionFailure.salary);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.salary);
   }
};

SalaryController.delete = async (req, res) => {
   try {
      let { sal_id } = req.query;
      if (!sal_id) throw createError.BadRequest();
      const deleted = await salaryService.delete(sal_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.salary);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.salary);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.salary);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.salary);
      } else {
         logger.error(loggerMessage.deleteFailure.salary);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.salary);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.salary);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.salary);
   }
};

module.exports = SalaryController;
