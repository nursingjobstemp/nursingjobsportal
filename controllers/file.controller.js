'use strict';

const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger, s3 } = require('../helper');

class FileController { }
FileController.create = async (req, res) => {
   try {
      let result = await s3.s3Upload(req);
      logger.info(loggerMessage.file.success);
      return response.success(req, res, statusCodes.HTTP_CREATED, result, responseMessage.file.success);
   }
   catch (error) {
      logger.error(loggerMessage.file.errInUpload);
      return response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, error, responseMessage.file.errInUpload);
   }
  
}

FileController.delete = async (req, res) => {
   try {
      let { images } = req.body;
      let result = await s3.s3Delete(images);
      logger.info(loggerMessage.file.deleted);
      return response.success(req, res, statusCodes.HTTP_CREATED, result, responseMessage.file.deleted);
   }
   catch (error) {
      logger.error(loggerMessage.file.errInDelete);
      return response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, error, responseMessage.file.errInDelete);
   }
}

FileController.resume = async (req, res) => {
   try {
      logger.info(loggerMessage.file.success);
      return response.success(req, res, statusCodes.HTTP_CREATED, req.file.filename, responseMessage.file.success);
   }
   catch (error) {
      logger.error(loggerMessage.file.errInUpload);
      return response.errors(req, res, statusCodes.HTTP_BAD_REQUEST, error, responseMessage.file.errInUpload);
   }
};

module.exports = FileController;
