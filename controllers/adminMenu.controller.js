'use strict'
const { adminMenuService, operatorService } = require('../services')
const { response } = require('../middleware')
const { statusCodes, responseMessage, loggerMessage } = require('../constants')
const { logger } = require('../helper')

class AdminMenuController { }
AdminMenuController.get = async (req, res) => {
   try {
      let where = {
         $and: [
            { menu_status: "Y" },
            { pid: 0 }
         ]
      }
      if (req.user && req.user.type !== "SA") {
         let operator = await operatorService.findByPk(req.user.id)
         where['menu_id'] = { $in: operator.feat_id.split(',').map(Number) }
      };
      const adminMenu = await adminMenuService.findAllAndCount(where)
      if (adminMenu && adminMenu.length > 0) {
         logger.info(loggerMessage.getDataSuccess.adminMenu)
         return response.success(req, res, statusCodes.HTTP_OK, adminMenu, responseMessage.getDataSuccess.adminMenu)
      } else if (adminMenu.length == 0) {
         logger.warn(loggerMessage.noDataFound.adminMenu)
         return response.success(req, res, statusCodes.HTTP_OK, adminMenu, responseMessage.noDataFound.adminMenu)
      } else {
         logger.error(loggerMessage.getDataFailure.adminMenu)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.adminMenu)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.adminMenu)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.adminMenu)
   }
}

AdminMenuController.getAllCount = async (req, res) => {
   try {
      const op_id = req.query && req.query.op_id ? { posted_id: req.query.op_id } : {}
      const data = await adminMenuService.getAllCount(req.query.status, op_id)
      logger.info(loggerMessage.getCount.count)
      return response.success(req, res, statusCodes.HTTP_OK, data, responseMessage.getCount.count)
   } catch (error) {
      logger.error(loggerMessage.errInGetCount.count)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errInGetCount.count)
   }
}

AdminMenuController.getSpecOp = async (req, res) => {
   try {
      const status = req.query && req.query.status ? { posted_status: req.query.status } : {}
      const op_id = req.query && req.query.op_id ? { posted_id: req.query.op_id } : {}
      const data = await adminMenuService.getSpecOp(status, op_id)
      logger.info(loggerMessage.getCount.count)
      return response.success(req, res, statusCodes.HTTP_OK, data, responseMessage.getCount.count)
   } catch (error) {
      logger.error(loggerMessage.errInGetCount.count)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errInGetCount.count)
   }
}

module.exports = AdminMenuController
