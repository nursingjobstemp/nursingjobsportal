'use strict';
const { jobtypeService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class JobTypeController { }
JobTypeController.create = async (req, res) => {
   try {
      let payload = req.body;
        
         payload['jtype_name'] =  req.body.jtype_name,
         payload['jtype_status'] = "Y",
         payload['jtype_pos'] = req.body.jtype_pos,
         payload['jtype_date'] = new Date(),
         payload['lastupdate'] = new Date()
      
      let findwhere = {
         $and: [
            { jtype_status: { $ne: "D" } },
            {
               $and: [
                  { jtype_status: { $ne: "D" } },
                  { jtype_name: req.body.jtype_name }
               ]
            }
         ]
      };
      const founded = await jobtypeService.findOne(findwhere);
      if (!founded) {
         const created = await jobtypeService.create(payload);
         if (created && created.jtype_id) {
            logger.info(loggerMessage.createdSuccess.jobType);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.jobType);
         } else {
            logger.error(loggerMessage.createdFailure.jobType);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.jobType);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.jobType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.jobType);
   }
};

JobTypeController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['jtype_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            jtype_name: new RegExp(req.query.search, 'i')
         };
      };
      const { rows, count, totalCount } = await jobtypeService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.jobType);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.jobType);
      } else {
         logger.error(loggerMessage.getDataFailure.jobType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.jobType);
   }
};

JobTypeController.findByPk = async (req, res) => {
   try {
      let { jtype_id } = req.params;
      if (!jtype_id) throw createError.BadRequest();
      const founded = await jobtypeService.findByPk(jtype_id);
      if (founded && founded.jtype_id) {
         logger.info(loggerMessage.getIdSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.jobType);
      } else {
         logger.info(loggerMessage.notFound.jobType);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.jobType);
   }
};

JobTypeController.update = async (req, res) => {
   try {
      let { jtype_id } = req.params;
      if (!jtype_id) throw createError.BadRequest();
      const update = await jobtypeService.update(jtype_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await jobtypeService.bulkUpdateLastupdate([Number(jtype_id)]);
         logger.info(loggerMessage.updateDataSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.jobType);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.jobType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobType);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.jobType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobType);
      } else {
         logger.error(loggerMessage.updateDataFailure.jobType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.jobType);
   }
};

JobTypeController.updateStatus = async (req, res) => {
   try {
      let { jtype_ids, status } = req.body;
      const update = await jobtypeService.bulkUpdateStatus(jtype_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == jtype_ids.length && update.modifiedCount == jtype_ids.length) {
         await jobtypeService.bulkUpdateLastupdate(jtype_ids);
         logger.info(loggerMessage.updateStatusSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.jobType);
      } else if (update && update.acknowledged == true && update.matchedCount == jtype_ids.length && update.modifiedCount < jtype_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.jobType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobType);
      } else if (update && update.acknowledged == true && update.matchedCount < jtype_ids.length) {
         logger.error(loggerMessage.notFound.jobType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobType);
      } else {
         logger.error(loggerMessage.updateStatusFailure.jobType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.jobType);
   }
};

JobTypeController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await jobtypeService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.jobType);
      } else {
         logger.error(loggerMessage.updatePositionFailure.jobType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.jobType);
   }
};

JobTypeController.delete = async (req, res) => {
   try {
      let { jtype_id } = req.query;
      const deleted = await jobtypeService.delete(jtype_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.jobType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.jobType);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.jobType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobType);
      } else {
         logger.error(loggerMessage.deleteFailure.jobType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.jobType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.jobType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.jobType);
   }
};

module.exports = JobTypeController;
