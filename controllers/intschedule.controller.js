'use strict';
const { intscheduleService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class IntScheduleController { }
IntScheduleController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['applied_id'] = req.body.applied_id,
      payload['emp_id'] = req.body.emp_id,
      payload['company_id'] = req.body.company_id,
      payload['mail_title'] = req.body.mail_title,
      payload['mail_content'] = req.body.mail_content,
      payload['ipaddress'] = req.body.ipaddress,
      payload['intsch_status'] = "Y",
      payload['mail_date'] = new Date(),
      payload['lastupdate'] = new Date()
     
      let findwhere = {
         $and: [
            { status: { $ne: "D" } },
            {
               $and: [
                  { applied_id: req.body.applied_id },
                  { emp_id: req.body.emp_id },
                  { mail_content: req.body.mail_content }
               ]
            }
         ]
      };
      const founded = await intscheduleService.findOne(findwhere);
      if (!founded) {
         const created = await intscheduleService.create(payload);
         if (created && created.intsch_id) {
            logger.info(loggerMessage.createdSuccess.interviewSchedule);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.interviewSchedule);
         } else {
            logger.error(loggerMessage.createdFailure.interviewSchedule);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.interviewSchedule);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.interviewSchedule);
   }
};

IntScheduleController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['intsch_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { mail_title: new RegExp(req.query.search, 'i') },
               { mail_content: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await intscheduleService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.interviewSchedule);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.interviewSchedule);
      } else {
         logger.error(loggerMessage.getDataFailure.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.interviewSchedule);
   }
};

IntScheduleController.findByPk = async (req, res) => {
   try {
      let { intsch_id } = req.params;
      if (!intsch_id) throw createError.BadRequest();
      const founded = await intscheduleService.findByPk(intsch_id);
      if (founded && founded.intsch_id) {
         logger.info(loggerMessage.getIdSuccess.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.interviewSchedule);
      } else {
         logger.info(loggerMessage.notFound.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.interviewSchedule);
   }
};

IntScheduleController.update = async (req, res) => {
   try {
      let { intsch_id } = req.params;
      if (!intsch_id) throw createError.BadRequest();
      const update = await intscheduleService.update(intsch_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await intscheduleService.bulkUpdateLastupdate([Number(intsch_id)]);
         logger.info(loggerMessage.updateDataSuccess.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.interviewSchedule);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.interviewSchedule);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.interviewSchedule);
      } else {
         logger.error(loggerMessage.updateDataFailure.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.interviewSchedule);
   }
};

IntScheduleController.updateStatus = async (req, res) => {
   try {
      let { intsch_ids, status } = req.body;
      const update = await intscheduleService.bulkUpdateStatus(intsch_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == intsch_ids.length && update.modifiedCount == intsch_ids.length) {
         await intscheduleService.bulkUpdateLastupdate(intsch_ids);
         logger.info(loggerMessage.updateStatusSuccess.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.interviewSchedule);
      } else if (update && update.acknowledged == true && update.matchedCount == intsch_ids.length && update.modifiedCount < intsch_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.interviewSchedule);
      } else if (update && update.acknowledged == true && update.matchedCount < intsch_ids.length) {
         logger.error(loggerMessage.notFound.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.interviewSchedule);
      } else {
         logger.error(loggerMessage.updateStatusFailure.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.interviewSchedule);
   }
};

IntScheduleController.delete = async (req, res) => {
   try {
      let { intsch_id } = req.query;
      if (!intsch_id) throw createError.BadRequest();
      const deleted = await intscheduleService.delete(intsch_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.interviewSchedule);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.interviewSchedule);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.interviewSchedule);
      } else {
         logger.error(loggerMessage.deleteFailure.interviewSchedule);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.interviewSchedule);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.interviewSchedule);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.interviewSchedule);
   }
};

module.exports = IntScheduleController;
