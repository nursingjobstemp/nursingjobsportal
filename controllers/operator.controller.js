'use strict'
const { operatorService } = require('../services')
const { response } = require('../middleware')
const { statusCodes, responseMessage, loggerMessage } = require('../constants')
const { logger } = require('../helper')
const createError = require('http-errors')

class OperatorController { }
OperatorController.create = async (req, res) => {
   try {
      let payload = req.body;

      payload['op_type'] = req.body.op_type,
      payload['op_name'] = req.body.op_name,
      payload['op_uname'] = req.body.op_uname,
      payload['op_password'] = req.body.op_password,
      payload['feat_id'] = req.body.feat_id,
      payload['op_status'] = "Y",
      payload['op_dt'] = new Date(),
      payload['op_lastupdate'] = new Date()
    
      let findwhere = {
         $and: [
            { op_status: { $ne: "D" } },
            {
               $or: [
                  { op_name: req.body.op_name },
                  { op_uname: req.body.op_uname }
               ]
            }
         ]
      }
      const founded = await operatorService.findOne(findwhere)
      if (!founded) {
         const created = await operatorService.create(payload)
         if (created && created.op_id) {
            logger.info(loggerMessage.createdSuccess.operator)
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.operator)
         } else {
            logger.error(loggerMessage.createdFailure.operator)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.operator)
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.operator)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.operator)
   }
}

OperatorController.get = async (req, res) => {
   try {
      let where = {}
      let totalWhere = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      if (req.query && req.query.status) {
         totalWhere['op_status'] = req.query.status
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { op_name: new RegExp(req.query.search, 'i') },
               { op_uname: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await operatorService.findAllAndCount(where, _start, _limit, totalWhere)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.operator)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.operator)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.operator)
      } else {
         logger.error(loggerMessage.getDataFailure.operator)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.operator)
   }
}

OperatorController.data = async (req, res) => {
   try {
      let where = {}
      let totalWhere = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      if (req.query && req.query.status) {
         totalWhere['op_status'] = req.query.status
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { op_name: new RegExp(req.query.search, 'i') },
               { op_uname: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await operatorService.data(where, _start, _limit, totalWhere)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.operator)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.operator)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.operator)
      } else {
         logger.error(loggerMessage.getDataFailure.operator)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.operator)
   }
}

OperatorController.findByPk = async (req, res) => {
   try {
      let { op_id } = req.params
      if (!op_id) throw createError.BadRequest()
      const founded = await operatorService.findByPk(op_id)
      if (founded && founded.op_id) {
         logger.info(loggerMessage.getIdSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.operator)
      } else {
         logger.info(loggerMessage.notFound.operator)
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.operator)
   }
}

OperatorController.update = async (req, res) => {
   try {
      let { op_id } = req.params
      if (!op_id) throw createError.BadRequest()
      const update = await operatorService.update(op_id, req.body)
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await operatorService.bulkUpdateLastupdate([Number(op_id)])
         logger.info(loggerMessage.updateDataSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.operator)
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.operator)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.operator)
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.operator)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.operator)
      } else {
         logger.error(loggerMessage.updateDataFailure.operator)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.operator)
   }
}

OperatorController.updateStatus = async (req, res) => {
   try {
      let { op_ids, status } = req.body
      const update = await operatorService.bulkUpdateStatus(op_ids, status)
      if (update && update.acknowledged == true && update.matchedCount == op_ids.length && update.modifiedCount == op_ids.length) {
         await operatorService.bulkUpdateLastupdate(op_ids)
         logger.info(loggerMessage.updateStatusSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.operator)
      } else if (update && update.acknowledged == true && update.matchedCount == op_ids.length && update.modifiedCount < op_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.operator)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.operator)
      } else if (update && update.acknowledged == true && update.matchedCount < op_ids.length) {
         logger.error(loggerMessage.notFound.operator)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.operator)
      } else {
         logger.error(loggerMessage.updateStatusFailure.operator)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.operator)
   }
}

OperatorController.changePassword = async (req, res) => {
   try {
      const founded_op = await operatorService.findOne({ op_uname: req.body.user_name })
      if (founded_op && founded_op.op_id) {
         if (founded_op.op_password === Buffer.from(req.body.old_password).toString("base64")) {
            const update = await operatorService.update(founded_op.op_id, { op_password: Buffer.from(req.body.new_password).toString("base64") })
            if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
               await operatorService.bulkUpdateLastupdate([Number(founded_op.op_id)])
               logger.info(loggerMessage.passwordChangeSuccess.operator)
               return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.passwordChangeSuccess.operator)
            } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
               logger.warn(loggerMessage.exitedPassword.operator)
               return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.exitedPassword.operator)
            } else {
               logger.error(loggerMessage.passwordChangeFailure.operator)
               return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.passwordChangeFailure.operator)
            }
         } else {
            logger.error(loggerMessage.passwordIncorrect.operator)
            return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.passwordIncorrect.operator)
         }
      } else {
         logger.error(loggerMessage.notFound.operator)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.operator)
      }
   } catch (err) {
      logger.error(loggerMessage.errInChangePassword.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errInChangePassword.operator)
   }
}

OperatorController.delete = async (req, res) => {
   try {
      let { op_id } = req.query
      if (!op_id) throw createError.BadRequest()
      const deleted = await operatorService.delete(op_id)
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.operator)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.operator)
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.operator)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.operator)
      } else {
         logger.error(loggerMessage.deleteFailure.operator)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.operator)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.operator)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.operator)
   }
}

module.exports = OperatorController
