'use strict';
const { contactresumeService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class ContactResumeController { }
ContactResumeController.create = async (req, res) => {
   try {
      let payload = req.body;

         payload['emp_id'] = req.body.emp_id,
         payload['comp_id'] = req.body.comp_id,
         payload['cont_type'] = req.body.cont_type,
         payload['ipaddress'] = req.body.ipaddress,
         payload['cont_date'] = new Date(),
         payload['lastupdate'] = new Date()
     
      let findwhere = {
         $and: [
            { emp_id: req.body.emp_id },
            { cont_type: req.body.cont_type }
         ]
      };
      const founded = await contactresumeService.findOne(findwhere);
      if (!founded) {
         const created = await contactresumeService.create(payload);
         if (created && created.cont_id) {
            logger.info(loggerMessage.createdSuccess.contactResume);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.contactResume);
         } else {
            logger.error(loggerMessage.createdFailure.contactResume);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.contactResume);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.contactResume);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.contactResume);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.contactResume);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.contactResume);
   }
};

ContactResumeController.get = async (req, res) => {
   try {
      let cont_type = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.cont_type) {
         cont_type['cont_type'] = req.query.cont_type;
      }
      const { rows, count, totalCount } = await contactresumeService.findAllAndCount(_start, _limit, cont_type);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.contactResume);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.contactResume);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.contactResume);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.contactResume);
      } else {
         logger.error(loggerMessage.getDataFailure.contactResume);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.contactResume);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.contactResume);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.contactResume);
   }
};

ContactResumeController.findByPk = async (req, res) => {
   try {
      let { cont_id } = req.params;
      if (!cont_id) throw createError.BadRequest();
      const founded = await contactresumeService.findByPk(cont_id);
      if (founded && founded.cont_id) {
         logger.info(loggerMessage.getIdSuccess.contactResume);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.contactResume);
      } else {
         logger.info(loggerMessage.notFound.contactResume);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.contactResume);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.contactResume);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.contactResume);
   }
};

ContactResumeController.update = async (req, res) => {
   try {
      let { cont_id } = req.params;
      if (!cont_id) throw createError.BadRequest();
      const update = await contactresumeService.update(cont_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await contactresumeService.bulkUpdateLastupdate([Number(cont_id)]);
         logger.info(loggerMessage.updateDataSuccess.contactResume);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.contactResume);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.contactResume);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.contactResume);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.contactResume);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.contactResume);
      } else {
         logger.error(loggerMessage.updateDataFailure.contactResume);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.contactResume);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.contactResume);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.contactResume);
   }
};

ContactResumeController.delete = async (req, res) => {
   try {
      let { cont_id } = req.query;
      const deleted = await contactresumeService.delete(cont_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.contactResume);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.contactResume);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.contactResume);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.contactResume);
      } else {
         logger.error(loggerMessage.deleteFailure.contactResume);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.contactResume);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.contactResume);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.contactResume);
   }
};

module.exports = ContactResumeController;
