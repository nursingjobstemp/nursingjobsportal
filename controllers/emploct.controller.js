'use strict';
const { emploctService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EmployeLocationController { }
EmployeLocationController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] = req.body.emp_id,
      payload['emp_country'] = req.body.emp_country,
      payload['emp_state'] = req.body.emp_state,
      payload['emp_city'] = req.body.emp_city,
      payload['locat_status'] = "Y",
      payload['ipaddr'] = req.body.ipaddr,
      payload['locat_date'] = new Date(),
      payload['lastupdate'] = new Date()
  
      let findwhere = {
         $and: [
            { locat_status: { $ne: "D" } },
            {
               $and: [
                  { emp_id: req.body.emp_id },
                  { emp_city: req.body.emp_city }
               ]
            }
         ]
      };
      const founded = await emploctService.findOne(findwhere);
      if (!founded) {
         const created = await emploctService.create(payload);
         if (created && created.emplocat_id) {
            logger.info(loggerMessage.createdSuccess.emp_location);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.emp_location);
         } else {
            logger.error(loggerMessage.createdFailure.emp_location);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.emp_location);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.emp_location);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.emp_location);
   }
};

EmployeLocationController.get = async (req, res) => {
   try {
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['locat_status'] = req.query.status;
      };
      const { rows, count, totalCount } = await emploctService.findAllAndCount(_start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.emp_location);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.emp_location);
      } else {
         logger.error(loggerMessage.getDataFailure.emp_location);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.emp_location);
   }
};

EmployeLocationController.findByPk = async (req, res) => {
   try {
      let { emplocat_id } = req.params;
      if (!emplocat_id) throw createError.BadRequest();
      const founded = await emploctService.findByPk(emplocat_id);
      if (founded && founded.emplocat_id) {
         logger.info(loggerMessage.getIdSuccess.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.emp_location);
      } else {
         logger.info(loggerMessage.notFound.emp_location);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.emp_location);
   }
};

EmployeLocationController.update = async (req, res) => {
   try {
      let { emplocat_id } = req.params;
      if (!emplocat_id) throw createError.BadRequest();
      const update = await emploctService.update(emplocat_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await emploctService.bulkUpdateLastupdate([Number(emplocat_id)]);
         logger.info(loggerMessage.updateDataSuccess.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.emp_location);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_location);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_location);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.emp_location);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_location);
      } else {
         logger.error(loggerMessage.updateDataFailure.emp_location);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.emp_location);
   }
};

EmployeLocationController.updateStatus = async (req, res) => {
   try {
      let { emplocat_ids, status } = req.body;
      const update = await emploctService.bulkUpdateStatus(emplocat_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == emplocat_ids.length && update.modifiedCount == emplocat_ids.length) {
         await emploctService.bulkUpdateLastupdate(emplocat_ids);
         logger.info(loggerMessage.updateStatusSuccess.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.emp_location);
      } else if (update && update.acknowledged == true && update.matchedCount == emplocat_ids.length && update.modifiedCount < emplocat_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_location);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_location);
      } else if (update && update.acknowledged == true && update.matchedCount < emplocat_ids.length) {
         logger.error(loggerMessage.notFound.emp_location);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_location);
      } else {
         logger.error(loggerMessage.updateStatusFailure.emp_location);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.emp_location);
   }
};

EmployeLocationController.delete = async (req, res) => {
   try {
      let { emplocat_id } = req.query;
      if (!emplocat_id) throw createError.BadRequest();
      const deleted = await emploctService.delete(emplocat_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.emp_location);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.emp_location);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.emp_location);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_location);
      } else {
         logger.error(loggerMessage.deleteFailure.emp_location);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.emp_location);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.emp_location);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.emp_location);
   }
};

module.exports = EmployeLocationController;
