'use strict';
const { industrytypeService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class IndustryTypeController { }
IndustryTypeController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['indust_name'] = req.body.indust_name,
      payload['indust_slug'] = req.body.indust_slug,
      payload['indust_pos'] = req.body.indust_pos,
      payload['indust_status'] = "Y",
      payload['indust_date'] = new Date(),
      payload['lastupdate'] = new Date()
     
      let findwhere = {
         $and: [
            { indust_status: { $ne: "D" } },
            {
               $or: [
                  { indust_name: req.body.indust_name },
                  { indust_slug: req.body.indust_slug }
               ]
            }
         ]
      };
      const founded = await industrytypeService.findOne(findwhere);
      if (!founded) {
         const created = await industrytypeService.create(payload);
         if (created && created.indust_id) {
            logger.info(loggerMessage.createdSuccess.industryType);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.industryType);
         } else {
            logger.error(loggerMessage.createdFailure.industryType);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.industryType);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.industryType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.industryType);
   }
};

IndustryTypeController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['indust_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { indust_name: new RegExp(req.query.search, 'i') },
               { indust_slug: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await industrytypeService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.industryType);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.industryType);
      } else {
         logger.error(loggerMessage.getDataFailure.industryType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.industryType);
   }
};

IndustryTypeController.findByPk = async (req, res) => {
   try {
      let { indust_id } = req.params;
      if (!indust_id) throw createError.BadRequest();
      const founded = await industrytypeService.findByPk(indust_id);
      if (founded && founded.indust_id) {
         logger.info(loggerMessage.getIdSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.industryType);
      } else {
         logger.info(loggerMessage.notFound.industryType);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.industryType);
   }
};

IndustryTypeController.update = async (req, res) => {
   try {
      let { indust_id } = req.params;
      if (!indust_id) throw createError.BadRequest();
      const update = await industrytypeService.update(indust_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await industrytypeService.bulkUpdateLastupdate([Number(indust_id)]);
         logger.info(loggerMessage.updateDataSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.industryType);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.industryType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.industryType);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.industryType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.industryType);
      } else {
         logger.error(loggerMessage.updateDataFailure.industryType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.industryType);
   }
};

IndustryTypeController.updateStatus = async (req, res) => {
   try {
      let { indust_ids, status } = req.body;
      if (!indust_ids) throw createError.BadRequest();
      const update = await industrytypeService.bulkUpdateStatus(indust_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == indust_ids.length && update.modifiedCount == indust_ids.length) {
         await industrytypeService.bulkUpdateLastupdate(indust_ids);
         logger.info(loggerMessage.updateStatusSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.industryType);
      } else if (update && update.acknowledged == true && update.matchedCount == indust_ids.length && update.modifiedCount < indust_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.industryType);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.industryType);
      } else if (update && update.acknowledged == true && update.matchedCount < indust_ids.length) {
         logger.error(loggerMessage.notFound.industryType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.industryType);
      } else {
         logger.error(loggerMessage.updateStatusFailure.industryType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.industryType);
   }
};

IndustryTypeController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await industrytypeService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.industryType);
      } else {
         logger.error(loggerMessage.updatePositionFailure.industryType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.industryType);
   }
};

IndustryTypeController.delete = async (req, res) => {
   try {
      let { indust_id } = req.query;
      const deleted = await industrytypeService.delete(indust_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.industryType);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.industryType);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.industryType);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.industryType);
      } else {
         logger.error(loggerMessage.deleteFailure.industryType);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.industryType);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.industryType);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.industryType);
   }
};

module.exports = IndustryTypeController;
