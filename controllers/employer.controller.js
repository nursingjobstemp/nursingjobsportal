'use strict';
const { employerService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EmployerController { };
EmployerController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['comp_name'] = req.body.comp_name,
         payload['comp_slug'] = req.body.comp_slug,
         payload['mail_id'] = req.body.mail_id,
         payload['mobile_no'] = req.body.mobile_no,
         payload['email_otp'] = req.body.email_otp,
         payload['email_verify'] = req.body.email_verify,
         payload['mobile_otp'] = req.body.mobile_otp,
         payload['mobile_verify'] = req.body.mobile_verify,
         payload['comp_logo'] = req.body.comp_logo,
         payload['comp_pass'] = req.body.comp_pass,
         payload['cont_person'] = req.body.cont_person,
         payload['indust_id'] = req.body.indust_id,
         payload['recut_type'] = req.body.recut_type,
         payload['country_id'] = req.body.country_id,
         payload['state_id'] = req.body.state_id,
         payload['city_id'] = req.body.city_id,
         payload['pincode'] = req.body.pincode,
         payload['emp_strength'] = req.body.emp_strength,
         payload['recut_desc'] = req.body.recut_desc,
         payload['facebook'] = req.body.facebook,
         payload['twitter'] = req.body.twitter,
         payload['linkedin'] = req.body.linkedin,
         payload['google'] = req.body.google,
         payload['recut_status'] = "Y",
         payload['user_type'] = "C",
         payload['recut_address'] = req.body.recut_address,
         payload['ipaddress'] = req.body.ipaddress,
         payload['recut_date'] = new Date(),
         payload['lastupdate'] = new Date();

      let findWhere = {
         $and: [
            { recut_status: { $ne: "D" } },
            {
               $or: [
                  { mail_id: req.body.mail_id },
                  { mobile_no: req.body.mobile_no }
               ]
            }
         ]
      };
      const founded = await employerService.findOne(findWhere);
      if (!founded) {
         const created = await employerService.create(payload);
         if (created && created.recut_id) {
            logger.info(loggerMessage.createdSuccess.employer);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.employer);
         } else {
            logger.error(loggerMessage.createdFailure.employer);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.employer);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.employer);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.employer);
   }
};

EmployerController.get = async (req, res) => {
   try {
      let where = {};
      let dateRange = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { recut_status: req.query.status } : {};
      let cityId = req.query && req.query.cityId ? { city_id: Number(req.query.cityId) } : {};
      let stateId = req.query && req.query.stateId ? { state_id: Number(req.query.stateId) } : {};
      let countryId = req.query && req.query.countryId ? { country_id: Number(req.query.countryId) } : {};
      let countryFilter = {};
      if (req.query && req.query.countryFilter) {
         if (req.query.countryFilter === 'I') {
            countryFilter = { country_id: 100 };
         } else if (req.query.countryFilter === 'F') {
            countryFilter = { country_id: { $ne: 100 } };
         } else {
            countryFilter = {};
         }
      }
      if (req.query && req.query.dateRange) {
         dateRange = {
            recut_date: {
               $gte: new Date(req.query.dateRange.split(',')[0]),
               $lte: new Date(req.query.dateRange.split(',')[1])
            }
         };
      }
      let totalWhere = {
         $and: [
            status,
            dateRange,
            cityId,
            stateId,
            countryId,
            countryFilter
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { comp_name: new RegExp(req.query.search, 'i') },
               { mail_id: new RegExp(req.query.search, 'i') },
               { mobile_no: new RegExp(req.query.search, 'i') },
               { pincode: new RegExp(req.query.search, 'i') },
               { 'cityData.city_name': new RegExp(req.query.search, 'i') }
            ]
         };
      }
      const { rows, count, totalCount } = await employerService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.employer);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.employer);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.employer);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.employer);
      } else {
         logger.error(loggerMessage.getDataFailure.employer);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.employer);
   }
};

EmployerController.findByPk = async (req, res) => {
   try {
      let { recut_id } = req.params;
      if (!recut_id) throw createError.BadRequest();
      const founded = await employerService.findByPk(recut_id);
      if (founded && founded.recut_id) {
         logger.info(loggerMessage.getDataSuccess.employer);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getDataSuccess.employer);
      } else {
         logger.info(loggerMessage.notFound.employer);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.employer);
   }
};

EmployerController.update = async (req, res) => {
   try {
      let { recut_id } = req.params;
      if (!recut_id) throw createError.BadRequest();
      const update = await employerService.update(recut_id, req.body);
      if (update && update.acknowledged == true && update.modifiedCount == 1) {
         await employerService.bulkUpdateLastupdate([Number(recut_id)]);
         logger.info(loggerMessage.updateDataSuccess.employer);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.employer);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.employer);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.employer);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.employer);
         return response.errors(req, res, statusCodes.HTTP_FOUND, responseMessage.notFound.employer);
      } else {
         logger.error(loggerMessage.updateDataFailure.employer);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.employer);
   }
};

EmployerController.updateStatus = async (req, res) => {
   try {
      let { recut_ids, status } = req.body;
      const update = await employerService.bulkUpdateStatus(recut_ids, status);
      if (update && update.acknowledged == true && update.modifiedCount == recut_ids.length) {
         await employerService.bulkUpdateLastupdate(recut_ids);
         logger.info(loggerMessage.updateStatusSuccess.employer);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.employer);
      } else if (update && update.acknowledged == true && update.matchedCount == recut_ids.length && update.modifiedCount < recut_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.employer);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.employer);
      } else if (update && update.acknowledged == true && update.matchedCount < recut_ids.length) {
         logger.error(loggerMessage.notFound.employer);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.employer);
      } else {
         logger.error(loggerMessage.updateStatusFailure.employer);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.employer);
   }
};

EmployerController.delete = async (req, res) => {
   try {
      let { recut_id } = req.query;
      const deleted = await employerService.delete(recut_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.employer);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.employer);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.employer);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.employer);
      } else {
         logger.error(loggerMessage.deleteFailure.employer);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.employer);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.employer);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.employer);
   }
};

module.exports = EmployerController;