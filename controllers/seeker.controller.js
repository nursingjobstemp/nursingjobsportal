'use strict'
const { seekerService } = require('../services')
const { empkskillsService } = require('../services')
const { response } = require('../middleware')
const { statusCodes, responseMessage, loggerMessage } = require('../constants')
const { logger } = require('../helper')
const createError = require('http-errors')

class SeekerController { }
SeekerController.create = async (req, res) => {
   try {
      //    let findWhere = {
      //       $and: [
      //          { emp_status: { $ne: "D" } },
      //          {
      //             $or: [
      //                { emp_mobile: req.body.emp_mobile },
      //                { emp_email: req.body.emp_email }
      //             ]
      //          }
      //       ]
      //    };
      //    const founded = await seekerService.findOne(findWhere);
      //    if (!founded) {
      let payload = req.body
      payload['emp_name'] = req.body.emp_name,
         payload['emp_email'] = req.body.emp_email,
         payload['emp_mobile'] = req.body.emp_mobile,
         payload['emp_pass'] = req.body.emp_pass,
         payload['email_otp'] = req.body.email_otp,
         payload['email_verify'] = req.body.email_verify,
         payload['mobile_otp'] = req.body.mobile_otp,
         payload['mobile_verify'] = req.body.mobile_verify,
         payload['emp_dob'] = req.body.emp_dob,
         payload['emp_gender'] = req.body.emp_gender,
         payload['emp_cat'] = req.body.emp_cat,
         payload['emp_subcat'] = req.body.emp_subcat,
         payload['cat_name'] = req.body.cat_name,
         payload['subcat_name'] = req.body.subcat_name,
         payload['emp_desig'] = req.body.emp_desig,
         payload['emp_typeval'] = req.body.emp_typeval,
         payload['emp_comp'] = req.body.emp_comp,
         payload['min_sal'] = req.body.min_sal,
         payload['sal_lakh'] = req.body.sal_lakh,
         payload['sal_thousands'] = req.body.sal_thousands,
         payload['exp_year'] = req.body.exp_year,
         payload['exp_month'] = req.body.exp_month,
         payload['emp_photo'] = req.body.emp_photo,
         payload['emp_resumeheadline'] = req.body.emp_resumeheadline,
         payload['emp_country'] = req.body.emp_country,
         payload['emp_state'] = req.body.emp_state,
         payload['emp_city'] = req.body.emp_city,
         payload['city_name'] = req.body.city_name,
         payload['high_qualif'] = req.body.high_qualif,
         payload['high_course'] = req.body.high_course,
         payload['high_special'] = req.body.high_special,
         payload['high_college'] = req.body.high_college,
         payload['colg_name'] = req.body.colg_name,
         payload['course_type'] = req.body.course_type,
         payload['exp_type'] = req.body.exp_type,
         payload['high_pass_yr'] = req.body.high_pass_yr,
         payload['emp_resume'] = req.body.emp_resume,
         payload['emp_pincode'] = req.body.emp_pincode,
         payload['emp_marital'] = req.body.emp_marital,
         payload['emp_address'] = req.body.emp_address,
         payload['user_type'] = req.body.user_type,
         payload['ipaddress'] = req.body.ipaddress,
         payload['emp_status'] = "W",
         payload['emp_date'] = new Date(),
         payload['lastupdate'] = new Date()

      await seekerService.skillsUpdateEmployee(req.body.skills, req.user.id)
      await seekerService.employmentUpdateEmployee(req.body.offdetails, req.user.id)
      await seekerService.locationUpdateEmployee(req.body.location, req.user.id)
      await seekerService.educationUpdateEmployee(req.body.education, req.user.id)

      const created = await seekerService.create(payload)
      if (created && created.emp_id) {
         logger.info(loggerMessage.createdSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.seeker)
      } else {
         logger.error(loggerMessage.createdFailure.seeker)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.seeker)
      }
      // } else {
      //    logger.warn(loggerMessage.alreadyExisting.seeker);
      //    return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.seeker);
      // }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.seeker)
   }
}

SeekerController.get = async (req, res) => {
   try {
      let where = {}
      let dateRange = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      let status = req.query && req.query.status ? { emp_status: req.query.status } : {}
      let cityId = req.query && req.query.cityId ? { emp_city: Number(req.query.cityId) } : {}
      let stateId = req.query && req.query.stateId ? { emp_state: Number(req.query.stateId) } : {}
      let countryId = req.query && req.query.countryId ? { emp_country: Number(req.query.countryId) } : {}
      let mainCatId = req.query && req.query.mainCatId ? { emp_cat: Number(req.query.mainCatId) } : {}
      let subCatId = req.query && req.query.subCatId ? { emp_subcat: Number(req.query.subCatId) } : {}
      if (req.query && req.query.dateRange) {
         dateRange = {
            emp_date: {
               $gte: new Date(req.query.dateRange.split(',')[0]),
               $lte: new Date(req.query.dateRange.split(',')[1])
            }
         }
      }
      let totalWhere = {
         $and: [
            status,
            dateRange,
            cityId,
            stateId,
            countryId,
            mainCatId,
            subCatId
         ]
      }
      if (req.query && req.query.search) {
         where = {
            $or: [
               { emp_name: new RegExp(req.query.search, 'i') },
               { emp_email: new RegExp(req.query.search, 'i') },
               { emp_mobile: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const data = await seekerService.cityNotMatched(where, _start, _limit, totalWhere)
      console.log({ data })
      if (data) {
         logger.info(loggerMessage.getDataSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_OK, { data }, responseMessage.getDataSuccess.seeker)
         // } else if (rows && count == 0) {
         //    logger.warn(loggerMessage.noDataFound.seeker)
         //    return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.seeker)
      } else {
         logger.error(loggerMessage.getDataFailure.seeker)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.seeker)
      }
   } catch (error) {
      console.log({ error })
      logger.error(loggerMessage.errorInGetAllData.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.seeker)
   }
}

SeekerController.findByPk = async (req, res) => {
   try {
      let { emp_id } = req.params
      if (!emp_id) throw createError.BadRequest()
      const founded = await seekerService.findByPk(emp_id)
      if (founded && founded.emp_id) {
         logger.info(loggerMessage.getIdSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.seeker)
      } else {
         logger.info(loggerMessage.notFound.seeker)
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.seeker)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.seeker)
   }
}

SeekerController.update = async (req, res) => {
   try {
      let { emp_id } = req.params
      if (!emp_id) throw createError.BadRequest()
      const update = await seekerService.update(emp_id, req.body)
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await seekerService.bulkUpdateLastupdate([Number(emp_id)])
         logger.info(loggerMessage.updateDataSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.seeker)
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.seeker)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.seeker)
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.seeker)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.seeker)
      } else {
         logger.error(loggerMessage.updateDataFailure.seeker)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.seeker)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.seeker)
   }
}

SeekerController.updateStatus = async (req, res) => {
   try {
      let { emp_ids, status } = req.body
      const update = await seekerService.bulkUpdateStatus(emp_ids, status)
      if (update && update.acknowledged == true && update.matchedCount == emp_ids.length && update.modifiedCount == emp_ids.length) {
         await seekerService.bulkUpdateLastupdate(emp_ids)
         logger.info(loggerMessage.updateStatusSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.seeker)
      } else if (update && update.acknowledged == true && update.matchedCount == emp_ids.length && update.modifiedCount < emp_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.seeker)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.seeker)
      } else if (update && update.acknowledged == true && update.matchedCount < emp_ids.length) {
         logger.error(loggerMessage.notFound.seeker)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.seeker)
      } else {
         logger.error(loggerMessage.updateStatusFailure.seeker)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.seeker)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.seeker)
   }
}

SeekerController.delete = async (req, res) => {
   try {
      let { emp_id } = req.query
      const deleted = await seekerService.delete(emp_id)
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.seeker)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.seeker)
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.seeker)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.seeker)
      } else {
         logger.error(loggerMessage.deleteFailure.seeker)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.seeker)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.seeker)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.seeker)
   }
}

module.exports = SeekerController