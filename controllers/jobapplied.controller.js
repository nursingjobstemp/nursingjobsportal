'use strict';
const { jobappliedService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class JobAppliedController { }
JobAppliedController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] =req.body.emp_id,
      payload['job_id'] = req.body.job_id,
      payload['company_id'] = req.body.company_id,
      payload['job_type'] = req.body.job_type,
      payload['appl_status'] = "W",
      payload['ipaddress'] = req.body.ipaddress,
      payload['applied_date'] = new Date(),
      payload['lastupdate'] = new Date()
    
      let findwhere = {};
      if (req.body && req.body.emp_id) {
         findwhere = {
            $and: [
               { appl_status: { $ne: "D" } },
               {
                  $and: [
                     { emp_id: req.body.emp_id },
                     { job_id: req.body.job_id }
                  ]
               }
            ]
         };
      } else {
         findwhere = {
            $and: [
               { appl_status: { $ne: "D" } },
               {
                  $and: [
                     { company_id: req.body.company_id },
                     { job_id: req.body.job_id }
                  ]
               }
            ]
         };
      }
      const founded = await jobappliedService.findOne(findwhere);
      if (!founded) {
         const created = await jobappliedService.create(payload);
         if (created && created.applied_id) {
            logger.info(loggerMessage.createdSuccess.jobApplied);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.jobApplied);
         } else {
            logger.error(loggerMessage.createdFailure.jobApplied);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.jobApplied);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.jobApplied);
   }
};

JobAppliedController.get = async (req, res) => {
   try {
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['appl_status'] = req.query.status;
      };
      const { rows, count, totalCount } = await jobappliedService.findAllAndCount(_start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.jobApplied);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.jobApplied);
      } else {
         logger.error(loggerMessage.getDataFailure.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.jobApplied);
   }
};

JobAppliedController.findByPk = async (req, res) => {
   try {
      let { applied_id } = req.params;
      if (!applied_id) throw createError.BadRequest();
      const founded = await jobappliedService.findByPk(applied_id);
      if (founded && founded.applied_id) {
         logger.info(loggerMessage.getIdSuccess.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.jobApplied);
      } else {
         logger.info(loggerMessage.notFound.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.jobApplied);
   }
};

JobAppliedController.update = async (req, res) => {
   try {
      let { applied_id } = req.params;
      if (!applied_id) throw createError.BadRequest();
      const update = await jobappliedService.update(applied_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await jobappliedService.bulkUpdateLastupdate([Number(applied_id)]);
         logger.info(loggerMessage.updateDataSuccess.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.jobApplied);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobApplied);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobApplied);
      } else {
         logger.error(loggerMessage.updateDataFailure.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.jobApplied);
   }
};

JobAppliedController.updateStatus = async (req, res) => {
   try {
      let { applied_ids, status } = req.body;
      const update = await jobappliedService.bulkUpdateStatus(applied_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == applied_ids.length && update.modifiedCount == applied_ids.length) {
         await jobappliedService.bulkUpdateLastupdate(applied_ids);
         logger.info(loggerMessage.updateStatusSuccess.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.jobApplied);
      } else if (update && update.acknowledged == true && update.matchedCount == applied_ids.length && update.modifiedCount < applied_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobApplied);
      } else if (update && update.acknowledged == true && update.matchedCount < applied_ids.length) {
         logger.error(loggerMessage.notFound.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobApplied);
      } else {
         logger.error(loggerMessage.updateStatusFailure.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.jobApplied);
   }
};

JobAppliedController.delete = async (req, res) => {
   try {
      let { applied_id } = req.query;
      if (!applied_id) throw createError.BadRequest();
      const deleted = await jobappliedService.delete(applied_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.jobApplied);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.jobApplied);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobApplied);
      } else {
         logger.error(loggerMessage.deleteFailure.jobApplied);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.jobApplied);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.jobApplied);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.jobApplied);
   }
};

module.exports = JobAppliedController;
