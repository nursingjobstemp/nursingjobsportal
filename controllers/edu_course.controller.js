'use strict'
const { edu_courseService } = require('../services')
const { response } = require('../middleware')
const { statusCodes, responseMessage, loggerMessage } = require('../constants')
const { logger } = require('../helper')
const createError = require('http-errors')

class Edu_CourseController { }
Edu_CourseController.create = async (req, res) => {
   try {

      let payload = req.body;
      payload['pid'] = req.body.pid,
      payload['ecatid_sub'] = req.body.ecatid_sub,
      payload['ecat_type'] = req.body.ecat_type,
      payload['ecat_name'] = req.body.ecat_name,
      payload['ecat_slug'] = req.body.ecat_slug,
      payload['ecat_pos'] = req.body.ecat_pos,
      payload['ecat_status'] = "Y",
      payload['ecat_dt'] = new Date(),
      payload['gcat_lastupdate'] = new Date()
   
      let findwhere = {
         $and: [
            { ecat_status: { $ne: "D" } },
            {
               $or: [
                  { ecat_name: req.body.ecat_name },
                  { ecat_slug: req.body.ecat_slug }
               ]
            }
         ]
      }
      const founded = await edu_courseService.findOne(findwhere)
      if (!founded) {
         const created = await edu_courseService.create(payload)
         if (created && created.ecat_id) {
            logger.info(loggerMessage.createdSuccess.educationCourse)
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.educationCourse)
         } else {
            logger.error(loggerMessage.createdFailure.educationCourse)
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.educationCourse)
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.educationCourse)
   }
}

Edu_CourseController.get = async (req, res) => {
   try {
      let where = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      let status = req.query && req.query.status ? { ecat_status: req.query.status } : {}
      let mastCatId = req.query && req.query.mastCatId ? { ecat_id: Number(req.query.mastCatId) } : {}
      let mainCatId = req.query && req.query.mainCatId ? { pid: Number(req.query.mainCatId) } : {}
      let subCatId = req.query && req.query.subCatId ? { ecatid_sub: Number(req.query.subCatId) } : {}
      let which = req.query && (req.query.mainCatId || req.query.subsCatId) ? true : false
      if (!req.query.mastCatId && !req.query.mainCatId && !req.query.subCatId) mastCatId = { ecat_type: 'M', pid: 0 }
      let totalWhere = {
         $and: [
            mastCatId,
            mainCatId,
            subCatId,
            status
         ]
      }
      if (req.query && req.query.search) {
         where = {
            $or: [
               { ecat_name: new RegExp(req.query.search, 'i') },
               { ecat_slug: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await edu_courseService.findAllAndCount(where, _start, _limit, totalWhere, which)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.educationCourse)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.educationCourse)
      } else {
         logger.error(loggerMessage.getDataFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.educationCourse)
   }
}

Edu_CourseController.getMastcat = async (req, res) => {
   try {
      let where = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      let status = req.query && req.query.status ? { ecat_status: req.query.status } : {}
      let mastCatId = req.query && req.query.mastCatId ? { ecat_id: Number(req.query.mastCatId) } : {}
      let totalWhere = {
         $and: [
            { ecat_type: 'M' },
            mastCatId,
            status
         ]
      }
      if (req.query && req.query.search) {
         where = {
            $or: [
               { ecat_name: new RegExp(req.query.search, 'i') },
               { ecat_slug: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await edu_courseService.findAllMast(where, _start, _limit, totalWhere)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.educationCourse)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.educationCourse)
      } else {
         logger.error(loggerMessage.getDataFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.educationCourse)
   }
}

Edu_CourseController.getMaincat = async (req, res) => {
   try {
      let where = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      let status = req.query && req.query.status ? { ecat_status: req.query.status } : {}
      let mainCatId = req.query && req.query.mainCatId ? { pid: Number(req.query.mainCatId) } : {}
      let totalWhere = {
         $and: [
            { ecat_type: 'C' },
            mainCatId,
            status,
         ]
      }
      if (req.query && req.query.search) {
         where = {
            $or: [
               { ecat_name: new RegExp(req.query.search, 'i') },
               { ecat_slug: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await edu_courseService.findAllMain(where, _start, _limit, totalWhere, mainCatId)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.educationCourse)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.educationCourse)
      } else {
         logger.error(loggerMessage.getDataFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.educationCourse)
   }
}

Edu_CourseController.getSubcat = async (req, res) => {
   try {
      let where = {}
      let _start = req.query && req.query._start ? Number(req.query._start) : 0
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10
      let status = req.query && req.query.status ? { ecat_status: req.query.status } : {}
      let subCatId = req.query && req.query.subCatId ? { ecatid_sub: Number(req.query.subCatId) } : {}
      let totalWhere = {
         $and: [
            { ecat_type: 'S' },
            status,
            subCatId,
         ]
      }
      if (req.query && req.query.search) {
         where = {
            $or: [
               { ecat_name: new RegExp(req.query.search, 'i') },
               { ecat_slug: new RegExp(req.query.search, 'i') }
            ]
         }
      };
      const { rows, count, totalCount } = await edu_courseService.findAllSub(where, _start, _limit, totalWhere)
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.educationCourse)
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.educationCourse)
      } else {
         logger.error(loggerMessage.getDataFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.educationCourse)
   }
}

Edu_CourseController.findByPk = async (req, res) => {
   try {
      let { ecat_id } = req.params
      if (!ecat_id) throw createError.BadRequest()
      const founded = await edu_courseService.findByPk(ecat_id)
      if (founded && founded.ecat_id) {
         logger.info(loggerMessage.getIdSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.educationCourse)
      } else {
         logger.info(loggerMessage.notFound.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.educationCourse)
   }
}

Edu_CourseController.update = async (req, res) => {
   try {
      let { ecat_id } = req.params
      if (!ecat_id) throw createError.BadRequest()
      const update = await edu_courseService.update(ecat_id, req.body)
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await edu_courseService.bulkUpdateLastupdate([Number(ecat_id)])
         logger.info(loggerMessage.updateDataSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.educationCourse)
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.educationCourse)
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.educationCourse)
      } else {
         logger.error(loggerMessage.updateDataFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.educationCourse)
   }
}

Edu_CourseController.updateStatus = async (req, res) => {
   try {
      let { ecat_ids, status } = req.body
      const update = await edu_courseService.bulkUpdateStatus(ecat_ids, status)
      if (update && update.acknowledged == true && update.matchedCount == ecat_ids.length && update.modifiedCount == ecat_ids.length) {
         await edu_courseService.bulkUpdateLastupdate(ecat_ids)
         logger.info(loggerMessage.updateStatusSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.educationCourse)
      } else if (update && update.acknowledged == true && update.matchedCount == ecat_ids.length && update.modifiedCount < ecat_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.educationCourse)
      } else if (update && update.acknowledged == true && update.matchedCount < ecat_ids.length) {
         logger.error(loggerMessage.notFound.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.educationCourse)
      } else {
         logger.error(loggerMessage.updateStatusFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.educationCourse)
   }
}

Edu_CourseController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body
      const update = await edu_courseService.bulkUpdatePosition(positionArray)
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.educationCourse)
      } else {
         logger.error(loggerMessage.updatePositionFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.educationCourse)
   }
}

Edu_CourseController.delete = async (req, res) => {
   try {
      let { ecat_id } = req.query
      if (!ecat_id) throw createError.BadRequest()
      const deleted = await edu_courseService.delete(ecat_id)
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.educationCourse)
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.educationCourse)
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.educationCourse)
      } else {
         logger.error(loggerMessage.deleteFailure.educationCourse)
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.educationCourse)
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.educationCourse)
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.educationCourse)
   }
}

module.exports = Edu_CourseController
