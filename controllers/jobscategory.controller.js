'use strict';
const { jobscategoryService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class JobsCategoryController { }
JobsCategoryController.create = async (req, res) => {
   try {
      let payload = req.body;

      payload['pid'] =  0,
      payload['jcat_name'] =  req.body.jcat_name,
      payload['jcat_slug'] =  req.body.jcat_slug,
      payload['jcat_code'] = req.body.jcat_code,
      payload['jcat_icon'] =  req.body.jcat_icon,
      payload['jcat_desc'] =  req.body.jcat_desc ? req.body.jcat_desc : null,
      payload['jcat_pos'] =  req.body.jcat_pos,
      payload['jcat_image'] =  req.body.jcat_image ? req.body.jcat_image : null,
      payload['jcat_status'] =  "Y",
      payload['foot_status'] =  "N",
      payload['jcat_dt'] =  new Date(),
      payload['jcat_lastupdate'] =  new Date()
    
      let findwhere = {
         $and: [
            { jcat_status: { $ne: "D" } },
            {
               $or: [
                  { jcat_name: req.body.jcat_name },
                  { jcat_slug: req.body.jcat_slug }
               ]
            }
         ]
      };
      const founded = await jobscategoryService.findOne(findwhere);
      if (!founded) {
         const created = await jobscategoryService.create(payload);
         if (created && created.jcat_id) {
            logger.info(loggerMessage.createdSuccess.jobsCategory);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.jobsCategory);
         } else {
            logger.error(loggerMessage.createdFailure.jobsCategory);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.jobsCategory);
         }
      } else {
         logger.error(loggerMessage.alreadyExisting.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.jobsCategory);
   }
};

JobsCategoryController.get = async (req, res) => {
   try {
      let where = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      let status = req.query && req.query.status ? { jcat_status: req.query.status } : {};
      let mainCategory = req.query && req.query.mainCatId ? { jcat_id: Number(req.query.mainCatId) } : {};
      let subCategory = req.query && req.query.subCatId ? { pid: Number(req.query.subCatId) } : { pid: 0 };
      let sortBy = req.query && (req.query.mainCatId || req.query.subCatId) ? { jcat_name: 1 } : { jcat_pos: 1, jcat_name: 1 };
      let totalWhere = {
         $and: [
            status,
            mainCategory,
            subCategory
         ]
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { jcat_name: new RegExp(req.query.search, 'i') },
               { jcat_code: new RegExp(req.query.search, 'i') },
               { jcat_slug: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await jobscategoryService.findAllAndCount(where, _start, _limit, totalWhere, sortBy);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.jobsCategory);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.jobsCategory);
      } else {
         logger.error(loggerMessage.getDataFailure.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.jobsCategory);
   }
};

JobsCategoryController.findByPk = async (req, res) => {
   try {
      let { jcat_id } = req.params;
      if (!jcat_id) throw createError.BadRequest();
      const founded = await jobscategoryService.findByPk(jcat_id);
      if (founded && founded.jcat_id) {
         logger.info(loggerMessage.getIdSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.jobsCategory);
      } else {
         logger.info(loggerMessage.notFound.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.jobsCategory);
   }
};

JobsCategoryController.update = async (req, res) => {
   try {
      let { jcat_id } = req.params;
      if (!jcat_id) throw createError.BadRequest();
      const update = await jobscategoryService.update(jcat_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await jobscategoryService.bulkUpdateLastupdate([Number(jcat_id)]);
         logger.info(loggerMessage.updateDataSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.jobsCategory);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobsCategory);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobsCategory);
      } else {
         logger.error(loggerMessage.updateDataFailure.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.jobsCategory);
   }
};

JobsCategoryController.updateStatus = async (req, res) => {
   try {
      let { jcat_ids, status } = req.body;
      const update = await jobscategoryService.bulkUpdateStatus(jcat_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == jcat_ids.length && update.modifiedCount == jcat_ids.length) {
         await jobscategoryService.bulkUpdateLastupdate(jcat_ids);
         logger.info(loggerMessage.updateStatusSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.jobsCategory);
      } else if (update && update.acknowledged == true && update.matchedCount == jcat_ids.length && update.modifiedCount < jcat_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.jobsCategory);
      } else if (update && update.acknowledged == true && update.matchedCount < jcat_ids.length) {
         logger.error(loggerMessage.notFound.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobsCategory);
      } else {
         logger.error(loggerMessage.updateStatusFailure.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.jobsCategory);
   }
};

JobsCategoryController.updatePosition = async (req, res) => {
   try {
      let { positionArray } = req.body;
      const update = await jobscategoryService.bulkUpdatePosition(positionArray);
      if (update) {
         logger.info(loggerMessage.updatePositionSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updatePositionSuccess.jobsCategory);
      } else {
         logger.error(loggerMessage.updatePositionFailure.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updatePositionFailure.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingPosition.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingPosition.jobsCategory);
   }
};

JobsCategoryController.delete = async (req, res) => {
   try {
      let { jcat_id } = req.query;
      if (!jcat_id) throw createError.BadRequest();
      const deleted = await jobscategoryService.delete(jcat_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.jobsCategory);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.jobsCategory);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.jobsCategory);
      } else {
         logger.error(loggerMessage.deleteFailure.jobsCategory);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.jobsCategory);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.jobsCategory);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.jobsCategory);
   }
};

module.exports = JobsCategoryController;