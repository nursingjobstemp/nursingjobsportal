'use strict';
const { empoffdetailsService } = require('../services');
const { response } = require('../middleware');
const { statusCodes, responseMessage, loggerMessage } = require('../constants');
const { logger } = require('../helper');
const createError = require('http-errors');

class EmpOffDetailsController { }
EmpOffDetailsController.create = async (req, res) => {
   try {
      let payload = req.body;
      payload['emp_id'] =  req.body.emp_id,
      payload['emp_desig'] =  req.body.emp_desig,
      payload['emp_org'] =  req.body.emp_org,
      payload['exp_yr'] =  req.body.exp_yr,
      payload['cur_comp'] =  req.body.cur_comp,
      payload['exp_month'] =  req.body.exp_month,
      payload['exp_yr_to'] = req.body.exp_yr_to,
      payload['exp_month_to'] =  req.body.exp_month_to,
      payload['sal_type'] =  req.body.sal_type,
      payload['sal_lakhs'] =  req.body.sal_lakhs,
      payload['sal_thousand'] =  req.body.sal_thousand,
      payload['emp_detail'] =  req.body.emp_detail,
      payload['wrk_status'] =  "Y",
      payload['wrk_date'] =  new Date(),
      payload['lastupdate'] =  new Date()
     
      let findwhere = {
         $and: [
            { wrk_status: { $ne: "D" } },
            {
               $and: [
                  { emp_design: req.body.emp_design },
                  { emp_org: req.body.emp_org },
                  { exp_yr: req.body.exp_yr }
               ]
            }
         ]
      };
      const founded = await empoffdetailsService.findOne(findwhere);
      if (!founded) {
         const created = await empoffdetailsService.create(payload);
         if (created && created.wrk_id) {
            logger.info(loggerMessage.createdSuccess.emp_Official_Details);
            return response.success(req, res, statusCodes.HTTP_CREATED, created, responseMessage.createdSuccess.emp_Official_Details);
         } else {
            logger.error(loggerMessage.createdFailure.emp_Official_Details);
            return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.createdFailure.emp_Official_Details);
         }
      } else {
         logger.warn(loggerMessage.alreadyExisting.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExisting.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInCreate.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInCreate.emp_Official_Details);
   }
};

EmpOffDetailsController.get = async (req, res) => {
   try {
      let where = {};
      let totalWhere = {};
      let _start = req.query && req.query._start ? Number(req.query._start) : 0;
      let _limit = req.query && req.query._limit ? Number(req.query._limit) : 10;
      if (req.query && req.query.status) {
         totalWhere['wrk_status'] = req.query.status;
      };
      if (req.query && req.query.search) {
         where = {
            $or: [
               { emp_desig: new RegExp(req.query.search, 'i') },
               { emp_org: new RegExp(req.query.search, 'i') }
            ]
         };
      };
      const { rows, count, totalCount } = await empoffdetailsService.findAllAndCount(where, _start, _limit, totalWhere);
      if (rows && count > 0) {
         logger.info(loggerMessage.getDataSuccess.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.getDataSuccess.emp_Official_Details);
      } else if (rows && count == 0) {
         logger.warn(loggerMessage.noDataFound.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, { rows, count, totalCount }, responseMessage.noDataFound.emp_Official_Details);
      } else {
         logger.error(loggerMessage.getDataFailure.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.getDataFailure.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInGetAllData.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInGetAllData.emp_Official_Details);
   }
};

EmpOffDetailsController.findByPk = async (req, res) => {
   try {
      let { wrk_id } = req.params;
      if (!wrk_id) throw createError.BadRequest();
      const founded = await empoffdetailsService.findByPk(wrk_id);
      if (founded && founded.wrk_id) {
         logger.info(loggerMessage.getIdSuccess.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, founded, responseMessage.getIdSuccess.emp_Official_Details);
      } else {
         logger.info(loggerMessage.notFound.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_OK, responseMessage.notFound.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInFindingId.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInFindingId.emp_Official_Details);
   }
};

EmpOffDetailsController.update = async (req, res) => {
   try {
      let { wrk_id } = req.params;
      if (!wrk_id) throw createError.BadRequest();
      const update = await empoffdetailsService.update(wrk_id, req.body);
      if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
         await empoffdetailsService.bulkUpdateLastupdate([Number(wrk_id)]);
         logger.info(loggerMessage.updateDataSuccess.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateDataSuccess.emp_Official_Details);
      } else if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 0) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_Official_Details);
      } else if (update && update.acknowledged == true && update.matchedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Official_Details);
      } else {
         logger.error(loggerMessage.updateDataFailure.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateDataFailure.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdating.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdating.emp_Official_Details);
   }
};

EmpOffDetailsController.updateStatus = async (req, res) => {
   try {
      let { wrk_ids, status } = req.body;
      const update = await empoffdetailsService.bulkUpdateStatus(wrk_ids, status);
      if (update && update.acknowledged == true && update.matchedCount == wrk_ids.length && update.modifiedCount == wrk_ids.length) {
         await empoffdetailsService.bulkUpdateLastupdate(wrk_ids);
         logger.info(loggerMessage.updateStatusSuccess.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.updateStatusSuccess.emp_Official_Details);
      } else if (update && update.acknowledged == true && update.matchedCount == wrk_ids.length && update.modifiedCount < wrk_ids.length) {
         logger.warn(loggerMessage.alreadyExistingValue.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_ALREADY_REPORTED, responseMessage.alreadyExistingValue.emp_Official_Details);
      } else if (update && update.acknowledged == true && update.matchedCount < wrk_ids.length) {
         logger.error(loggerMessage.notFound.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Official_Details);
      } else {
         logger.error(loggerMessage.updateStatusFailure.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.updateStatusFailure.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInUpdatingStatus.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInUpdatingStatus.emp_Official_Details);
   }
};

EmpOffDetailsController.delete = async (req, res) => {
   try {
      let { wrk_id } = req.query;
      if (!wrk_id) throw createError.BadRequest();
      const deleted = await empoffdetailsService.delete(wrk_id);
      if (deleted && deleted.acknowledged == true && deleted.deletedCount == 1) {
         logger.info(loggerMessage.deleteSuccess.emp_Official_Details);
         return response.success(req, res, statusCodes.HTTP_OK, null, responseMessage.deleteSuccess.emp_Official_Details);
      } else if (deleted && deleted.acknowledged == true && deleted.deletedCount == 0) {
         logger.error(loggerMessage.notFound.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_NOT_FOUND, responseMessage.notFound.emp_Official_Details);
      } else {
         logger.error(loggerMessage.deleteFailure.emp_Official_Details);
         return response.errors(req, res, statusCodes.HTTP_EXPECTATION_FAILED, responseMessage.deleteFailure.emp_Official_Details);
      }
   } catch (error) {
      logger.error(loggerMessage.errorInDeleting.emp_Official_Details);
      return response.errors(req, res, statusCodes.HTTP_INTERNAL_SERVER_ERROR, responseMessage.errorInDeleting.emp_Official_Details);
   }
};

module.exports = EmpOffDetailsController;
