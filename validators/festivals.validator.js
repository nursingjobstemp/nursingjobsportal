const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class FestivalsValidation {
   create(req, res, next) {
      const festivals_schema = Joi.object({
         fest_title: Joi.string().min(1).required(),
         fest_image: Joi.string().min(1).optional(),
         fest_pos: Joi.number().min(1).optional(),
         fest_date: Joi.date().raw().optional(),
         view_type: Joi.string().allow('').valid('U', 'A', 'M').optional(),
      });
      return BaseValidation.validateBody(req, res, next, festivals_schema);
   }
   get(req, res, next) {
      const festivals_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, festivals_schema);
   }
   update(req, res, next) {
      const festivals_schema = Joi.object({
         fest_title: Joi.string().min(1).optional(),
         fest_image: Joi.string().min(1).optional(),
         fest_pos: Joi.number().min(1).optional(),
         fest_date: Joi.date().raw().required(),
         live_status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         view_type: Joi.string().allow('').valid('U', 'A', 'M').optional(),
      });


      return BaseValidation.validateBody(req, res, next, festivals_schema);
   }
   updateStatus(req, res, next) {
      const festivals_schema = Joi.object({
         fest_ids: Joi.array().raw().required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });

      return BaseValidation.validateBody(req, res, next, festivals_schema);
   }
   updatePosition(req, res, next) {
      const sallary_schema = Joi.object({
         positionArray: Joi.array().items({
            fest_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, sallary_schema);
   }
   delete(req, res, next) {
      const festivals_schema = Joi.object({
         fest_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, festivals_schema);
   }
}
module.exports = new FestivalsValidation();
