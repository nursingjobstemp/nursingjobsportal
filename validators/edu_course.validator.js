const Joi = require('joi')
const BaseValidation = require("../middleware/baseValidation")

class Edu_CourseValidation {
   create(req, res, next) {
      const edu_course_schema = Joi.object({
         pid: Joi.number().required(),
         ecatid_sub: Joi.number().required(),
         ecat_type: Joi.string().valid('M', 'C', 'S').max(1).required(),
         ecat_name: Joi.string().min(1).required(),
         ecat_slug: Joi.string().min(1).required(),
         ecat_pos: Joi.number().min(1).required()
      })
      return BaseValidation.validateBody(req, res, next, edu_course_schema)
   }
   get(req, res, next) {
      const edu_course_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         mastCatId: Joi.number().allow('').optional(),
         mainCatId: Joi.number().allow('').optional(),
         subCatId: Joi.number().allow('').optional(),
      })
      return BaseValidation.validateQuery(req, res, next, edu_course_schema)
   }
   getMastcat(req, res, next) {
      const edu_course_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         mastCatId: Joi.string().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, edu_course_schema)
   }
   getMaincat(req, res, next) {
      const edu_course_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         mainCatId: Joi.string().allow('').optional(),
      })
      return BaseValidation.validateQuery(req, res, next, edu_course_schema)
   }
   getSubcat(req, res, next) {
      const edu_course_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         subCatId: Joi.number().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, edu_course_schema)
   }
   update(req, res, next) {
      const edu_course_schema = Joi.object({
         pid: Joi.number().optional(),
         ecatid_sub: Joi.number().optional(),
         ecat_type: Joi.string().valid('M', 'C', 'S').max(1).optional(),
         ecat_name: Joi.string().min(1).optional(),
         ecat_slug: Joi.string().min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, edu_course_schema)
   }
   updateStatus(req, res, next) {
      const edu_course_schema = Joi.object({
         ecat_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      })
      return BaseValidation.validateBody(req, res, next, edu_course_schema)
   }
   updatePosition(req, res, next) {
      const edu_course_schema = Joi.object({
         positionArray: Joi.array().items({
            ecat_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      })
      return BaseValidation.validateBody(req, res, next, edu_course_schema)
   }
   delete(req, res, next) {
      const edu_course_schema = Joi.object({
         ecat_id: Joi.number().min(1).required()
      })
      return BaseValidation.validateQuery(req, res, next, edu_course_schema)
   }
}
module.exports = new Edu_CourseValidation()
