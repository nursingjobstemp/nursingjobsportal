const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation")

class ChatValidation {
   create(req, res, next) {
      const chat_schema = Joi.object({
         job_id: Joi.number().min(1).required(),
         applyid: Joi.number().min(1).required(),
         chat_from: Joi.number().min(1).required(),
         chat_fname: Joi.string().min(1).required(),
         chat_ftype: Joi.string().min(1).required(),
         chat_to: Joi.number().min(1).required(),
         chat_tname: Joi.string().min(1).required(),
         chat_ttype: Joi.string().min(1).required(),
         chat_msg: Joi.string().min(1).required(),
         chat_status: Joi.string().valid('Y', 'N', 'D').max(1).required(),
         chat_date: Joi.date().raw().required(),
         read_status: Joi.string().valid('W', 'Y').max(1).required(),
         read_date: Joi.date().raw().required(),
         ipaddr: Joi.string().min(1).required()
      })
      return BaseValidation.validateBody(req, res, next, chat_schema)
   }
   get(req, res, next) {
      const chat_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, chat_schema);
   }
   update(req, res, next) {
      const chat_schema = Joi.object({
         job_id: Joi.number().min(1).optional(),
         applyid: Joi.number().min(1).optional(),
         chat_from: Joi.number().min(1).optional(),
         chat_fname: Joi.string().min(1).optional(),
         chat_ftype: Joi.string().min(1).optional(),
         chat_to: Joi.number().min(1).optional(),
         chat_tname: Joi.string().min(1).optional(),
         chat_ttype: Joi.string().min(1).optional(),
         chat_msg: Joi.string().min(1).optional(),
         chat_status: Joi.string().valid('Y', 'N', 'D').max(1).optional(),
         chat_date: Joi.date().raw().optional(),
         read_status: Joi.string().valid('W', 'Y').max(1).optional(),
         read_date: Joi.date().raw().optional(),
         ipaddr: Joi.string().min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, chat_schema)
   }
   updateStatus(req, res, next) {
      const chat_schema = Joi.object({
         chat_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, chat_schema);
   }
   delete(req, res, next) {
      const chat_schema = Joi.object({
         chat_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, chat_schema);
   }
}
module.exports = new ChatValidation();
