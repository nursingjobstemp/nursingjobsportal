const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmpkSkillsValidation {
   create(req, res, next) {
      const empkskills_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         keysk_id: Joi.number().min(1).required(),
         keysk_name: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, empkskills_schema);
   }
   get(req, res, next) {
      const empkskills_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, empkskills_schema);
   }
   update(req, res, next) {
      const empkskills_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         keysk_id: Joi.number().min(1).optional(),
         keysk_name: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, empkskills_schema);
   }
   updateStatus(req, res, next) {
      const empkskills_schema = Joi.object({
         empkskil_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required(),
      });
      return BaseValidation.validateBody(req, res, next, empkskills_schema);
   }
   delete(req, res, next) {
      const empkskills_schema = Joi.object({
         empkskil_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, empkskills_schema);
   }
}
module.exports = new EmpkSkillsValidation();
