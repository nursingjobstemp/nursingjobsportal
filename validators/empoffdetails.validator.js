const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmpofficialDetailsValidation {
   create(req, res, next) {
      const empoffdetails_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         emp_desig: Joi.string().min(1).required(),
         emp_org: Joi.string().min(1).required(),
         cur_comp: Joi.string().min(1).required(),
         exp_yr: Joi.string().min(1).required(),
         exp_month: Joi.string().min(1).required(),
         exp_yr_to: Joi.string().min(1).required(),
         exp_month_to: Joi.string().min(1).required(),
         sal_type: Joi.string().min(1).required(),
         sal_lakhs: Joi.string().min(1).required(),
         sal_thousand: Joi.string().min(1).required(),
         emp_detail: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, empoffdetails_schema);
   }
   get(req, res, next) {
      const empoffdetails_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, empoffdetails_schema);
   }
   update(req, res, next) {
      const empoffdetails_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         emp_desig: Joi.string().min(1).optional(),
         emp_org: Joi.string().min(1).optional(),
         cur_comp: Joi.string().min(1).optional(),
         exp_yr: Joi.string().min(1).optional(),
         exp_month: Joi.string().min(1).optional(),
         exp_yr_to: Joi.string().min(1).optional(),
         exp_month_to: Joi.string().min(1).optional(),
         sal_type: Joi.string().min(1).optional(),
         sal_lakhs: Joi.string().min(1).optional(),
         sal_thousand: Joi.string().min(1).optional(),
         emp_detail: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, empoffdetails_schema);
   }
   updateStatus(req, res, next) {
      const empoffdetails_schema = Joi.object({
         wrk_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, empoffdetails_schema);
   }
   delete(req, res, next) {
      const empoffdetails_schema = Joi.object({
         wrk_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, empoffdetails_schema);
   }
}
module.exports = new EmpofficialDetailsValidation();
