const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class SeekerValidation {
	create(req, res, next) {
		const seeker_schema = Joi.object({
			emp_name: Joi.string().min(1).required(),
			emp_email: Joi.string().allow(null).min(1).email().optional(),
			emp_mobile: Joi.string().allow(null).min(1).optional(),
			emp_pass: Joi.string().allow(null).min(1).optional(),
			email_otp: Joi.number().allow(null).min(1).optional(),
			email_verify: Joi.string().allow(null).min(1).optional(),
			mobile_otp: Joi.number().allow(null).min(1).optional(),
			mobile_verify: Joi.string().allow(null).min(1).optional(),
			emp_dob: Joi.date().raw().allow(null).optional(),
			emp_gender: Joi.string().allow(null).min(1).optional(),
			emp_cat: Joi.number().allow(null).min(1).optional(),
			emp_subcat: Joi.number().allow(null).min(1).optional(),
			cat_name: Joi.string().allow(null).min(1).optional(),
			subcat_name: Joi.string().allow(null).min(1).optional(),
			emp_desig: Joi.string().allow(null).min(1).optional(),
			emp_typeval: Joi.string().allow(null).min(1).optional(),
			emp_comp: Joi.string().allow(null).min(1).optional(),
			min_sal: Joi.string().allow(null).min(1).optional(),
			sal_lakh: Joi.string().allow(null).min(1).optional(),
			sal_thousands: Joi.string().allow(null).min(1).optional(),
			exp_year: Joi.string().allow(null).min(1).optional(),
			exp_month: Joi.string().allow(null).min(1).optional(),
			emp_photo: Joi.string().allow(null).min(1).optional(),
			emp_resumeheadline: Joi.string().allow(null).min(1).optional(),
			emp_country: Joi.number().allow(null).min(1).optional(),
			emp_state: Joi.number().allow(null).min(1).optional(),
			emp_city: Joi.number().allow(null).min(1).optional(),
			city_name: Joi.string().allow(null).min(1).optional(),
			high_qualif: Joi.string().allow(null).min(1).optional(),
			high_course: Joi.number().allow(null).min(1).optional(),
			high_special: Joi.number().allow(null).min(1).optional(),
			high_college: Joi.number().allow(null).min(1).optional(),
			colg_name: Joi.string().allow(null).min(1).optional(),
			course_type: Joi.string().allow(null).min(1).optional(),
			exp_type: Joi.string().allow(null).min(1).optional(),
			high_pass_yr: Joi.date().allow(null).min(1).optional(),
			emp_resume: Joi.string().allow(null).min(1).optional(),
			emp_pincode: Joi.number().allow(null).min(1).optional(),
			emp_marital: Joi.string().allow(null).min(1).optional(),
			emp_address: Joi.string().allow(null).min(1).optional(),
			user_type: Joi.string().allow(null).min(1).optional(),
			ipaddress: Joi.string().allow(null).min(1).optional()
		});
		return BaseValidation.validateBody(req, res, next, seeker_schema);
	}
	update(req, res, next) {
		const seeker_schema = Joi.object({
			emp_name: Joi.string().min(1).required(),
			emp_email: Joi.string().allow(null).min(1).email().optional(),
			emp_mobile: Joi.string().allow(null).min(1).optional(),
			emp_pass: Joi.string().allow(null).min(1).optional(),
			email_otp: Joi.number().allow(null).min(1).optional(),
			email_verify: Joi.string().allow(null).min(1).optional(),
			mobile_otp: Joi.number().allow(null).min(1).optional(),
			mobile_verify: Joi.string().allow(null).min(1).optional(),
			emp_dob: Joi.date().raw().allow(null).optional(),
			emp_gender: Joi.string().allow(null).min(1).optional(),
			emp_cat: Joi.number().allow(null).min(1).optional(),
			emp_subcat: Joi.number().allow(null).min(1).optional(),
			cat_name: Joi.string().allow(null).min(1).optional(),
			subcat_name: Joi.string().allow(null).min(1).optional(),
			emp_desig: Joi.string().allow(null).min(1).optional(),
			emp_typeval: Joi.string().allow(null).min(1).optional(),
			emp_comp: Joi.string().allow(null).min(1).optional(),
			min_sal: Joi.string().allow(null).min(1).optional(),
			sal_lakh: Joi.string().allow(null).min(1).optional(),
			sal_thousands: Joi.string().allow(null).min(1).optional(),
			exp_year: Joi.string().allow(null).min(1).optional(),
			exp_month: Joi.string().allow(null).min(1).optional(),
			emp_photo: Joi.string().allow(null).min(1).optional(),
			emp_resumeheadline: Joi.string().allow(null).min(1).optional(),
			emp_country: Joi.number().allow(null).min(1).optional(),
			emp_state: Joi.number().allow(null).min(1).optional(),
			emp_city: Joi.number().allow(null).min(1).optional(),
			city_name: Joi.string().allow(null).min(1).optional(),
			high_qualif: Joi.string().allow(null).min(1).optional(),
			high_course: Joi.number().allow(null).min(1).optional(),
			high_special: Joi.number().allow(null).min(1).optional(),
			high_college: Joi.number().allow(null).min(1).optional(),
			colg_name: Joi.string().allow(null).min(1).optional(),
			course_type: Joi.string().allow(null).min(1).optional(),
			exp_type: Joi.string().allow(null).min(1).optional(),
			high_pass_yr: Joi.date().allow(null).min(1).optional(),
			emp_resume: Joi.string().allow(null).min(1).optional(),
			emp_pincode: Joi.number().allow(null).min(1).optional(),
			emp_marital: Joi.string().allow(null).min(1).optional(),
			emp_address: Joi.string().allow(null).min(1).optional(),
			user_type: Joi.string().allow(null).min(1).optional(),
			ipaddress: Joi.string().allow(null).min(1).optional(),
			skills: Joi.array().allow(null).min(1).optional(),
			offdetails: Joi.array().allow(null).min(1).optional(),
			location: Joi.array().allow(null).min(1).optional(),
			education: Joi.array().allow(null).min(1).optional(),
		});
		return BaseValidation.validateBody(req, res, next, seeker_schema);
	}
	get(req, res, next) {
		const salary_schema = Joi.object({
			_start: Joi.number().allow('').optional(),
			_limit: Joi.number().allow('').optional(),
			status: Joi.string().allow('').valid('Y', 'N','W', 'D').optional(),
			search: Joi.string().allow('').optional(),
			dateRange: Joi.string().allow('').optional(),
			cityId: Joi.number().allow('').optional(),
			stateId: Joi.number().allow('').optional(),
			countryId: Joi.number().allow('').optional(),
			mainCatId: Joi.number().allow('').optional(),
			subCatId: Joi.number().allow('').optional()
		});
		return BaseValidation.validateQuery(req, res, next, salary_schema);
	}
	updateStatus(req, res, next) {
		const seeker_schema = Joi.object({
			emp_ids: Joi.array().items(Joi.number()).min(1).required(),
			status: Joi.string().valid('Y', 'N', 'D','W').max(1).required()
		});
		return BaseValidation.validateBody(req, res, next, seeker_schema);
	}
	delete(req, res, next) {
		const salary_schema = Joi.object({
			emp_id: Joi.number().min(1).required()
		});
		return BaseValidation.validateQuery(req, res, next, salary_schema);
	}
}
module.exports = new SeekerValidation();