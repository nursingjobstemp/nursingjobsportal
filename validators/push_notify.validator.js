const BaseValidation = require('../middleware/baseValidation')
const Joi = require('joi')

class push_notifyValidator {
    create(req, res, next) {
        const push_notify_Schema = Joi.object({
            notify_title: Joi.string().min(1).required(),
            notify_msg: Joi.string().min(1).required(),
            seeker_id: Joi.string().allow(null).min(1).optional(),
            employer_id: Joi.string().allow(null).min(1).optional(),
            user_status: Joi.string().valid('A', 'S', 'E').min(1).optional(),
            notify_time: Joi.date().raw().required(),
        })
        return BaseValidation.validateBody(req, res, next, push_notify_Schema)
    }
    get(req, res, next) {
        const push_notify_Schema = Joi.object({
            _start: Joi.number().allow('').optional(),
            _limit: Joi.number().allow('').optional(),
            status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
            search: Joi.string().allow('').optional(),
            notifyId:Joi.number().allow('').optional()
        })
        return BaseValidation.validateQuery(req, res, next, push_notify_Schema)
    }
    sendNotify(req, res, next) {
        const push_notify_Schema = Joi.object({
            user_id: Joi.array().items(Joi.number()).min(1).optional(),
            title: Joi.string().min(1).required(),
            body: Joi.string().min(1).required(),
            image_url: Joi.string().min(1).optional(),
            tokens: Joi.array().min(1).optional(),
        })
        return BaseValidation.validateBody(req, res, next, push_notify_Schema)
    }
    update(req, res, next) {
        const push_notify_Schema = Joi.object({
            notify_title: Joi.string().min(1).optional(),
            notify_msg: Joi.string().min(1).optional(),
            seeker_id: Joi.string().min(1).allow(null).optional(),
            employer_id: Joi.string().min(1).allow(null).optional(),
            user_status: Joi.string().valid('A', 'S', 'E').min(1).optional(),
            notify_time: Joi.date().raw().required(),
        })
        return BaseValidation.validateBody(req, res, next, push_notify_Schema)
    }
    updateStatus(req, res, next) {
        const push_notify_Schema = Joi.object({
            notify_ids: Joi.array().min(1).required(),
            status: Joi.string().valid('Y', 'N', 'D').required()
        })
        return BaseValidation.validateBody(req, res, next, push_notify_Schema)
    }
    delete(req, res, next) {
        const push_notify_Schema = Joi.object({
            notify_id: Joi.string().min(1).required(),
        })
        return BaseValidation.validateQuery(req, res, next, push_notify_Schema)
    }
}
module.exports = new push_notifyValidator()