const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class CollegeValidation {
   create(req, res, next) {
      const college_schema = Joi.object({
         colg_name: Joi.string().min(1).required(),
         colg_slug: Joi.string().min(1).required(),
         colg_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, college_schema);
   }
   get(req, res, next) {
      const college_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, college_schema);
   }
   update(req, res, next) {
      const college_schema = Joi.object({
         colg_name: Joi.string().min(1),
         colg_slug: Joi.string().min(1),
         colg_pos: Joi.number().min(1)
      });
      return BaseValidation.validateBody(req, res, next, college_schema);
   }
   updateStatus(req, res, next) {
      const college_schema = Joi.object({
         colg_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required(),
      });
      return BaseValidation.validateBody(req, res, next, college_schema);
   }
   updatePosition(req, res, next) {
      const college_schema = Joi.object({
         positionArray: Joi.array().items({
            colg_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, college_schema);
   }
   delete(req, res, next) {
      const college_schema = Joi.object({
         colg_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, college_schema);
   }

}
module.exports = new CollegeValidation();
