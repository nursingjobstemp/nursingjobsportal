const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation")

class JobAppliedValidation {
   create(req, res, next) {
      const jobapplied_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         job_id: Joi.number().min(1).required(),
         company_id: Joi.number().min(1).required(),
         job_type: Joi.string().min(1).required(),
         ipaddress: Joi.string().min(1).required()
      })
      return BaseValidation.validateBody(req, res, next, jobapplied_schema)
   }
   get(req, res, next) {
      const jobapplied_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('W', 'A', 'C', 'H', 'R').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, jobapplied_schema);
   }
   update(req, res, next) {
      const jobapplied_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         job_id: Joi.number().min(1).optional(),
         company_id: Joi.number().min(1).optional(),
         job_type: Joi.string().min(1).optional(),
         ipaddress: Joi.string().min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, jobapplied_schema)
   }
   updateStatus(req, res, next) {
      const jobapplied_schema = Joi.object({
         applied_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('W', 'A', 'C', 'H', 'R').required()
      });
      return BaseValidation.validateBody(req, res, next, jobapplied_schema);
   }
   delete(req, res, next) {
      const jobapplied_schema = Joi.object({
         applied_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, jobapplied_schema);
   }
}
module.exports = new JobAppliedValidation();
