const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class IndustryTypeValidation {
   create(req, res, next) {
      const industryType_schema = Joi.object({
         indust_name: Joi.string().min(1).required(),
         indust_slug: Joi.string().min(1).required(),
         indust_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, industryType_schema);
   }
   get(req, res, next) {
      const industryType_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, industryType_schema);
   }
   update(req, res, next) {
      const industryType_schema = Joi.object({
         indust_name: Joi.string().min(1).optional(),
         indust_slug: Joi.string().min(1).optional(),
         indust_pos: Joi.number().min(1).optional(),
         indust_date: Joi.date().raw().optional()
      });
      return BaseValidation.validateBody(req, res, next, industryType_schema);
   }
   updateStatus(req, res, next) {
      const industryType_schema = Joi.object({
         indust_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, industryType_schema);
   }
   updatePosition(req, res, next) {
      const industryType_schema = Joi.object({
         positionArray: Joi.array().items({
            indust_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, industryType_schema);
   }
   delete(req, res, next) {
      const industryType_schema = Joi.object({
         indust_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, industryType_schema);
   }
}
module.exports = new IndustryTypeValidation();
