const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmployeeValidation {
   create(req, res, next) {
      const employer_schema = Joi.object({
         comp_name: Joi.string().min(1).required(),
         comp_slug: Joi.string().min(1).required(),
         mail_id: Joi.string().email().min(1).required(),
         mobile_no: Joi.number().min(1).required(),
         email_otp: Joi.number().min(1).required(),
         email_verify: Joi.string().min(1).required(),
         mobile_otp: Joi.number().min(1).required(),
         mobile_verify: Joi.string().min(1).required(),
         comp_logo: Joi.string().min(1).required(),
         comp_pass: Joi.string().min(1).required(),
         cont_person: Joi.string().min(1).required(),
         indust_id: Joi.number().min(1).required(),
         recut_type: Joi.string().min(1).required(),
         country_id: Joi.number().min(1).required(),
         state_id: Joi.number().min(1).required(),
         city_id: Joi.number().min(1).required(),
         pincode: Joi.number().min(1).required(),
         emp_strength: Joi.string().min(1).required(),
         recut_desc: Joi.string().min(1).required(),
         facebook: Joi.string().min(1).required(),
         twitter: Joi.string().min(1).required(),
         linkedin: Joi.string().min(1).required(),
         google: Joi.string().min(1).required(),
         recut_address: Joi.string().min(1).required(),
         ipaddress: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, employer_schema);
   }
   get(req, res, next) {
      const employer_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         dateRange: Joi.string().allow('').optional(),
         cityId: Joi.number().allow('').optional(),
         stateId: Joi.number().allow('').optional(),
         countryId: Joi.number().allow('').optional(),
         mainCatId: Joi.number().allow('').optional(),
         subCatId: Joi.number().allow('').optional(),
         countryFilter: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, employer_schema);
   }
   update(req, res, next) {
      const employer_schema = Joi.object({
         comp_name: Joi.string().min(1).optional(),
         comp_slug: Joi.string().min(1).optional(),
         mail_id: Joi.string().email().min(1).optional(),
         mobile_no: Joi.number().min(1).optional(),
         email_otp: Joi.number().min(1).optional(),
         email_verify: Joi.string().min(1).optional(),
         mobile_otp: Joi.number().min(1).optional(),
         mobile_verify: Joi.string().min(1).optional(),
         comp_logo: Joi.string().min(1).optional(),
         comp_pass: Joi.string().min(1).optional(),
         cont_person: Joi.string().min(1).optional(),
         indust_id: Joi.number().min(1).optional(),
         recut_type: Joi.string().min(1).optional(),
         country_id: Joi.number().min(1).optional(),
         state_id: Joi.number().min(1).optional(),
         city_id: Joi.number().min(1).optional(),
         pincode: Joi.number().min(1).optional(),
         emp_strength: Joi.string().min(1).optional(),
         recut_desc: Joi.string().min(1).optional(),
         facebook: Joi.string().min(1).optional(),
         twitter: Joi.string().min(1).optional(),
         linkedin: Joi.string().min(1).optional(),
         google: Joi.string().min(1).optional(),
         recut_address: Joi.string().min(1).optional(),
         ipaddress: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, employer_schema);
   }
   updateStatus(req, res, next) {
      const employer_schema = Joi.object({
         recut_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, employer_schema);
   }
   delete(req, res, next) {
      const employer_schema = Joi.object({
         recut_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, employer_schema);
   }
   empPostJob(req, res, next) {
      const employer_schema = Joi.object({
         apply: Joi.string().min(1).max(1).optional(),
         unrest_jcat: Joi.number().min(1).required(),
         unrest_jsubcat: Joi.number().min(1).required(),
         unrest_jquali: Joi.string().min(1).optional(),
         unrest_jallow: Joi.string().min(1).optional(),
         unrest_jdesc: Joi.string().min(1).optional(),
         country_id: Joi.number().min(1).required(),
         state: Joi.number().min(1).required(),
         unrest_jloct: Joi.number().min(1).required(),
         unrest_jcompany: Joi.string().min(4).optional(),
         comp_detail: Joi.string().min(5).optional(),
         comp_address: Joi.string().min(5).optional(),
         unrest_jemail: Joi.string().email().optional(),
         unrest_jphone: Joi.string().min(10).optional(),
         jtype_id: Joi.number().min(1).required(),
         sal_id: Joi.number().min(1).required(),
         job_exp: Joi.number().min(1).optional(),
         exp_date: Joi.date().required(),
         no_of_openings: Joi.string().min(1).optional(),
         unrest_landline: Joi.string().min(1).optional(),
         key_skills: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, employer_schema);
   }
}
module.exports = new EmployeeValidation();


