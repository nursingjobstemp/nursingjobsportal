const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class JobsCategoryValidation {
   create(req, res, next) {
      const jobscategory_schema = Joi.object({
         jcat_name: Joi.string().min(1).required(),
         jcat_slug: Joi.string().min(1).required(),
         jcat_code: Joi.string().min(1).required(),
         jcat_icon: Joi.string().allow('').allow(null).min(1).optional(),
         jcat_desc: Joi.string().allow('').allow(null).min(1).optional(),
         jcat_image: Joi.string().allow('').allow(null).min(1).optional(),
         jcat_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, jobscategory_schema);
   }
   get(req, res, next) {
      const jobscategory_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         mainCatId: Joi.string().allow('').optional(),
         subCatId: Joi.number().allow('').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, jobscategory_schema);
   }
   update(req, res, next) {
      const jobscategory_schema = Joi.object({
         jcat_name: Joi.string().min(1).optional(),
         jcat_slug: Joi.string().min(1).optional(),
         jcat_code: Joi.string().min(1).optional(),
         jcat_icon: Joi.string().allow(null).min(1).optional(),
         jcat_desc: Joi.string().allow(null).min(1).optional(),
         jcat_image: Joi.string().allow(null).min(1).optional(),
         jcat_pos: Joi.number().min(1).optional(),
         foot_status: Joi.string().valid('Y', 'N').min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, jobscategory_schema);
   }
   updateStatus(req, res, next) {
      const jobscategory_schema = Joi.object({
         jcat_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, jobscategory_schema);
   }
   updatePosition(req, res, next) {
      const jobscategory_schema = Joi.object({
         positionArray: Joi.array().items({
            jcat_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, jobscategory_schema);
   }
   delete(req, res, next) {
      const jobscategory_schema = Joi.object({
         jcat_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, jobscategory_schema);
   }
}
module.exports = new JobsCategoryValidation();
