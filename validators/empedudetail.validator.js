const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmpedudetailValidation {
   create(req, res, next) {
      const empedudetail_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         high_qualif: Joi.number().min(1).required(),
         high_course: Joi.number().min(1).required(),
         high_special: Joi.number().min(1).required(),
         high_college: Joi.number().min(1).required(),
         colg_name: Joi.string().min(1).required(),
         high_pass_yr: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, empedudetail_schema);
   }
   get(req, res, next) {
      const empjobcat_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, empjobcat_schema);
   }
   update(req, res, next) {
      const empedudetail_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         high_qualif: Joi.number().min(1).optional(),
         high_course: Joi.number().min(1).optional(),
         high_special: Joi.number().min(1).optional(),
         high_college: Joi.number().min(1).optional(),
         colg_name: Joi.string().min(1).optional(),
         high_pass_yr: Joi.number().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, empedudetail_schema);
   }
   delete(req, res, next) {
      const empjobcat_schema = Joi.object({
         edu_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, empjobcat_schema);
   }
}
module.exports = new EmpedudetailValidation();
