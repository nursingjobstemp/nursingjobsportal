const Joi = require('joi')
const BaseValidation = require("../middleware/baseValidation")

class OperatorValidation {
   create(req, res, next) {
      const operator_schema = Joi.object({
         op_type: Joi.string().valid('O', 'A').max(1).required(),
         op_name: Joi.string().min(1).required(),
         op_uname: Joi.string().min(1).required(),
         op_password: Joi.string().min(1).required(),
         feat_id: Joi.string().allow('').optional()
      })
      return BaseValidation.validateBody(req, res, next, operator_schema)
   }
   get(req, res, next) {
      const operator_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         type: Joi.string().allow('').valid('SA', 'O', 'A').optional(),
         search: Joi.string().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, operator_schema)
   }
   data(req, res, next) {
      const operator_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         type: Joi.string().allow('').valid('SA', 'O', 'A').optional(),
         search: Joi.string().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, operator_schema)
   }
   update(req, res, next) {
      const operator_schema = Joi.object({
         op_type: Joi.string().valid('O', 'A').max(1).optional(),
         op_name: Joi.string().min(1).optional(),
         op_uname: Joi.string().min(1).optional(),
         op_password: Joi.string().min(1).optional(),
         feat_id: Joi.string().min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, operator_schema)
   }
   updateStatus(req, res, next) {
      const operator_schema = Joi.object({
         op_ids: Joi.array().raw().required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      })
      return BaseValidation.validateBody(req, res, next, operator_schema)
   }
   changePassword(req, res, next) {
      const operator_schema = Joi.object({
         user_name: Joi.string().min(1).required(),
         old_password: Joi.string().min(1).required(),
         new_password: Joi.string().min(1).required(),
         op_type: Joi.string().max(1).valid('O', 'A').optional()
      })
      return BaseValidation.validateBody(req, res, next, operator_schema)
   }
   delete(req, res, next) {
      const operator_schema = Joi.object({
         op_id: Joi.number().min(1).required()
      })
      return BaseValidation.validateQuery(req, res, next, operator_schema)
   }
}
module.exports = new OperatorValidation()
