const Joi = require('joi')
const BaseValidation = require("../middleware/baseValidation")

class UnrestJobPostValidation {
   create(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         jcat_type: Joi.string().valid('M', 'S').max(1).required(),
         unrest_jcat: Joi.number().min(1).required(),
         unrest_jsubcat: Joi.number().min(1).required(),
         unrest_jcode: Joi.string().min(1).required(),
         verify: Joi.string().valid('Y', 'N').max(1).required(),
         unrest_jdesc: Joi.string().min(1).required(),
         unrest_jquali: Joi.string().allow(null).min(1).required(),
         unrest_jrequ: Joi.string().allow(null).min(1).optional(),
         unrest_jname: Joi.string().min(1).required(),
         high_qualif: Joi.array().optional(),
         high_course: Joi.array().optional(),
         high_special: Joi.array().optional(),
         unrest_jallow: Joi.string().allow(null).min(1).optional(),
         sal_id: Joi.string().allow(null).min(1).optional(),
         jtype_id: Joi.string().allow(null).min(1).optional(),
         jtype_id_new: Joi.string().allow(null).min(1).optional(),
         job_type: Joi.string().min(1).required(),
         key_skills: Joi.string().allow(null).min(1).optional(),
         job_exp: Joi.string().allow(null).min(1).optional(),
         country_id: Joi.number().min(1).required(),
         state: Joi.number().allow(null).min(1).optional(),
         unrest_jloct: Joi.number().allow(null).min(1).optional(),
         unrest_jcompany: Joi.string().allow(null).min(1).optional(),
         comp_detail: Joi.string().allow(null).min(1).optional(),
         unrest_jemail: Joi.string().allow(null).min(1).optional(),
         unrest_jphoneold: Joi.number().allow(null).min(1).optional(),
         unrest_jphone: Joi.string().allow(null).min(1).optional(),
         unrest_landline: Joi.string().allow(null).min(1).optional(),
         unrest_sal: Joi.string().min(1).required(),
         comp_address: Joi.string().allow(null).min(1).optional(),
         apply: Joi.string().min(1).required(),
         ip_address: Joi.string().min(1).required(),
         posted_id: Joi.number().min(1).required(),
         posted_by: Joi.string().min(1).required(),
         posted_name: Joi.string().min(1).required(),
         posted_pos: Joi.number().min(1).required(),
         posted_status: Joi.string().min(1).valid('Y', 'N', 'W', 'C', 'D').required(),
         exp_date: Joi.date().raw().required(),
         comp_website: Joi.string().allow(null).min(1).optional(),
         field_exp: Joi.string().allow(null).min(1).required(),
         nationality: Joi.string().allow(null).min(1).optional(),
         no_of_openings: Joi.number().allow(null).min(1).optional(),
         gender: Joi.string().allow(null).min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, unrestjobpost_schema)
   }
   createDuplicate(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         duplicate_from: Joi.number().min(1).required(),
         jcat_type: Joi.string().valid('M', 'S').max(1).required(),
         unrest_jcat: Joi.number().min(1).required(),
         unrest_jsubcat: Joi.number().min(1).required(),
         unrest_jcode: Joi.string().min(1).required(),
         verify: Joi.string().valid('Y', 'N').max(1).required(),
         unrest_jdesc: Joi.string().min(1).required(),
         unrest_jquali: Joi.string().allow(null).min(1).required(),
         unrest_jrequ: Joi.string().min(1).allow(null).required(),
         unrest_jname: Joi.string().min(1).required(),
         high_qualif: Joi.array().optional(),
         high_course: Joi.array().optional(),
         high_special: Joi.array().optional(),
         unrest_jallow: Joi.string().allow(null).min(1).optional(),
         sal_id: Joi.string().allow(null).min(1).optional(),
         jtype_id: Joi.string().min(1).required(),
         jtype_id_new: Joi.string().allow(null).min(1).optional(),
         job_type: Joi.string().min(1).required(),
         key_skills: Joi.string().allow(null).min(1).optional(),
         job_exp: Joi.string().allow(null).min(1).required(),
         country_id: Joi.number().min(1).required(),
         state: Joi.number().min(1).required(),
         unrest_jloct: Joi.number().min(1).required(),
         unrest_jcompany: Joi.string().allow(null).min(1).optional(),
         comp_detail: Joi.string().allow(null).min(1).optional(),
         unrest_jemail: Joi.string().allow(null).min(1).optional(),
         unrest_jphoneold: Joi.number().allow(null).min(1).optional(),
         unrest_jphone: Joi.string().allow(null).min(1).optional(),
         unrest_landline: Joi.string().allow(null).min(1).optional(),
         unrest_sal: Joi.string().min(1).required(),
         comp_address: Joi.string().allow(null).min(1).optional(),
         apply: Joi.string().min(1).required(),
         ip_address: Joi.string().min(1).required(),
         posted_id: Joi.number().min(1).required(),
         posted_by: Joi.string().min(1).required(),
         posted_name: Joi.string().min(1).required(),
         posted_pos: Joi.number().min(1).required(),
         posted_status: Joi.string().min(1).valid('Y', 'N', 'W', 'C', 'D').required(),
         exp_date: Joi.date().raw().required(),
         comp_website: Joi.string().allow(null).min(1).optional(),
         field_exp: Joi.string().allow(null).min(1).required(),
         nationality: Joi.string().allow(null).min(1).optional(),
         no_of_openings: Joi.number().allow(null).min(1).optional(),
         gender: Joi.string().allow(null).min(1).optional()
      })
      return BaseValidation.validateBody(req, res, next, unrestjobpost_schema)
   }
   get(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D', 'W', 'C').optional(),
         search: Joi.string().allow('').optional(),
         posted_id: Joi.number().allow('').optional(),
         dateRange: Joi.string().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, unrestjobpost_schema)
   }
   getCompanyJobs(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         expired: Joi.boolean().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D', 'W', 'C').optional(),
         search: Joi.string().allow('').optional(),
         posted_id: Joi.number().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, unrestjobpost_schema)
   }
   getExpJob(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D', 'W', 'C').optional(),
         search: Joi.string().allow('').optional(),
         posted_id: Joi.number().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, unrestjobpost_schema)
   }
   getDuplicateJob(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D', 'W', 'C').optional(),
         search: Joi.string().allow('').optional(),
         posted_id: Joi.number().allow('').optional()
      })
      return BaseValidation.validateQuery(req, res, next, unrestjobpost_schema)
   }
   update(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         duplicate_from: Joi.number().allow(null).min(1).optional(),
         jcat_type: Joi.string().valid('M', 'S').max(1).optional(),
         unrest_jcat: Joi.number().allow(null).min(1).optional(),
         unrest_jsubcat: Joi.number().allow(null).min(1).optional(),
         unrest_jname: Joi.string().min(1).required(),
         unrest_jcode: Joi.string().allow(null).min(1).optional(),
         verify: Joi.string().valid('Y', 'N').max(1).optional(),
         unrest_jdesc: Joi.string().allow(null).min(1).optional(),
         unrest_jquali: Joi.string().allow(null).min(1).optional(),
         unrest_jrequ: Joi.string().allow(null).min(1).optional(),
         high_qualif: Joi.array().optional(),
         high_course: Joi.array().optional(),
         high_special: Joi.array().optional(),
         unrest_jallow: Joi.string().allow(null).min(1).optional(),
         sal_id: Joi.string().allow(null).min(1).optional(),
         jtype_id: Joi.string().allow(null).min(1).optional(),
         jtype_id_new: Joi.string().allow(null).min(1).optional(),
         job_type: Joi.string().allow(null).min(1).optional(),
         key_skills: Joi.string().allow(null).min(1).optional(),
         job_exp: Joi.string().allow(null).min(1).optional(),
         country_id: Joi.number().allow(null).min(1).optional(),
         state: Joi.number().allow(null).min(1).optional(),
         unrest_jloct: Joi.number().allow(null).min(1).optional(),
         unrest_jcompany: Joi.string().allow(null).min(1).optional(),
         comp_detail: Joi.string().allow(null).min(1).optional(),
         unrest_jemail: Joi.string().allow(null).min(1).optional(),
         unrest_jphoneold: Joi.number().allow(null).min(1).optional(),
         unrest_jphone: Joi.string().allow(null).min(1).optional(),
         unrest_landline: Joi.string().allow(null).min(1).optional(),
         unrest_sal: Joi.string().allow(null).min(1).optional(),
         comp_address: Joi.string().allow(null).min(1).optional(),
         apply: Joi.string().allow(null).min(1).optional(),
         ip_address: Joi.string().allow(null).min(1).optional(),
         posted_pos: Joi.number().allow(null).min(1).optional(),
         posted_status: Joi.string().allow(null).min(1).optional(),
         exp_date: Joi.date().raw().optional(),
         comp_website: Joi.string().allow(null).min(1).optional(),
         field_exp: Joi.string().allow(null).min(1).optional(),
         nationality: Joi.string().allow(null).min(1).optional(),
         no_of_openings: Joi.number().allow(null).min(1).optional(),
         gender: Joi.string().allow(null).min(1).optional()

      })
      return BaseValidation.validateBody(req, res, next, unrestjobpost_schema)
   }
   updateStatus(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         unrst_jids: Joi.array().min(1).raw().required(),
         status: Joi.string().valid('Y', 'N', 'D', 'W', 'C').max(1).required()
      })
      return BaseValidation.validateBody(req, res, next, unrestjobpost_schema)
   }
   delete(req, res, next) {
      const unrestjobpost_schema = Joi.object({
         unrst_jid: Joi.number().min(1).required()
      })
      return BaseValidation.validateQuery(req, res, next, unrestjobpost_schema)
   }
}

module.exports = new UnrestJobPostValidation()
