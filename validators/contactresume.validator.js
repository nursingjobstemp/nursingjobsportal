const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class ContactresumeValidation {
   create(req, res, next) {
      const contactresume_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         comp_id: Joi.number().min(1).required(),
         cont_type: Joi.string().valid('C', 'S').max(1).required(),
         ipaddress: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, contactresume_schema);
   }
   get(req, res, next) {
      const contactresume_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         cont_type: Joi.string().allow('').valid('C', 'S').optional()
      });
      return BaseValidation.validateQuery(req, res, next, contactresume_schema);
   }
   update(req, res, next) {
      const contactresume_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         comp_id: Joi.number().min(1).optional(),
         cont_type: Joi.string().valid('C', 'S').max(1).optional(),
         ipaddress: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, contactresume_schema);
   }
   updateStatus(req, res, next) {
      const contactresume_schema = Joi.object({
         cont_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, contactresume_schema);
   }
   delete(req, res, next) {
      const contactresume_schema = Joi.object({
         cont_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, contactresume_schema);
   }
}
module.exports = new ContactresumeValidation();
