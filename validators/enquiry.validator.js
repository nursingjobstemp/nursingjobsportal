const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EnquiryValidation {
   create(req, res, next) {
      const enquiry_schema = Joi.object({
         enq_name: Joi.string().min(1).required(),
         enq_email: Joi.string().min(1).required(),
         enq_mobile: Joi.number().min(1).required(),
         enq_msg: Joi.string().min(1).required(),
         enq_date: Joi.date().raw().required(),
         ipaddress: Joi.string().min(1).required(),
         enq_altmobile: Joi.string().min(1).required(),
         maincat: Joi.number().min(1).required(),
         type_home: Joi.string().min(1).required(),
         type_bhk: Joi.string().min(1).required(),
         enq_loc: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, enquiry_schema);
   }
   get(req, res, next) {
      const enquiry_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, enquiry_schema);
   }
   update(req, res, next) {
      const enquiry_schema = Joi.object({
         enq_name: Joi.string().min(1).optional(),
         enq_email: Joi.string().min(1).optional(),
         enq_mobile: Joi.number().min(1).optional(),
         enq_msg: Joi.string().min(1).optional(),
         enq_date: Joi.date().raw().optional(),
         ipaddress: Joi.string().min(1).optional(),
         enq_altmobile: Joi.string().min(1).optional(),
         maincat: Joi.number().min(1).optional(),
         type_home: Joi.string().min(1).optional(),
         type_bhk: Joi.string().min(1).optional(),
         enq_loc: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, enquiry_schema);
   }
   updateStatus(req, res, next) {
      const enquiry_schema = Joi.object({
         enq_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, enquiry_schema);
   }
   delete(req, res, next) {
      const enquiry_schema = Joi.object({
         enq_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, enquiry_schema);
   }
}
module.exports = new EnquiryValidation();
