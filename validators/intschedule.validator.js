const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class IntScheduleValidation {
   create(req, res, next) {
      const intschedule_schema = Joi.object({
         applied_id: Joi.number().min(1).required(),
         emp_id: Joi.number().min(1).required(),
         company_id: Joi.number().min(1).required(),
         mail_title: Joi.string().min(1).required(),
         mail_content: Joi.string().min(1).required(),
         ipaddress: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, intschedule_schema);
   }
   get(req, res, next) {
      const intschedule_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, intschedule_schema);
   }
   update(req, res, next) {
      const intschedule_schema = Joi.object({
         applied_id: Joi.number().min(1).optional(),
         emp_id: Joi.number().min(1).optional(),
         company_id: Joi.number().min(1).optional(),
         mail_title: Joi.string().min(1).optional(),
         mail_content: Joi.string().min(1).optional(),
         ipaddress: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, intschedule_schema);
   }
   updateStatus(req, res, next) {
      const intschedule_schema = Joi.object({
         intsch_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, intschedule_schema);
   }
   delete(req, res, next) {
      const intschedule_schema = Joi.object({
         intsch_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, intschedule_schema);
   }
}
module.exports = new IntScheduleValidation();
