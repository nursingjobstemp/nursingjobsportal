const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmplocationValidation {
   create(req, res, next) {
      const emplocat_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         emp_country: Joi.number().min(1).required(),
         emp_state: Joi.number().min(1).required(),
         emp_city: Joi.number().min(1).required(),
         ipaddr: Joi.string().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, emplocat_schema);
   }
   get(req, res, next) {
      const emplocat_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, emplocat_schema);
   }
   update(req, res, next) {
      const emplocat_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         emp_country: Joi.number().min(1).optional(),
         emp_state: Joi.number().min(1).optional(),
         emp_city: Joi.number().min(1).optional(),
         ipaddr: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, emplocat_schema);
   }
   updateStatus(req, res, next) {
      const emplocat_schema = Joi.object({
         emplocat_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, emplocat_schema);
   }
   delete(req, res, next) {
      const emplocat_schema = Joi.object({
         emplocat_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, emplocat_schema);
   }
}
module.exports = new EmplocationValidation();
