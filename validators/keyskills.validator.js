const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class KeySkillsValidation {

   create(req, res, next) {
      const keyskills_schema = Joi.object({
         keysk_name: Joi.string().min(1).required(),
         keysk_slug: Joi.string().min(1).required(),
         keysk_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, keyskills_schema);
   }
   get(req, res, next) {
      const keyskills_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional(),
         id: Joi.number().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, keyskills_schema);
   }
   update(req, res, next) {
      const keyskills_schema = Joi.object({
         keysk_name: Joi.string().min(1).optional(),
         keysk_slug: Joi.string().min(1).optional(),
         keysk_code: Joi.string().min(1).optional(),
         keysk_pos: Joi.number().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, keyskills_schema);
   }
   updateStatus(req, res, next) {
      const keyskills_schema = Joi.object({
         keysk_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, keyskills_schema);
   }
   updatePosition(req, res, next) {
      const keyskills_schema = Joi.object({
         positionArray: Joi.array().items({
            keysk_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, keyskills_schema);
   }
   delete(req, res, next) {
      const keyskills_schema = Joi.object({
         keysk_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, keyskills_schema);
   }
}
module.exports = new KeySkillsValidation();
