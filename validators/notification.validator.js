const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class NotificationValidation {
   create(req, res, next) {
      const notification_schema = Joi.object({
         noti_title: Joi.string().min(1).required(),
         noti_msg: Joi.string().min(1).required(),
         chat_id: Joi.number().min(1).required(),
         noti_type: Joi.string().valid('JOB', 'CHAT').max(1).required(),
         job_applyid: Joi.number().min(1).required(),
         noti_from: Joi.number().min(1).required(),
         noti_ftype: Joi.string().min(1).required(),
         noti_to: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, notification_schema);
   }
   get(req, res, next) {
      const notification_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, notification_schema);
   }
   update(req, res, next) {
      const notification_schema = Joi.object({
         noti_title: Joi.string().min(1).optional(),
         noti_msg: Joi.string().min(1).optional(),
         chat_id: Joi.number().min(1).optional(),
         noti_type: Joi.string().valid('JOB', 'CHAT').max(1).optional(),
         job_applyid: Joi.number().min(1).optional(),
         noti_from: Joi.number().min(1).optional(),
         noti_ftype: Joi.string().min(1).optional(),
         noti_to: Joi.number().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, notification_schema);
   }
   updateStatus(req, res, next) {
      const notification_schema = Joi.object({
         notify_ids: Joi.array().raw().required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, notification_schema);
   }
   delete(req, res, next) {
      const notification_schema = Joi.object({
         notify_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, notification_schema);
   }
}
module.exports = new NotificationValidation();
