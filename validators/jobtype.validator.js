const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class JobTypeValidation {
   create(req, res, next) {
      const JobType_schema = Joi.object({
         jtype_name: Joi.string().min(1).required(),
         jtype_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, JobType_schema);
   }
   get(req, res, next) {
      const JobType_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, JobType_schema);
   }
   update(req, res, next) {
      const JobType_schema = Joi.object({
         jtype_name: Joi.string().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, JobType_schema);
   }
   updateStatus(req, res, next) {
      const JobType_schema = Joi.object({
         jtype_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, JobType_schema);
   }
   updatePosition(req, res, next) {
      const JobType_schema = Joi.object({
         positionArray: Joi.array().items({
            jtype_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, JobType_schema);
   }
   delete(req, res, next) {
      const JobType_schema = Joi.object({
         jobtype_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, JobType_schema);
   }
}
module.exports = new JobTypeValidation();
