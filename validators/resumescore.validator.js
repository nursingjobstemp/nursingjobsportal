const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class ResumeScoreValidation {
   create(req, res, next) {
      const resumescore_schema = Joi.object({
         resume_title:  Joi.string().allow('').allow(null).min(1).optional(),
         resume_fdesc:  Joi.string().allow('').allow(null).min(1).optional(),
         resume_sdesc:  Joi.string().allow('').allow(null).min(1).optional(),
         first_image:  Joi.string().allow('').allow(null).min(1).optional(),
         second_image:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_title:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_description:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_keywords:  Joi.string().allow('').allow(null).min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, resumescore_schema);
   }
   get(req, res, next) {
      const resumescore_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, resumescore_schema);
   }
   update(req, res, next) {
      const resumescore_schema = Joi.object({
         resume_title:  Joi.string().allow('').allow(null).min(1).optional(),
         resume_fdesc:  Joi.string().allow('').allow(null).min(1).optional(),
         resume_sdesc:  Joi.string().allow('').allow(null).min(1).optional(),
         first_image:  Joi.string().allow('').allow(null).min(1).optional(),
         second_image:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_title:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_description:  Joi.string().allow('').allow(null).min(1).optional(),
         seo_keywords:  Joi.string().allow('').allow(null).min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, resumescore_schema);
   }
   updateStatus(req, res, next) {
      const resumescore_schema = Joi.object({
         resume_ids: Joi.array().raw().required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, resumescore_schema);
   }
   delete(req, res, next) {
      const resumescore_schema = Joi.object({
         resume_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, resumescore_schema);
   }
}
module.exports = new ResumeScoreValidation();
