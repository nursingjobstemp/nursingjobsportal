const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class SalaryValidation {
   create(req, res, next) {
      const salary_schema = Joi.object({
         sal_name: Joi.string().min(1).required(),
         sal_slug: Joi.string().min(1).required(),
         sal_pos: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, salary_schema);
   }
   get(req, res, next) {
      const salary_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, salary_schema);
   }
   update(req, res, next) {
      const salary_schema = Joi.object({
         sal_name: Joi.string().min(1).optional(),
         sal_slug: Joi.string().min(1).optional(),
         sal_pos: Joi.number().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, salary_schema);
   }
   updateStatus(req, res, next) {
      const salary_schema = Joi.object({
         sal_ids: Joi.array().raw().required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, salary_schema);
   }
   updatePosition(req, res, next) {
      const sallary_schema = Joi.object({
         positionArray: Joi.array().items({
            sal_id: Joi.number().min(1).required(),
            position: Joi.number().min(1).required()
         }).min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, sallary_schema);
   }
   delete(req, res, next) {
      const salary_schema = Joi.object({
         sal_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, salary_schema);
   }
}
module.exports = new SalaryValidation();
