const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class EmpjobcatValidation {
   create(req, res, next) {
      const empjobcat_schema = Joi.object({
         emp_id: Joi.number().min(1).required(),
         cat_id: Joi.number().min(1).required(),
         subcat_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateBody(req, res, next, empjobcat_schema);
   }
   get(req, res, next) {
      const empjobcat_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional()
      });
      return BaseValidation.validateQuery(req, res, next, empjobcat_schema);
   }
   update(req, res, next) {
      const empjobcat_schema = Joi.object({
         emp_id: Joi.number().min(1).optional(),
         cat_id: Joi.number().min(1).optional(),
         subcat_id: Joi.number().min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, empjobcat_schema);
   }
   updateStatus(req, res, next) {
      const empjobcat_schema = Joi.object({
         mjcat_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required(),
      });
      return BaseValidation.validateBody(req, res, next, empjobcat_schema);
   }
   delete(req, res, next) {
      const empjobcat_schema = Joi.object({
         mjcat_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, empjobcat_schema);
   }
}
module.exports = new EmpjobcatValidation();
