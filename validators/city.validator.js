const Joi = require('joi');
const BaseValidation = require("../middleware/baseValidation");

class CityValidation {
   create(req, res, next) {
      const city_schema = Joi.object({
         state_id: Joi.number().min(1).required(),
         country_id: Joi.number().min(1).required(),
         city_name: Joi.string().min(1).required(),
         city_slug: Joi.string().min(1).required(),
         city_code: Joi.number().min(1).optional(),
         city_image: Joi.string().allow(null).min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, city_schema);
   }
   get(req, res, next) {
      const city_schema = Joi.object({
         _start: Joi.number().allow('').optional(),
         _limit: Joi.number().allow('').optional(),
         status: Joi.string().allow('').valid('Y', 'N', 'D').optional(),
         stateId: Joi.number().allow('').optional(),
         countryId: Joi.number().allow('').optional(),
         id: Joi.number().allow('').optional(),
         search: Joi.string().allow('').optional()
      });
      return BaseValidation.validateQuery(req, res, next, city_schema);
   }
   update(req, res, next) {
      const city_schema = Joi.object({
         state_id: Joi.number().min(1).optional(),
         country_id: Joi.number().min(1).optional(),
         city_name: Joi.string().min(1).optional(),
         city_slug: Joi.string().min(1).optional(),
         city_code: Joi.number().min(1).optional(),
         city_image: Joi.string().min(1).optional(),
         foot_status: Joi.string().valid('Y', 'N').min(1).optional()
      });
      return BaseValidation.validateBody(req, res, next, city_schema);
   }
   updateStatus(req, res, next) {
      const city_schema = Joi.object({
         city_ids: Joi.array().items(Joi.number()).min(1).required(),
         status: Joi.string().valid('Y', 'N', 'D').required()
      });
      return BaseValidation.validateBody(req, res, next, city_schema);
   }
   delete(req, res, next) {
      const city_schema = Joi.object({
         city_id: Joi.number().min(1).required()
      });
      return BaseValidation.validateQuery(req, res, next, city_schema);
   }
}
module.exports = new CityValidation();
