'use strict';
const ResumeScore = require('../models/resumescore.model');

class ResumeService { }
ResumeService.create = async (payload) => {
   try {
      let saved = new ResumeScore(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

ResumeService.findOne = async (findwhere) => {
   try {
      const founded = await ResumeScore.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

ResumeService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await ResumeScore.count(totalWhere);
      const rows = await ResumeScore.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

ResumeService.find = async () => {
   try {
      const rows = await ResumeScore.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

ResumeService.findByPk = async (resume_id) => {
   try {
      const findByPk = await ResumeScore.findOne({ resume_id: resume_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

ResumeService.update = async (resume_id, obj) => {
   try {
      const updateById = await ResumeScore.updateOne({ resume_id: resume_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

ResumeService.bulkUpdateLastupdate = async (resume_ids) => {
   try {
      const updated = await ResumeScore.updateMany({ resume_id: { $in: resume_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

ResumeService.delete = async (resume_id) => {
   try {
      const deleted = await ResumeScore.deleteOne({ resume_id: resume_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = ResumeService;