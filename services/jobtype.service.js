'use strict';
const JobType = require('../models/jobtype.model');

class JobTypeService { }
JobTypeService.create = async (payload) => {
   try {
      let saved = new JobType(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

JobTypeService.findOne = async (findwhere) => {
   try {
      const founded = await JobType.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

JobTypeService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await JobType.count(totalWhere);
      const rows = await JobType.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
         .sort({ jtype_pos: 1 });
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

JobTypeService.find = async () => {
   try {
      const rows = await JobType.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

JobTypeService.findByPk = async (jtype_id) => {
   try {
      const findByPk = await JobType.findOne({ jtype_id: jtype_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

JobTypeService.update = async (jtype_id, obj) => {
   try {
      const updateById = await JobType.updateOne({ jtype_id: jtype_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

JobTypeService.bulkUpdateStatus = async (jtype_ids, status) => {
   try {
      const updated = await JobType.updateMany({ jtype_id: { $in: jtype_ids } }, { jtype_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

JobTypeService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {};
      positionArray.forEach(async val => {
         update = await JobType.updateOne({ jtype_id: val.jtype_id }, { jtype_pos: val.position });
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await JobTypeService.bulkUpdateLastupdate([Number(val.jtype_id)]);
         }
      });
      return update;
   } catch (err) {
      return err;
   }
};

JobTypeService.bulkUpdateLastupdate = async (jtype_ids) => {
   try {
      const updated = await JobType.updateMany({ jtype_id: { $in: jtype_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

JobTypeService.delete = async (jtype_id) => {
   try {
      const deleted = await JobType.deleteOne({ jtype_id: jtype_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = JobTypeService;