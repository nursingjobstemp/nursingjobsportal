'use strict';
const Enquiry = require('../models/enquiry.model');

class EnquiryService { }
EnquiryService.create = async (payload) => {
   try {
      let saved = new Enquiry(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

EnquiryService.findOne = async (findwhere) => {
   try {
      const founded = await Enquiry.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

EnquiryService.findAllAndCount = async (where, _start, _limit) => {
   try {
      const totalCount = await Enquiry.count();
      const rows = await Enquiry.find(where).skip(_start).limit(_limit);
      const count = rows.length;
      return { count, rows, totalCount };
   } catch (err) {
      return err;
   }
};

EnquiryService.find = async () => {
   try {
      const rows = await Enquiry.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

EnquiryService.findByPk = async (enq_id) => {
   try {
      const findByPk = await Enquiry.findOne({ enq_id: enq_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

EnquiryService.update = async (enq_id, obj) => {
   try {
      const updateById = await Enquiry.updateOne({ enq_id: enq_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

EnquiryService.bulkUpdateLastupdate = async (enq_ids) => {
   try {
      const updated = await Enquiry.updateMany({ enq_id: { $in: enq_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

EnquiryService.delete = async (enq_id) => {
   try {
      const deleted = await Enquiry.deleteOne({ enq_id: enq_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = EnquiryService;