'use strict'
const JobsCategory = require('../models/jobscategory.model')

class JobsCategoyService { }
JobsCategoyService.create = async (payload) => {
   try {
      let saved = new JobsCategory(payload);
      await saved.save()
      return saved
   } catch (error) {
      return error
   }
}

JobsCategoyService.findOne = async (findwhere) => {
   try {
      const founded = await JobsCategory.findOne(findwhere)
      return founded
   } catch (error) {
      return error
   }
}

JobsCategoyService.findAllAndCount = async (where, _start, _limit, totalWhere, sortBy) => {
   try {
      const totalCount = await JobsCategory.count(totalWhere)
      const rows = await JobsCategory.aggregate([{ $match: { $and: [where, totalWhere] } }]).sort(sortBy).skip(_start).limit(_limit)
      const data = await JobsCategory.aggregate([
         {
            $match: {
               $and: [
                  where,
                  totalWhere
               ]
            }
         },
         {
            $lookup: {
               from: 'col__jobscategory',
               localField: 'jcat_id',
               foreignField: 'pid',
               as: 'maincat'
            }
         },
         { $unwind: '$maincat' },
         {
            $group: {
               _id: '$jcat_id',
               data: { $first: '$$ROOT' },
               totalCount: { $sum: 1 }
            }
         },
         {
            $project: {
               jcat_id: '$data.jcat_id', pid: '$data.pid', total_count: '$totalCount',
               jcat_name: '$data.jcat_name', jcat_slug: '$data.jcat_slug',
               jcat_code: '$data.jcat_code', jcat_icon: '$data.jcat_icon',
               jcat_desc: '$data.jcat_desc', jcat_image: '$data.jcat_image',
               jcat_pos: '$data.jcat_pos', jcat_status: '$data.jcat_status',
               foot_status: '$data.foot_status', jcat_dt: '$data.jcat_dt',
               jcat_lastupdate: '$data.jcat_lastupdate', jcat_ids: '$data.jcat_id', _id: 0
            }
         }
      ]).sort(sortBy)

      const maped = []
      rows.map((i) => {
         const Int = data.find(x => x.jcat_id === i.jcat_id)
         Int ? i.totalCount = Int.total_count : i.totalCount = 0
         maped.push(i)
      })
      const count = maped.length
      return { totalCount, count, rows: maped }
   } catch (err) {
      return err
   }
}

JobsCategoyService.find = async () => {
   try {
      const rows = await JobsCategory.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

JobsCategoyService.findByPk = async (jcat_id) => {
   try {
      const findByPk = await JobsCategory.findOne({ jcat_id: jcat_id })
      return findByPk
   } catch (err) {
      return err
   }
}

JobsCategoyService.update = async (jcat_id, obj) => {
   try {
      const updateById = await JobsCategory.updateOne({ jcat_id: jcat_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

JobsCategoyService.bulkUpdateStatus = async (jcat_ids, status) => {
   try {
      const updated = await JobsCategory.updateMany({ jcat_id: { $in: jcat_ids } }, { jcat_status: status })
      return updated
   } catch (err) {
      return err
   }
}

JobsCategoyService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {}
      positionArray.forEach(async val => {
         update = await JobsCategory.updateOne({ jcat_id: val.jcat_id }, { jcat_pos: val.position })
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await JobsCategoyService.bulkUpdateLastupdate([Number(val.jcat_id)])
         }
      })
      return update
   } catch (err) {
      return err
   }
}

JobsCategoyService.bulkUpdateLastupdate = async (jcat_ids) => {
   try {
      const updated = await JobsCategory.updateMany({ jcat_id: { $in: jcat_ids } }, { jcat_lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

JobsCategoyService.delete = async (jcat_id) => {
   try {
      const deleted = await JobsCategory.deleteOne({ jcat_id: jcat_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = JobsCategoyService
