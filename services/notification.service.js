'use strict';
const Notification = require('../models/notification.model');

class NotificationService { }
NotificationService.create = async (payload) => {
   try {
      let saved = new Notification(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

NotificationService.findOne = async (findwhere) => {
   try {
      const founded = await Notification.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

NotificationService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Notification.count(totalWhere);
      const rows = await Notification.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

NotificationService.find = async () => {
   try {
      const rows = await Notification.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

NotificationService.findByPk = async (notify_id) => {
   try {
      const findByPk = await Notification.findOne({ notify_id: notify_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

NotificationService.update = async (notify_id, obj) => {
   try {
      const updateById = await Notification.updateOne({ notify_id: notify_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

NotificationService.bulkUpdateStatus = async (notify_ids, status) => {
   try {
      const updated = await Notification.updateMany({ notify_id: { $in: notify_ids } }, { noti_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

NotificationService.bulkUpdateLastupdate = async (notify_ids) => {
   try {
      const updated = await Notification.updateMany({ notify_id: { $in: notify_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

NotificationService.delete = async (notify_id) => {
   try {
      const deleted = await Notification.deleteOne({ notify_id: notify_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = NotificationService;