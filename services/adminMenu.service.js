'use strict'
const privateJob = require("../models/unrestjobpost.model")
const AdminMenu = require('../models/adminMenu.model')
const City = require('../models/city.model')
const JobsCategory = require('../models/jobscategory.model')
const KeySkills = require('../models/keyskills.model')

class AdminMenuService { }
AdminMenuService.findAllAndCount = async (where) => {
   try {
      const findAll = await AdminMenu.find(where).sort({ menu_pos: 1 })
      return findAll
   } catch (err) {
      return err
   }
}

AdminMenuService.getAllCount = async (status, op_id) => {
   try {
      const city = await City.count({ city_status: status })
      const pvtCount = await privateJob.count({
         $and: [
            { posted_status: status },
            op_id,
            { country_id: 100 }
         ]
      })
      const intCount = await privateJob.count({
         $and: [
            { posted_status: status },
            op_id,
            { country_id: { $ne: 100 } }
         ]
      })
      const govCount = await governmentJob.count({
         $and: [
            { posted_status: status },
            op_id
         ]
      })
      const stateCount = await State.count({ state_status: status })
      const jobsCatCount = await JobsCategory.count({ jcat_status: status, pid: 0 })
      const jobsSubCatCount = await JobsCategory.count({ jcat_status: status, pid: { $ne: 0 } })
      const keyskCount = await KeySkills.count({ keysk_status: status })
      const countryCount = await Country.count({ country_status: status })
      const govCategory = await Govcategory.count({ gcat_status: status, pid: 0 })
      const govSubCatCount = await Govcategory.count({ gcat_status: status, pid: { $ne: 0 } })
      const qualification = await Qualification.count({ qual_status: status })
      const obj = {
         city: city,
         pvt: pvtCount,
         int: intCount,
         gov: govCount,
         state: stateCount,
         jobsCategory: jobsCatCount,
         govCategory: govCategory,
         jobsSubCategory: jobsSubCatCount,
         govSubCatCount: govSubCatCount,
         keySkills: keyskCount,
         country: countryCount,
         qualification: qualification
      }
      return obj
   } catch (error) {
      return error
   }
}

AdminMenuService.getAllCount_ex = async (status, op_id) => {
   try {
      const city = await City.count({ city_status: status })
      const pvtCount = await privateJob.count({
         $and: [
            { posted_status: status },
            op_id,
            { country_id: 100 }
         ]
      })
      const intCount = await privateJob.count({
         $and: [
            { posted_status: status },
            op_id,
            { country_id: { $ne: 100 } }
         ]
      })
      const govCount = await governmentJob.count({
         $and: [
            { posted_status: status },
            op_id
         ]
      })
      const stateCount = await State.count({ state_status: status })
      const jobsCatCount = await JobsCategory.count({ jcat_status: status, pid: 0 })
      const jobsSubCatCount = await JobsCategory.count({ jcat_status: status, pid: { $ne: 0 } })
      const keyskCount = await KeySkills.count({ keysk_status: status })
      const countryCount = await Country.count({ country_status: status })
      const govCategory = await Govcategory.count({ gcat_status: status, pid: 0 })
      const govSubCatCount = await Govcategory.count({ gcat_status: status, pid: { $ne: 0 } })
      const qualification = await Qualification.count({ qual_status: status })
      const obj = {
         city: city,
         pvt: pvtCount,
         int: intCount,
         gov: govCount,
         state: stateCount,
         jobsCategory: jobsCatCount,
         govCategory: govCategory,
         jobsSubCategory: jobsSubCatCount,
         govSubCatCount: govSubCatCount,
         keySkills: keyskCount,
         country: countryCount,
         qualification: qualification
      }
      return obj
   } catch (error) {
      return error
   }
}


module.exports = AdminMenuService