'use strict';
const EmpOffDetails = require('../models/empoffdetails.model');

class EmpOffDetailsService { }
EmpOffDetailsService.create = async (payload) => {
   try {
      let saved = new EmpOffDetails(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

EmpOffDetailsService.findOne = async (obj) => {
   try {
      const founded = await EmpOffDetails.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

EmpOffDetailsService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await EmpOffDetails.count(totalWhere);
      const rows = await EmpOffDetails.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.find = async () => {
   try {
      const rows = await EmpOffDetails.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.findByPk = async (wrk_id) => {
   try {
      const findByPk = await EmpOffDetails.findOne({ wrk_id: wrk_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.update = async (wrk_id, obj) => {
   try {
      const updateById = await EmpOffDetails.updateOne({ wrk_id: wrk_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.bulkUpdateStatus = async (wrk_ids, status) => {
   try {
      const updated = await EmpOffDetails.updateMany({ wrk_id: { $in: wrk_ids } }, { wrk_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.bulkUpdateLastupdate = async (wrk_ids) => {
   try {
      const updated = await EmpOffDetails.updateMany({ wrk_id: { $in: wrk_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

EmpOffDetailsService.delete = async (wrk_id) => {
   try {
      const deleted = await EmpOffDetails.deleteOne({ wrk_id: wrk_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = EmpOffDetailsService;
