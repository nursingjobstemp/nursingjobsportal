'use strict'
const Edu_Course = require('../models/edu_course.model')

class Edu_CourseService { }
Edu_CourseService.create = async (payload) => {
   try {
      let saved = new Edu_Course(payload)
      await saved.save()
      return saved
   } catch (error) {
      return error
   }
}

Edu_CourseService.findOne = async (obj) => {
   try {
      const founded = await Edu_Course.findOne(obj)
      return founded
   } catch (error) {
      return error
   }
}

Edu_CourseService.findAllAndCount = async (where, _start, _limit, totalWhere, which) => {
   try {
      let query = {}
      const totalCount = await Edu_Course.aggregate([{ $match: totalWhere }])
      const data = await Edu_Course.aggregate([{ $match: { $and: [where, totalWhere] } }]).skip(_start).limit(_limit)
      if (!which) {
         query = [
            { $match: totalWhere },
            {
               $lookup: {
                  from: 'col__edu_course',
                  localField: 'ecat_id',
                  foreignField: 'pid',
                  as: 'category'
               }
            },
            { $unwind: '$category' },
            {
               $group: {
                  _id: '$ecat_id',
                  data: { $first: '$$ROOT' },
                  totalCount: { $sum: 1 }
               }
            },
            {
               $project: {
                  ecat_id: '$data.ecat_id',
                  totalCount: '$totalCount'
               }
            }
         ]
      } else {
         query = [
            { $match: totalWhere },
            {
               $lookup: {
                  from: 'col__edu_course',
                  localField: 'ecat_id',
                  foreignField: 'ecatid_sub',
                  as: 'category'
               }
            },
            { $unwind: '$category' },
            {
               $group: {
                  _id: '$category.ecatid_sub',
                  data: { $first: '$$ROOT' },
                  totalCount: { $sum: 1 }
               }
            },
            {
               $project: {
                  ecat_id: '$data.ecat_id',
                  totalCount: '$totalCount'
               }
            }
         ]
      }
      const rows = await Edu_Course.aggregate(query)
      const maped = []
      data.map(async (item) => {
         const mascat = await rows.find(x => x.ecat_id === item.ecat_id)
         mascat ? item.totalCount = mascat.totalCount : item.totalCount = 0
         maped.push(item)
      })
      return { totalCount: totalCount.length, count: data.length, rows: maped }
   } catch (err) {
      return err
   }
}

Edu_CourseService.findAllMast = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Edu_Course.aggregate([{ $match: totalWhere }])
      const data = await Edu_Course.aggregate([{ $match: { $and: [where, totalWhere] } }]).skip(_start).limit(_limit)
      const rows = await Edu_Course.aggregate([
         { $match: totalWhere },
         {
            $lookup: {
               from: 'col__edu_course',
               localField: 'ecat_id',
               foreignField: 'pid',
               as: 'mastCat'
            }
         },
         { $unwind: '$mastCat' },
         {
            $group: {
               _id: '$ecat_id',
               data: { $first: '$$ROOT' },
               totalCount: { $sum: 1 }
            }
         },
         {
            $project: {
               ecat_id: '$data.ecat_id',
               totalCount: '$totalCount'
            }
         }
      ])
      const maped = []
      data.map(async (item) => {
         const mascat = await rows.find(x => x.ecat_id === item.ecat_id)
         mascat ? item.totalCount = mascat.totalCount : item.totalCount = 0
         maped.push(item)
      })
      return { totalCount: totalCount.length, count: data.length, rows: maped }
   } catch (err) {
      return err
   }
}

Edu_CourseService.findAllMain = async (where, _start, _limit, totalWhere, mainCatId) => {
   try {
      const totalCount = await Edu_Course.aggregate([{ $match: totalWhere }])
      const data = await Edu_Course.aggregate([{ $match: { $and: [where, totalWhere] } }]).skip(_start).limit(_limit)
      const rows = await Edu_Course.aggregate([
         {
            $lookup: {
               from: 'col__edu_course',
               localField: 'ecat_id',
               foreignField: 'ecatid_sub',
               as: 'mainCat'
            }
         },
         { $unwind: '$mainCat' },
         {
            $group: {
               _id: '$mainCat.ecatid_sub',
               data: { $first: '$$ROOT' },
               totalCount: { $sum: 1 }
            }
         },
         {
            $project: {

               ecat_id: '$data.ecat_id',
               totalCount: '$totalCount'
            }
         }
      ])
      const maped = []
      data.map((item) => {
         const maincat = rows.find(x => x.ecat_id === item.ecat_id)
         maincat ? item.totalCount = maincat.totalCount : item.totalCount = 0
         maped.push(item)
      })
      return { totalCount: totalCount.length, count: data.length, rows: maped }
   } catch (err) {
      return err
   }
}

Edu_CourseService.findAllSub = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Edu_Course.aggregate([{ $match: totalWhere }])
      const rows = await Edu_Course.aggregate([{ $match: { $and: [where, totalWhere] } }]).skip(_start).limit(_limit)
      return { totalCount: totalCount.length, count: rows.length, rows }
   } catch (err) {
      return err
   }
}

Edu_CourseService.find = async () => {
   try {
      const rows = await Edu_Course.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

Edu_CourseService.findByPk = async (ecat_id) => {
   try {
      const findByPk = await Edu_Course.findOne({ ecat_id: ecat_id })
      return findByPk
   } catch (err) {
      return err
   }
}

Edu_CourseService.update = async (ecat_id, obj) => {
   try {
      const updateById = await Edu_Course.updateOne({ ecat_id: ecat_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

Edu_CourseService.bulkUpdateStatus = async (ecat_ids, status) => {
   try {
      const updated = await Edu_Course.updateMany({ ecat_id: { $in: ecat_ids } }, { ecat_status: status })
      return updated
   } catch (err) {
      return err
   }
}

Edu_CourseService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {}
      positionArray.forEach(async val => {
         update = await Edu_Course.updateOne({ ecat_id: val.ecat_id }, { ecat_pos: val.position })
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await Edu_CourseService.bulkUpdateLastupdate([Number(val.ecat_id)])
         }
      })
      return update
   } catch (err) {
      return err
   }
}

Edu_CourseService.bulkUpdateLastupdate = async (ecat_ids) => {
   try {
      const updated = await Edu_Course.updateMany({ ecat_id: { $in: ecat_ids } }, { gcat_lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

Edu_CourseService.delete = async (ecat_id) => {
   try {
      const deleted = await Edu_Course.deleteOne({ ecat_id: ecat_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = Edu_CourseService
