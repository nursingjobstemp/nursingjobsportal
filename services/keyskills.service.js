'use strict';
const KeySkills = require('../models/keyskills.model');

class KeySkillsService { }
KeySkillsService.create = async (payload) => {
   try {
      let saved = new KeySkills(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

KeySkillsService.findOne = async (obj) => {
   try {
      const founded = await KeySkills.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

KeySkillsService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await KeySkills.count(totalWhere);
      const rows = await KeySkills.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start).limit(_limit).sort({ keysk_pos: 1, keysk_name: 1 });
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

KeySkillsService.find = async () => {
   try {
      const rows = await KeySkills.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

KeySkillsService.findByPk = async (keysk_id) => {
   try {
      const findByPk = await KeySkills.findOne({ keysk_id: keysk_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

KeySkillsService.update = async (keysk_id, obj) => {
   try {
      const updateById = await KeySkills.updateOne({ keysk_id: keysk_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

KeySkillsService.bulkUpdateStatus = async (keysk_ids, status) => {
   try {
      const updated = await KeySkills.updateMany({ keysk_id: { $in: keysk_ids } }, { keysk_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

KeySkillsService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {};
      positionArray.forEach(async val => {
         update = await KeySkills.updateOne({ keysk_id: val.keysk_id }, { keysk_pos: val.position });
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await KeySkillsService.bulkUpdateLastupdate([Number(val.keysk_id)]);
         }
      });
      return update;
   } catch (err) {
      return err;
   }
};

KeySkillsService.bulkUpdateLastupdate = async (keysk_ids) => {
   try {
      const updated = await KeySkills.updateMany({ keysk_id: { $in: keysk_ids } }, { keysk_lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

KeySkillsService.delete = async (keysk_id) => {
   try {
      const deleted = await KeySkills.deleteOne({ keysk_id: keysk_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = KeySkillsService;