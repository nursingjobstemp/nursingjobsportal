'use strict';
const Contact_Resume = require('../models/contactresume.model');

class Contact_ResumeService { }
Contact_ResumeService.create = async (payload) => {
   try {
      let saved = new Contact_Resume(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

Contact_ResumeService.findOne = async (findwhere) => {
   try {
      const founded = await Contact_Resume.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

Contact_ResumeService.findAllAndCount = async (_start, _limit, cont_type) => {
   try {
      const totalCount = await Contact_Resume.count(cont_type);
      const rows = await Contact_Resume.find(cont_type).skip(_start).limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

Contact_ResumeService.find = async () => {
   try {
      const rows = await Contact_Resume.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

Contact_ResumeService.findByPk = async (cont_id) => {
   try {
      const findByPk = await Contact_Resume.findOne({ cont_id: cont_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

Contact_ResumeService.update = async (cont_id, obj) => {
   try {
      const updateById = await Contact_Resume.updateOne({ cont_id: cont_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

Contact_ResumeService.bulkUpdateLastupdate = async (cont_ids) => {
   try {
      const updated = await Contact_Resume.updateMany({ cont_id: { $in: cont_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

Contact_ResumeService.delete = async (cont_id) => {
   try {
      const deleted = await Contact_Resume.deleteOne({ cont_id: cont_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = Contact_ResumeService;
