'use strict';
const City = require('../models/city.model');

class CityService { }
CityService.findOne = async (obj) => {
   try {
      const city = await City.findOne(obj);
      return city;
   } catch (error) {
      return error;
   }
};

CityService.find = async () => {
   try {
      const rows = await City.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

CityService.create = async (payload) => {
   try {
      let saved = new City(payload);
      await saved.save();
      return saved;
   } catch (error) {
      return error;
   }
};

CityService.findAllAndCount = async (_start, _limit, totalWhere, queryWhere, sortBy) => {
   try {
      let totalCount = await City.count(totalWhere);
      const rows = await City.aggregate([
         {
            $lookup: {
               from: 'col__city',
               localField: 'city_id',
               foreignField: 'city_id',
               as: 'cityData'
            }
         },
         { $unwind: '$cityData' },
         {
            $lookup: {
               from: 'col__state',
               localField: 'cityData.state_id',
               foreignField: 'state_id',
               as: 'stateData'
            }
         },
         { $unwind: '$stateData' },
         {
            $lookup: {
               from: 'col__country',
               localField: 'stateData.country_id',
               foreignField: 'country_id',
               as: 'countryData'
            }
         },
         { $unwind: '$countryData' },
         { $sort: sortBy ? sortBy : { 'countryData.country_name': 1 } },
         {
            $match: {
               $and: [
                  totalWhere,
                  queryWhere
               ]
            }
         },
         {
            $project: {
               city: '$cityData',
               stateName: '$stateData.state_name',
               countryName: '$countryData.country_name',
               count: { $sum: 1 }
            }
         }
      ]).skip(_start).limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      
      return err;
   }
};

CityService.findByPk = async (city_id) => {
   try {
      const findByPk = await City.findOne({ city_id: city_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

CityService.update = async (city_id, obj) => {
   try {
      const updateById = await City.updateOne({ city_id: city_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

CityService.bulkUpdateStatus = async (city_ids, status) => {
   try {
      const updated = await City.updateMany({ city_id: { $in: city_ids } }, { city_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

CityService.bulkUpdateLastupdate = async (city_ids) => {
   try {
      const updated = await City.updateMany({ city_id: { $in: city_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

CityService.delete = async (city_id) => {
   try {
      const deleted = await City.deleteOne({ city_id: city_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = CityService;