'use strict';
const Emp_KeySkills = require('../models/empkskills.model');

class Emp_KeySkillsService { }
Emp_KeySkillsService.create = async (payload) => {
   try {
      let saved = new Emp_KeySkills(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

Emp_KeySkillsService.findOne = async (obj) => {
   try {
      const founded = await Emp_KeySkills.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

Emp_KeySkillsService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Emp_KeySkills.count(totalWhere);
      const rows = await Emp_KeySkills.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.find = async () => {
   try {
      const rows = await Emp_KeySkills.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.findByPk = async (empkskil_id) => {
   try {
      const findByPk = await Emp_KeySkills.findOne({ empkskil_id: empkskil_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.update = async (empkskil_id, obj) => {
   try {
      const updateById = await Emp_KeySkills.updateOne({ empkskil_id: empkskil_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.bulkUpdateStatus = async (empkskil_ids, status) => {
   try {
      const updated = await Emp_KeySkills.updateMany({ empkskil_id: { $in: empkskil_ids } }, { empkskil_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.bulkUpdateLastupdate = async (empkskil_ids) => {
   try {
      const updated = await Emp_KeySkills.updateMany({ empkskil_id: { $in: empkskil_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

Emp_KeySkillsService.delete = async (empkskil_id) => {
   try {
      const deleted = await Emp_KeySkills.deleteOne({ empkskil_id: empkskil_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = Emp_KeySkillsService;
