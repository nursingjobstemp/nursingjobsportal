'use strict';
const Intjobpost = require('../models/intjobpost.model');
const mongoose = require('mongoose');

class IntjobpostService { }
IntjobpostService.create = async (payload) => {
    try {
        let saved = new Intjobpost(payload)
        await saved.save()
        return saved
     } catch (error) {
        return error
     }
};

IntjobpostService.findOne = async (findwhere) => {
    try {
        const founded = await Intjobpost.findOne(findwhere);
        return founded;
    } catch (error) {
        return error;
    }
};

IntjobpostService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
    try {
        const totalCount = await Intjobpost.count(totalWhere);
        const rows = await Intjobpost.aggregate([
            { $skip: _start },
            { $limit: _limit },
            {
                $match: {
                    $and: [
                        where,
                        totalWhere
                    ]
                }
            },
            { $sort: { posted_date: -1 } },
            {
                $project: {
                    _id: 0, unrest_jcode: 1,
                    posted_date: 1, posted_pos: 1, unrst_jid: 1, duplicate_from: 1, jcat_type: 1, apply: 1,
                    unrest_jcat: 1, unrest_jsubcat: 1, unrest_jname: 1, unrest_jdesc: 1, unrest_jeducat: 1,
                    unrest_jquali: 1, quali_type: 1, qualif_txt: 1, age_limit: 1, job_detail: 1, web_url: 1,
                    unrest_jrequ: 1, unrest_jallow: 1, job_type: 1, unrest_jemail: 1, no_of_openings: 1,
                    key_skills: 1, job_exp: 1, country_id: 1, state: 1, unrest_jloct: 1, unrest_jcompany: 1,
                    unrest_jphone: 1, unrest_sal: 1, sec_title: 1, all_india: 1, statename: 1, cityname: 1,
                    sec_jobtitle: 1, ip_address: 1, posted_id: 1, posted_by: 1, posted_name: 1, exp_date: 1,
                    posted_status: 1, posted_lastupdate: 1
                }
            }

        ]);
        const JobPostList = await Intjobpost.aggregate([
            { $skip: _start },
            { $limit: _limit },
            {
                $lookup: {
                    from: 'col__jobscategory',
                    localField: 'unrest_jcat',
                    foreignField: 'jcat_id',
                    as: 'CategoryData'
                }
            },
            { $unwind: '$CategoryData' },
            {
                $match: {
                    $and: [
                        where,
                        totalWhere
                    ]
                }
            },
            { $sort: { posted_date: -1 } },
            {
                $project: {
                    mainCategoryName: '$CategoryData.jcat_name',
                    mainCategory_id: '$CategoryData.jcat_id'
                }
            }
        ]);
        const subCategory = await Intjobpost.aggregate([
            { $skip: _start },
            { $limit: _limit },
            {
                $lookup: {
                    from: 'col__jobscategory',
                    localField: 'unrest_jsubcat',
                    foreignField: 'jcat_id',
                    as: 'subCategoryData'
                }
            },
            { $unwind: '$subCategoryData' },
            {
                $project: {
                    subCategory_id: '$subCategoryData.jcat_id',
                    SubCategory: '$subCategoryData.jcat_name'
                }
            }
        ]);
        const jobsCategory = await Intjobpost.aggregate([
            { $skip: _start },
            { $limit: _limit },
            {
                $lookup: {
                    from: 'col__jobposting',
                    localField: 'unrest_jcat',
                    foreignField: 'jcat_id',
                    as: 'jobsCategoryData'
                }
            },
            { $unwind: '$jobsCategoryData' },
            {
                $project: {
                    jobsCategory_id: '$jobsCategoryData.jcat_id',
                    jobsCategory: '$jobsCategoryData.jcat_name'
                }
            }
        ]);
        const location = await Intjobpost.aggregate([
            { $skip: _start },
            { $limit: _limit },
            {
                $lookup: {
                    from: 'col__city',
                    localField: 'unrest_jloct',
                    foreignField: 'city_id',
                    as: 'CityData'
                }
            },
            { $unwind: '$CityData' },
            {
                $project: {
                    location_id: '$CityData.city_id',
                    location: '$CityData.city_name'
                }
            }
        ]);
        const formattedData = [];
        rows.map((item) => {
            const checkExistJobPostList = JobPostList.find(x => x.mainCategory_id === item.unrest_jcat);
            const checkExist_sub = subCategory.find(x => x.subCategory_id === item.unrest_jsubcat);
            const checkExist_jobc = jobsCategory.find(x => x.jobsCategory === item.unrest_jsubcat);
            const checkExist_loc = location.find(x => x.location_id === item.unrest_jloct);
            if (!checkExistJobPostList) {
                item.mainCategoryName = 0;
                item.mainCategory_id = 0;
            } else {
                item.mainCategoryName = checkExistJobPostList.mainCategoryName;
                item.mainCategory_id = checkExistJobPostList.mainCategory_id;
            }
            !checkExist_sub ? item.SubCategory = 0 : item.SubCategory = checkExist_sub.SubCategory;
            !checkExist_jobc ? item.jobsCategory = 0 : item.jobsCategory = checkExist_jobc.jobsCategory;
            !checkExist_loc ? item.location = 0 : item.location = checkExist_loc.location;
            formattedData.push(item);
        });
        const count = formattedData.length;
        return { totalCount, count, rows: formattedData };
    } catch (err) {
        return err;
    }
};

IntjobpostService.getCompany = async (where, _start, _limit, totalWhere) => {
    try {
        const totalCount = await Intjobpost.count(totalWhere);
        const rows = await Intjobpost.find({
            $and: [
                where,
                totalWhere
            ]
        }).skip(_start)
            .limit(_limit).sort({ posted_date: -1 });
        const count = rows.length;
        return { totalCount, count, rows };
    } catch (err) {
        return err;
    }
};

// IntjobpostService.getDuplicateJob = async (where, _start, _limit, totalWhere) => {
//     try {
//         let dup = await Intjobpost.find(totalWhere, { _id: 1 });
//         let duplicate = dup.map(x => new mongoose.Types.ObjectId(x));
//         const totalCount = await Intjobpost.count({ _id: { $in: duplicate }, totalWhere });
//         const rows = await Intjobpost.find({
//             $and: [
//                 { _id: { $in: duplicate } },
//                 where,
//                 totalWhere
//             ]
//         }).sort({ posted_date: -1 }).skip(_start).limit(_limit);
//         return { totalCount, count: rows.length, rows };
//     } catch (err) {
//         return err;
//     }
// };

IntjobpostService.getDuplicateJob = async (where, _start, _limit, totalWhere) => {
    try {
       const totalCount = await Intjobpost.aggregate([{ $match: totalWhere }])
       const rows = await Intjobpost.aggregate([
          {
             $match: {
                $and: [
                   totalWhere,
                   where
                ]
             }
          },
          { $skip: _start },
          { $sort: { posted_date: -1 } },
          {
             $lookup: {
                from: 'col__jobscategory',
                localField: 'unrest_jcat',
                foreignField: 'jcat_id',
                as: 'jobsCat'
             }
          },
          { $unwind: { path: '$jobsCat', preserveNullAndEmptyArrays: false } },
          {
             $lookup: {
                from: 'col__jobscategory',
                localField: 'unrest_jsubcat',
                foreignField: 'jcat_id',
                as: 'subCat'
             }
          },
          { $unwind: { path: '$subCat', preserveNullAndEmptyArrays: false } },
          {
             $lookup: {
                from: 'col__city',
                localField: 'unrest_jloct',
                foreignField: 'city_id',
                as: 'cityData'
             }
          },
          { $unwind: { path: '$cityData', preserveNullAndEmptyArrays: false } },
          { $limit: _limit },
          {
             $project: {
                _id: 0, unrest_jcode: 1,
                posted_date: 1, posted_pos: 1, unrst_jid: 1, duplicate_from: 1, jcat_type: 1, apply: 1,
                unrest_jcat: 1, unrest_jsubcat: 1, unrest_jname: 1, unrest_jdesc: 1, unrest_jeducat: 1,
                unrest_jquali: 1, quali_type: 1, qualif_txt: 1, age_limit: 1, job_detail: 1, web_url: 1,
                unrest_jrequ: 1, unrest_jallow: 1, job_type: 1, unrest_jemail: 1, no_of_openings: 1,
                key_skills: 1, job_exp: 1, country_id: 1, state: 1, unrest_jloct: 1, unrest_jcompany: 1,
                unrest_jphone: 1, unrest_sal: 1, sec_title: 1, all_india: 1, statename: 1, cityname: 1,
                sec_jobtitle: 1, ip_address: 1, posted_id: 1, posted_by: 1, posted_name: 1, exp_date: 1,
                posted_status: 1, posted_lastupdate: 1, jobsCategory: '$jobsCat.jcat_name',
                SubCategory: '$subCat.jcat_name', location: '$cityData.city_name'
             }
          }
       ], { allowDiskUse: true })
       return { totalCount: totalCount.length, count: rows.length, rows }
    } catch (err) {
       return err
    }
 }



IntjobpostService.find = async () => {
    try {
        const rows = await Intjobpost.find({}).sort({ $natural: -1 }).limit(1);
        return { rows };
    } catch (err) {
        return err;
    }
};

IntjobpostService.findByPk = async (unrst_jid) => {
    try {
        const findByPk = await Intjobpost.findOne({ unrst_jid: unrst_jid });
        return findByPk;
    } catch (err) {
        return err;
    }
};

IntjobpostService.update = async (unrst_jid, obj) => {
    try {
        const updateById = await Intjobpost.updateOne({ unrst_jid: unrst_jid }, obj);
        return updateById;
    } catch (err) {
        return err;
    }
};

IntjobpostService.bulkUpdateStatus = async (unrst_jids, status) => {
    try {
        const updated = await Intjobpost.updateMany({ unrst_jid: { $in: unrst_jids } }, { posted_status: status });
        return updated;
    } catch (err) {
        return err;
    }
};

IntjobpostService.bulkUpdateLastupdate = async (unrst_jids) => {
    try {
        const updated = await Intjobpost.updateMany({ unrst_jid: { $in: unrst_jids } }, { posted_lastupdate: new Date() });
        return updated;
    } catch (err) {
        return err;
    }
};

IntjobpostService.delete = async (unrst_jid) => {
    try {
        const deleted = await Intjobpost.deleteOne({ unrst_jid: unrst_jid });
        return deleted;
    } catch (err) {
        return err;
    }
};

module.exports = IntjobpostService;