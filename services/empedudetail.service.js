'use strict';
const Employee_Education_Detail = require('../models/empedudetail.model');

class Employee_Education_DetailService { }
Employee_Education_DetailService.create = async (payload) => {
   try {
      let saved = new Employee_Education_Detail(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

Employee_Education_DetailService.findOne = async (obj) => {
   try {
      const founded = await Employee_Education_Detail.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

Employee_Education_DetailService.findAllAndCount = async (_start, _limit) => {
   try {
      const totalCount = await Employee_Education_Detail.count();
      const rows = await Employee_Education_Detail.find().skip(_start).limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

Employee_Education_DetailService.find = async () => {
   try {
      const rows = await Employee_Education_Detail.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

Employee_Education_DetailService.findByPk = async (edu_id) => {
   try {
      const findByPk = await Employee_Education_Detail.findOne({ edu_id: edu_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

Employee_Education_DetailService.update = async (edu_id, obj) => {
   try {
      const updateById = await Employee_Education_Detail.updateOne({ edu_id: edu_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

Employee_Education_DetailService.bulkUpdateLastupdate = async (edu_ids) => {
   try {
      const updated = await Employee_Education_Detail.updateMany({ edu_id: { $in: edu_ids } }, { edudate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

Employee_Education_DetailService.delete = async (edu_id) => {
   try {
      const deleted = await Employee_Education_Detail.deleteOne({ edu_id: edu_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = Employee_Education_DetailService;
