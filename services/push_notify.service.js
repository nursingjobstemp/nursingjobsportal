'user strict'
const Push_notify = require('../models/push_notify.model')

class push_notifyService { }
push_notifyService.create = async (payload) => {
    try {
        const created = new Push_notify(payload)
        return await created.save()
    } catch (err) {
        return err
    }
}

push_notifyService.findOne = async (obj) => {
    try {
        return await Push_notify.findOne(obj)
    } catch (err) {
        return err
    }
}

push_notifyService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
    try {
        const totalCount = await Push_notify.count(totalWhere)
        const rows = await Push_notify.find({
            $and: [
                where,
                totalWhere
            ]
        }).skip(_start)
            .limit(_limit)
        const count = rows.length
        return { totalCount, count, rows }
    } catch (err) {
        return err
    }
}

push_notifyService.find = async () => {
    try {
        const rows = await Push_notify.find({}).sort({ $natural: -1 }).limit(1)
        return { rows }
    } catch (err) {
        return err
    }
}

push_notifyService.findByPk = async (notify_id) => {
    try {
        const findByPk = await Push_notify.findOne({ notify_id: notify_id })
        return findByPk
    } catch (err) {
        return err
    }
}

push_notifyService.update = async (notify_id, obj) => {
    try {
        const updateById = await Push_notify.updateOne({ notify_id: notify_id }, obj)
        return updateById
    } catch (err) {
        return err
    }
}

push_notifyService.bulkUpdateStatus = async (notify_ids, status) => {
    try {
        const updated = await Push_notify.updateMany({ notify_id: { $in: notify_ids } }, { notify_status: status })
        return updated
    } catch (err) {
        return err
    }
}

push_notifyService.bulkUpdateLastupdate = async (notify_ids) => {
    try {
        const updated = await Push_notify.updateMany({ notify_id: { $in: notify_ids } }, { last_update: new Date() })
        return updated
    } catch (err) {
        return err
    }
}

push_notifyService.delete = async (notify_id) => {
    try {
        const deleted = await Push_notify.deleteOne({ notify_id: notify_id })
        return deleted
    } catch (err) {
        return err
    }
}

module.exports = push_notifyService