'use strict';
const Chat = require('../models/chat.model');

class ChatService { }
ChatService.create = async (payload) => {
   try {
      let saved = new Chat(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

ChatService.findOne = async (obj) => {
   try {
      const founded = await Chat.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

ChatService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Chat.count(totalWhere);
      const rows = await Chat.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

ChatService.find = async () => {
   try {
      const rows = await Chat.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

ChatService.findByPk = async (chat_id) => {
   try {
      const findByPk = await Chat.findOne({ chat_id: chat_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

ChatService.update = async (chat_id, obj) => {
   try {
      const updateById = await Chat.updateOne({ chat_id: chat_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

ChatService.bulkUpdateStatus = async (chat_ids, status) => {
   try {
      const updated = await Chat.updateMany({ chat_id: { $in: chat_ids } }, { chat_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

ChatService.bulkUpdateLastupdate = async (chat_ids) => {
   try {
      const updated = await Chat.updateMany({ chat_id: { $in: chat_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

ChatService.delete = async (chat_id) => {
   try {
      const deleted = await Chat.deleteOne({ chat_id: chat_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = ChatService;