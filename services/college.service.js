'use strict';
const College = require('../models/colg.model');

class CollegeService { }
CollegeService.create = async (payload) => {
   try {
      let saved = new College(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

CollegeService.findOne = async (findwhere) => {
   try {
      const founded = await College.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

CollegeService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await College.count(totalWhere);
      const rows = await College.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
         .sort({ colg_pos: 1 });
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

CollegeService.find = async () => {
   try {
      const rows = await College.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

CollegeService.findByPk = async (colg_id) => {
   try {
      const findByPk = await College.findOne({ colg_id: colg_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

CollegeService.update = async (colg_id, obj) => {
   try {
      const updateById = await College.updateOne({ colg_id: colg_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

CollegeService.bulkUpdateStatus = async (colg_ids, status) => {
   try {
      const updated = await College.updateMany({ colg_id: { $in: colg_ids } }, { colg_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

CollegeService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {};
      positionArray.forEach(async val => {
         update = await College.updateOne({ colg_id: val.colg_id }, { colg_pos: val.position });
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await CollegeService.bulkUpdateLastupdate([Number(val.colg_id)]);
         }
      });
      return update;
   } catch (err) {
      return err;
   }
};

CollegeService.bulkUpdateLastupdate = async (colg_ids) => {
   try {
      const updated = await College.updateMany({ colg_id: { $in: colg_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

CollegeService.delete = async (colg_id) => {
   try {
      const deleted = await College.deleteOne({ colg_id: colg_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = CollegeService;