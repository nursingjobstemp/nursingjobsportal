'use strict'
const Operator = require('../models/operator.model')
class OperatorService { }

OperatorService.create = async (payload) => {
   try {
      let saved = new Operator(payload)
      await saved.save()
      return saved
   } catch (error) {
      return error
   }
}

OperatorService.findOne = async (findwhere) => {
   try {
      const founded = await Operator.findOne(findwhere)
      return founded
   } catch (error) {
      return error
   }
}

OperatorService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Operator.count(totalWhere)
      const opList = await Operator.aggregate([{ $match: { $and: [where, totalWhere] } }]).skip(_start).limit(_limit)
      const int_job = await Operator.aggregate([
         {
            $lookup: {
               from: 'col__unrestjobpost',
               localField: 'op_id',
               foreignField: 'posted_id',
               as: 'Int_Details'
            }
         },
         { $unwind: { path: '$Int_Details', preserveNullAndEmptyArrays: false } },
         {
            $match: {
               'Int_Details.job_type': 'N',
               'Int_Details.posted_status': 'Y',
               'Int_Details.country_id': { $ne: 100 },
               $or: [
                  { 'Int_Details.posted_by': 'O' },
                  { 'Int_Details.posted_by': 'A' },
                  { 'Int_Details.posted_by': 'S' },
               ],
               'Int_Details.exp_date': { $gte: new Date() }
            }
         },
         {
            $group: {
               _id: '$op_id',
               data: { $first: '$$ROOT' },
               Int_total: { $sum: 1 }
            }
         },
         {
            $project: {
               Int_id: '$data.op_id',
               IntCount: '$Int_total'
            }
         }
      ])
      const pvt_job = await Operator.aggregate([
         {
            $lookup: {
               from: 'col__unrestjobpost',
               localField: 'op_id',
               foreignField: 'posted_id',
               as: 'Pvt_Details'
            }
         },
         { $unwind: { path: '$Pvt_Details', preserveNullAndEmptyArrays: false } },
         {
            $match: {
               'Pvt_Details.job_type': 'N',
               'Pvt_Details.posted_status': 'Y',
               'Pvt_Details.country_id': 100,
               $or: [
                  { 'Pvt_Details.posted_by': 'O' },
                  { 'Pvt_Details.posted_by': 'A' },
                  { 'Pvt_Details.posted_by': 'S' },
               ],
               'Pvt_Details.exp_date': { $gte: new Date() },
            }
         },
         {
            $group: {
               _id: '$op_id',
               data: { $first: '$$ROOT' },
               Pvt_Details: { $sum: 1 }
            }
         },
         {
            $project: {
               _id: 0,
               pvt_id: '$data.op_id',
               PvtCount: '$Pvt_Details',
            }
         }
      ])
      const gov_job = await Operator.aggregate([
         {
            $lookup: {
               from: 'col__govtjobpost',
               localField: 'op_id',
               foreignField: 'posted_id',
               as: 'Govtjobpost'
            }
         },
         { $unwind: { path: '$Govtjobpost', preserveNullAndEmptyArrays: false } },
         {
            $match: {
               $or: [
                  { 'Govtjobpost.posted_by': 'A' },
                  { 'Govtjobpost.posted_by': 'S' },
               ],
               'Govtjobpost.posted_status': 'Y',
               'Govtjobpost.exp_date': { $gte: new Date() },
            }
         },
         {
            $group: {
               _id: '$op_id',
               data: { $first: '$$ROOT' },
               Gov_Details: { $sum: 1 }
            }
         },
         {
            $project: {
               _id: 0,
               Gov_id: '$data.op_id',
               GovCount: '$Gov_Details',
            }
         }
      ])
      const maped = []
      opList.map((i) => {
         const Int = int_job.find(x => x.Int_id === i.op_id)
         const Pvt = pvt_job.find(x => x.pvt_id === i.op_id)
         const Gvt = gov_job.find(x => x.Gov_id === i.op_id)
         Int ? i.Int_total = Int.IntCount : i.Int_total = 0
         Pvt ? i.pvt_total = Pvt.PvtCount : i.pvt_total = 0
         Gvt ? i.Gov_total = Gvt.GovCount : i.Gov_total = 0
         maped.push(i)
      })
      return { totalCount, count: opList.length, rows: maped }
   } catch (err) {
      return err
   }
}

OperatorService.data = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Operator.count(totalWhere)
      const rows = await Operator.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
      const count = rows.length
      return { totalCount, count, rows }
   } catch (err) {
      return err
   }
}

OperatorService.find = async () => {
   try {
      const rows = await Operator.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

OperatorService.findByPk = async (op_id) => {
   try {
      const findByPk = await Operator.findOne({ op_id: op_id })
      return findByPk
   } catch (err) {
      return err
   }
}

OperatorService.update = async (op_id, obj) => {
   try {
      const updateById = await Operator.updateOne({ op_id: op_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

OperatorService.bulkUpdateStatus = async (op_ids, status) => {
   try {
      const updated = await Operator.updateMany({ op_id: { $in: op_ids } }, { op_status: status })
      return updated
   } catch (err) {
      return err
   }
}

OperatorService.bulkUpdateLastupdate = async (op_ids) => {
   try {
      const updated = await Operator.updateMany({ op_id: { $in: op_ids } }, { op_lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

OperatorService.delete = async (op_id) => {
   try {
      const deleted = await Operator.deleteOne({ op_id: op_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = OperatorService