'use strict';
const Intschedule = require('../models/intschedule.model');

class IntScheduleService { }
IntScheduleService.create = async (payload) => {
   try {

      let saved = new Intschedule(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

IntScheduleService.findOne = async (findwhere) => {
   try {
      const founded = await Intschedule.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

IntScheduleService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Intschedule.count(totalWhere);
      const rows = await Intschedule.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

IntScheduleService.find = async () => {
   try {
      const rows = await Intschedule.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

IntScheduleService.findByPk = async (intsch_id) => {
   try {
      const findByPk = await Intschedule.findOne({ intsch_id: intsch_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

IntScheduleService.update = async (intsch_id, obj) => {
   try {
      const updateById = await Intschedule.updateOne({ intsch_id: intsch_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

IntScheduleService.bulkUpdateStatus = async (intsch_ids, status) => {
   try {
      const updated = await Intschedule.updateMany({ intsch_id: { $in: intsch_ids } }, { intsch_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

IntScheduleService.bulkUpdateLastupdate = async (intsch_ids) => {
   try {
      const updated = await Intschedule.updateMany({ intsch_id: { $in: intsch_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

IntScheduleService.delete = async (intsch_id) => {
   try {
      const deleted = await Intschedule.deleteOne({ intsch_id: intsch_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = IntScheduleService;