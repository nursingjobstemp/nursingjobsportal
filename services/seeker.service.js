'use strict'
const Seeker = require("../models/employee.model")
const EmpKeySkills = require("../models/empkskills.model")
const EmpOffDetails = require("../models/empoffdetails.model")
const EmpLoct = require("../models/emploct.model")
const Employer = require('../models/recutcomp.model')
const EmpEduDetail = require("../models/empedudetail.model")
const Unrestjobpost = require('../models/unrestjobpost.model')
const City = require('../models/city.model')

class SeekerService { }
SeekerService.create = async (payload) => {
   try {
      let saved = new Seeker(payload)
      return await saved.save()
   } catch (error) {
      return error
   }
}

SeekerService.findOne = async (obj) => {
   try {
      const exited_email = await Seeker.findOne(obj)
      return exited_email
   } catch (error) {
      return error
   }
}

SeekerService.deviceToken = async (user_id) => {
   try {
      const founded = await Seeker.aggregate([
         { $match: { emp_id: { $in: user_id } } },
         {
            $project: {
               _id: 0,
               deviceToken: 1
            }
         }
      ]).then(emp => emp.map(id => id = id.deviceToken))
      return founded
   } catch (error) {
      return error
   }
}

SeekerService.find = async () => {
   try {
      const rows = await Seeker.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

SeekerService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Seeker.count(totalWhere)
      const rows = await Seeker.aggregate([
         {
            $match: {
               $and: [
                  where,
                  totalWhere
               ]
            }
         },
         { $sort: { emp_date: -1 } },
         {
            $lookup: {
               from: 'col__city',
               localField: 'emp_city',
               foreignField: 'city_id',
               as: 'cityData'
            }
         },
         { $unwind: '$cityData' },
         {
            $lookup: {
               from: 'col__state',
               localField: 'emp_state',
               foreignField: 'state_id',
               as: 'stateData'
            }
         },
         { $unwind: '$stateData' },
         {
            $lookup: {
               from: 'col__country',
               localField: 'emp_country',
               foreignField: 'country_id',
               as: 'countryData'
            }
         },
         { $unwind: '$countryData' },
         {
            $project: {
               _id: 0, emp_id: 1, emp_name: 1, emp_email: 1, emp_mobile: 1, emp_pass: 1, email_otp: 1, email_verify: 1,
               mobile_otp: 1, mobile_verify: 1, emp_dob: 1, emp_gender: 1, emp_cat: 1, emp_subcat: 1, cat_name: 1,
               subcat_name: 1, emp_desig: 1, emp_typeval: 1, emp_comp: 1, min_sal: 1, sal_lakh: 1, sal_thousands: 1,
               exp_year: 1, exp_month: 1, emp_photo: 1, emp_resumeheadline: 1, emp_country: 1, emp_state: 1, emp_city: 1,
               high_course: 1, high_special: 1, high_college: 1, colg_name: 1, course_type: 1, exp_type: 1, ipaddress: 1,
               high_pass_yr: 1, high_qualif: 1, emp_resume: 1, emp_pincode: 1, emp_marital: 1, emp_address: 1, user_type: 1,
               emp_status: 1, emp_date: 1, lastupdate: 1, city_name: 1, cityName: '$cityData.city_name', registerdate: '$emp_date',
               state_name: '$stateData.state_name', country_name: '$countryData.country_name'
            }
         }
      ]).skip(_start).limit(_limit)
      const appliedCount = await Seeker.aggregate([
         {
            $lookup: {
               from: 'col__jobapplied',
               localField: 'emp_id',
               foreignField: 'emp_id',
               as: 'appliedJobs'
            }
         },
         { $unwind: '$appliedJobs' },
         {
            $group: {
               _id: '$emp_id',
               appliedCount: { $sum: 1 }
            }
         },
         {
            $project: {
               _id: 1,
               appliedCount: '$appliedCount',
            }
         }
      ])
      const jobsId = await Seeker.aggregate([
         {
            $lookup: {
               from: 'col__jobapplied',
               localField: 'emp_id',
               foreignField: 'emp_id',
               as: 'appliedJobs'
            }
         },
         { $unwind: '$appliedJobs' },
         {
            $project: {
               _id: 0,
               emp_id: '$appliedJobs.emp_id',
               unrst_jid: '$appliedJobs.job_id',
            }
         }
      ])
      const jobsIdMaped = jobsId.map(id => id = id.unrst_jid)
      let compId = await Unrestjobpost.aggregate([
         {
            $match: { unrst_jid: { $in: jobsIdMaped } }
         },
         {
            $project: {
               _id: 0,
               unrst_jid: 1,
               posted_id: 1
            }
         }
      ])
      let compIdMaped = compId.map(comp_id => comp_id = comp_id.posted_id)
      let company = await Employer.aggregate([
         {
            $match: {
               recut_id: { $in: compIdMaped }
            }
         },
         {
            $project: {
               recut_id: 1, comp_name: 1,
               mail_id: 1, mobile_no: 1, comp_logo: 1,
               cont_person: 1, indust_id: 1, country_id: 1, state_id: 1, city_id: 1,
               pincode: 1, recut_desc: 1, recut_status: 1, recut_date: 1
            }
         }
      ])
      const mapedComp = []
      jobsId.map(id => {
         const founded = compId.find(comp => comp.unrst_jid == id.unrst_jid)
         if (founded) {
            id.posted_id = founded.posted_id
         } else {
            id.posted_id = null
         }
         mapedComp.push(id)
      })
      const compDetailMaped = []
      mapedComp.map(id => {
         const founded = company.find(c => c.recut_id == id.posted_id)
         if (founded) {
            id.comp_details = founded
         } else {
            id.comp_details = null
         }
         compDetailMaped.push(id)
      })
      let mapedComp2 = mapedComp.reduce((acc, obj) => {
         const key = obj['emp_id']
         if (!acc[key]) {
            acc[key] = []
         }
         acc[key].push(obj.comp_details)
         return acc
      }, {})

      const maped = []
      rows.map(async (i) => {
         const matched = appliedCount.find(x => x._id === i.emp_id)
         const company = Object.keys(mapedComp2).includes(String(i.emp_id))
         if (matched && company) {
            i.appliedCount = matched.appliedCount
            i.applied_comp = mapedComp2[String(i.emp_id)]
         } else {
            i.appliedCount = 0
            i.applied_comp = null
         }
         maped.push(i)
      })
      const count = rows.length
      return { totalCount, count, rows }
   } catch (err) {
      return err
   }
}

SeekerService.cityNotMatched = async () => {
   try {
      const data = await City.aggregate([{ $match: { city_status: 'Y' } }, { $project: { _id: 0, city_id: 1 } }]).then(c => c.map(id => id = id.city_id))
      return { data, count: data.length }
   } catch (err) {
      return err
   }
}

SeekerService.findByPk = async (emp_id) => {
   try {
      const findByPk = await Seeker.findOne({ emp_id: emp_id })
      return findByPk
   } catch (err) {
      return err
   }
}

SeekerService.skillsUpdateEmployee = async (skills, employeeId) => {
   try {
      await EmpKeySkills.deleteMany({ emp_id: employeeId })
      skills = skills.map(s => {
         delete s.empkskil_id
         if (s._id) delete s._id
         s['emp_id'] = employeeId
         s['empkskil_status'] = 'Y'
         s['empkskil_date'] = new Date()
         s['lastupdate'] = new Date()
         return s
      })
      if (skills && skills.length) {
         await EmpKeySkills.insertMany(skills)
      };
      return
   }
   catch (err) {
      return err
   }
}

SeekerService.employmentUpdateEmployee = async (employment, employeeId) => {
   try {
      await EmpOffDetails.deleteMany({ emp_id: employeeId })
      employment = employment.map(s => {
         delete s.wrk_id
         if (s._id) delete s._id
         s['emp_id'] = employeeId
         s['wrk_status'] = 'Y'
         s['wrk_date'] = new Date()
         s['lastupdate'] = new Date()
         return s
      })
      if (employment && employment.length) {
         await EmpOffDetails.insertMany(employment)
      };
      return
   }
   catch (err) {
      return err
   }
}

SeekerService.locationUpdateEmployee = async (locations, employeeId) => {
   try {
      await EmpLoct.deleteMany({ emp_id: employeeId })
      locations = locations.map(s => {
         delete s.emplocat_id
         if (s._id) delete s._id
         s['emp_id'] = employeeId
         s['locat_status'] = 'Y'
         s['locat_date'] = new Date()
         s['lastupdate'] = new Date()
         return s
      })
      if (locations && locations.length) {
         await EmpLoct.insertMany(locations)
      };
      return
   }
   catch (err) {
      return err
   }
}

SeekerService.educationUpdateEmployee = async (educations, employeeId) => {
   try {
      await EmpEduDetail.deleteMany({ emp_id: employeeId })
      educations = educations.map(s => {
         delete s.edu_id
         if (s._id) delete s._id
         s['emp_id'] = employeeId
         s['edudate'] = new Date()
         return s
      })
      if (educations && educations.length) {
         await EmpEduDetail.insertMany(educations)
      };
      return
   }
   catch (err) {
      return err
   }
}

SeekerService.update = async (emp_id, obj) => {
   try {
      const updateById = await Seeker.updateOne({ emp_id: emp_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

SeekerService.bulkUpdateStatus = async (emp_ids, status) => {
   try {
      const updated = await Seeker.updateMany({ emp_id: { $in: emp_ids } }, { emp_status: status },)
      return updated
   } catch (err) {
      return err
   }
}

SeekerService.bulkUpdateLastupdate = async (emp_ids) => {
   try {
      const updated = await Seeker.updateMany({ emp_id: { $in: emp_ids } }, { lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

SeekerService.delete = async (emp_id) => {
   try {
      const deleted = await Seeker.deleteOne({ emp_id: emp_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = SeekerService