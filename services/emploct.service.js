'use strict';
const EmployeeLocation = require('../models/emploct.model');

class EmployeeLocationService { }
EmployeeLocationService.create = async (payload) => {
   try {
      let saved = new EmployeeLocation(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

EmployeeLocationService.findOne = async (findwhere) => {
   try {
      const founded = await EmployeeLocation.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

EmployeeLocationService.findAllAndCount = async (_start, _limit, totalWhere) => {
   try {
      const totalCount = await EmployeeLocation.count(totalWhere);
      const rows = await EmployeeLocation.find(totalWhere).skip(_start).limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.find = async () => {
   try {
      const rows = await EmployeeLocation.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.findByPk = async (emplocat_id) => {
   try {
      const findByPk = await EmployeeLocation.findOne({ emplocat_id: emplocat_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.update = async (emplocat_id, obj) => {
   try {
      const updateById = await EmployeeLocation.updateOne({ emplocat_id: emplocat_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.bulkUpdateStatus = async (emplocat_ids, status) => {
   try {
      const updated = await EmployeeLocation.updateMany({ emplocat_id: { $in: emplocat_ids } }, { locat_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.bulkUpdateLastupdate = async (emplocat_ids) => {
   try {
      const updated = await EmployeeLocation.updateMany({ emplocat_id: { $in: emplocat_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

EmployeeLocationService.delete = async (emplocat_id) => {
   try {
      const deleted = await EmployeeLocation.deleteOne({ emplocat_id: emplocat_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = EmployeeLocationService;
