'use strict'
const Festivals = require('../models/festivals.model')

class FestivalsService { }
FestivalsService.create = async (payload) => {
   try {
      let saved = new Festivals(payload)
      return await saved.save()
   } catch (error) {
      return error
   }
}

FestivalsService.findOne = async (findwhere) => {
   try {
      const founded = await Festivals.findOne(findwhere)
      return founded
   } catch (error) {
      return error
   }
}

FestivalsService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Festivals.count(totalWhere)
      const rows = await Festivals.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
         .sort({ fest_pos: 1 })
      const count = rows.length
      return { totalCount, count, rows }
   } catch (err) {
      return err
   }
}

FestivalsService.find = async () => {
   try {
      const rows = await Festivals.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

FestivalsService.findByPk = async (fest_id) => {
   try {
      const findByPk = await Festivals.findOne({ fest_id: fest_id })
      return findByPk
   } catch (err) {
      return err
   }
}

FestivalsService.update = async (fest_id, obj) => {
   try {
      const updateById = await Festivals.updateOne({ fest_id: fest_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

FestivalsService.bulkUpdateStatus = async (fest_ids, status) => {
   try {
      const updated = await Festivals.updateMany({ fest_id: { $in: fest_ids } }, { fest_status: status })
      return updated
   } catch (err) {
      return err
   }
}

FestivalsService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {}
      positionArray.forEach(async val => {
         update = await Festivals.updateOne({ fest_id: val.fest_id }, { fest_pos: val.position })
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await FestivalsService.bulkUpdateLastupdate([Number(val.fest_id)])
         }
      })
      return update
   } catch (err) {
      return err
   }
}

FestivalsService.bulkUpdateLastupdate = async (fest_ids) => {
   try {
      const updated = await Festivals.updateMany({ fest_id: { $in: fest_ids } }, { lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

FestivalsService.delete = async (fest_id) => {
   try {
      const deleted = await Festivals.deleteOne({ fest_id: fest_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = FestivalsService