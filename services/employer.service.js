'use strict'
const Employer = require('../models/recutcomp.model')

class EmployerService { }
EmployerService.create = async (payload) => {
   try {
      let saved = new Employer(payload)
      await saved.save()
      return saved
   } catch (error) {
      return error
   }
}

EmployerService.find = async () => {
   try {
      const rows = await Employer.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

EmployerService.findOne = async (obj) => {
   try {
      const founded = await Employer.findOne(obj)
      return founded
   } catch (error) {
      return error
   }
}

EmployerService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      let totalCount = await Employer.count(totalWhere)
      const rows = await Employer.aggregate([
         {
            $lookup: {
               from: 'col__city',
               localField: 'city_id',
               foreignField: 'city_id',
               as: 'cityData'
            }
         },
         { $unwind: '$cityData' },
         {
            $lookup: {
               from: 'col__state',
               localField: 'state_id',
               foreignField: 'state_id',
               as: 'stateData'
            }
         },
         { $unwind: '$stateData' },
         {
            $lookup: {
               from: 'col__country',
               localField: 'country_id',
               foreignField: 'country_id',
               as: 'countryData'
            }
         },
         { $unwind: '$countryData' },
         {
            $match: {
               $and: [
                  where,
                  totalWhere
               ]
            }
         },
         { $sort: { recut_date: -1 } },
         {
            $project: {
               _id: 1, recut_id: 1, comp_name: 1, comp_slug: 1, mail_id: 1, mobile_no: 1, email_otp: 1,
               email_verify: 'Y', mobile_otp: 1, mobile_verify: 1, comp_logo: 1, comp_pass: 1, cont_person: 1,
               indust_id: 1, recut_type: 1, country: '$countryData.country_name', state: '$stateData.state_name',
               location: '$cityData.city_name', pincode: 1, emp_strength: 1, recut_desc: 1, facebook: 1, twitter: 1,
               linkedin: 1, google: 1, recut_status: 1, user_type: 1, recut_address: 1, recut_date: 1, ipaddress: 1,
               lastupdate: 1
            }
         }
      ]).skip(_start).limit(_limit)
      const listcounts = await Employer.aggregate([
         {
            $lookup: {
               from: 'col__jobapplied',
               localField: 'recut_id',
               foreignField: 'company_id',
               as: 'listcounts'
            }
         },
         { $unwind: '$listcounts' },
         {
            $group: {
               _id: '$recut_id',
               appliedCount: { $sum: 1 }
            }
         },
         {
            $project: {
               _id: 1,
               appliedCount: '$appliedCount',
            }
         }
      ])
      let empDetails = await Employer.aggregate([
         {
            $lookup: {
               from: 'col__jobapplied',
               localField: 'recut_id',
               foreignField: 'company_id',
               as: 'matched'
            }
         },
         { $unwind: '$matched' },
         {
            $lookup: {
               from: 'col__employee',
               localField: 'matched.emp_id',
               foreignField: 'emp_id',
               as: 'emp'
            }
         },
         { $unwind: '$emp' },
         {
            $project: {
               _id: 0,
               recut_id: 1,
               employee: '$emp'
            }
         }
      ])
      let mapedData = empDetails.reduce((acc, obj) => {
         const key = obj['recut_id']
         if (!acc[key]) {
            acc[key] = []
         }
         acc[key].push(obj.employee)
         return acc
      }, {})
      const maped = []
      rows.map(async (i) => {
         const mastCat = listcounts.find(x => x._id === i.recut_id)
         const emp = Object.keys(mapedData).includes(String(i.recut_id))
         if (mastCat && emp) {
            i.appliedCount = mastCat.appliedCount
            i.applied_emp = mapedData[String(i.recut_id)]
         } else {
            i.appliedCount = 0
            i.applied_emp = null
         }
         maped.push(i)
      })
      return { totalCount, count: maped.length, rows: maped }
   } catch (err) {
      return err
   }
}

EmployerService.findByPk = async (recut_id) => {
   try {
      const findByPk = await Employer.findOne({ recut_id: recut_id })
      return findByPk
   } catch (err) {
      return err
   }
}

EmployerService.update = async (recut_id, obj) => {
   try {
      const updateById = await Employer.updateOne({ recut_id: recut_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

EmployerService.bulkUpdateStatus = async (recut_ids, status) => {
   try {
      const updated = await Employer.updateMany({ recut_id: { $in: recut_ids } }, { recut_status: status })
      return updated
   } catch (err) {
      return err
   }
}

EmployerService.bulkUpdateLastupdate = async (recut_ids) => {
   try {
      const updated = await Employer.updateMany({ recut_id: { $in: recut_ids } }, { lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

EmployerService.delete = async (recut_id) => {
   try {
      const deleted = await Employer.deleteOne({ recut_id: recut_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = EmployerService
