'use strict';
const JobApplied = require('../models/jobapplied.model');

class JobAppliedService { }
JobAppliedService.create = async (payload) => {
   try {
      let saved = new JobApplied(payload);
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

JobAppliedService.findOne = async (findwhere) => {
   try {
      const founded = await JobApplied.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

JobAppliedService.findAllAndCount = async (_start, _limit, totalWhere) => {
   try {
      const totalCount = await JobApplied.count(totalWhere);
      const rows = await JobApplied.find(totalWhere).skip(_start).limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

JobAppliedService.find = async () => {
   try {
      const rows = await JobApplied.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

JobAppliedService.findByPk = async (applied_id) => {
   try {
      const findByPk = await JobApplied.findOne({ applied_id: applied_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

JobAppliedService.update = async (applied_id, obj) => {
   try {
      const updateById = await JobApplied.updateOne({ applied_id: applied_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

JobAppliedService.bulkUpdateStatus = async (applied_ids, status) => {
   try {
      const updated = await JobApplied.updateMany({ applied_id: { $in: applied_ids } }, { appl_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

JobAppliedService.bulkUpdateLastupdate = async (applied_ids) => {
   try {
      const updated = await JobApplied.updateMany({ applied_id: { $in: applied_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

JobAppliedService.delete = async (applied_id) => {
   try {
      const deleted = await JobApplied.deleteOne({ applied_id: applied_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = JobAppliedService;