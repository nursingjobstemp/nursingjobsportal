'use strict';
const Emp_Job_Category = require('../models/empjobcat.model');

class Emp_Job_CategoryService { }
Emp_Job_CategoryService.create = async (payload) => {
   try {
      let saved = new Emp_Job_Category(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

Emp_Job_CategoryService.findOne = async (obj) => {
   try {
      const founded = await Emp_Job_Category.findOne(obj);
      return founded;
   } catch (error) {
      return error;
   }
};

Emp_Job_CategoryService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Emp_Job_Category.count(totalWhere);
      const rows = await Emp_Job_Category.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit);
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.find = async () => {
   try {
      const rows = await Emp_Job_Category.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.findByPk = async (mjcat_id) => {
   try {
      const findByPk = await Emp_Job_Category.findOne({ mjcat_id: mjcat_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.update = async (mjcat_id, obj) => {
   try {
      const updateById = await Emp_Job_Category.updateOne({ mjcat_id: mjcat_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.bulkUpdateStatus = async (mjcat_ids, status) => {
   try {
      const updated = await Emp_Job_Category.updateMany({ mjcat_id: { $in: mjcat_ids } }, { mjcat_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.bulkUpdateLastupdate = async (mjcat_ids) => {
   try {
      const updated = await Emp_Job_Category.updateMany({ mjcat_id: { $in: mjcat_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

Emp_Job_CategoryService.delete = async (mjcat_id) => {
   try {
      const deleted = await Emp_Job_Category.deleteOne({ mjcat_id: mjcat_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = Emp_Job_CategoryService;
