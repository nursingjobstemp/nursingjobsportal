
const adminMenuService = require('./adminMenu.service')
const chatService = require('./chat.service')
const cityService = require('./city.service')
const collegeService = require('./college.service')
const contactresumeService = require('./contactresume.service')
const edu_courseService = require('./edu_course.service')
const empedudetailService = require('./empedudetail.service')
const empjobcatService = require('./empjobcat.service')
const empkskillsService = require('./empkskills.services')
const emploctService = require('./emploct.service')
const employerService = require("./employer.service")
const empoffdetailsService = require('./empoffdetails.service')
const enquiryService = require('./enquiry.service')
const industrytypeService = require('./industrytype.service')
const intjobpostService = require('./intjobpost.service')
const intscheduleService = require('./intschedule.service')
const jobappliedService = require('./jobapplied.service')
const jobscategoryService = require('./jobscategory.service')
const jobtypeService = require('./jobtype.service')
const keyskillsService = require('./keyskills.service')
const notificationService = require('./notification.service')
const operatorService = require('./operator.service')
const resumescoreService = require('./resumescore.service')
const salaryService = require('./salary.service')
const seekerService = require("./seeker.service")
const push_notifyService = require('./push_notify.service')
const unrestjobpostService = require('./unrestjobpost.service')
const festivalsService = require('./festivals.service')

module.exports = {
   adminMenuService,
   chatService,
   cityService,
   collegeService,
   contactresumeService,
   edu_courseService,
   empedudetailService,
   empjobcatService,
   empkskillsService,
   emploctService,
   empoffdetailsService,
   enquiryService,
   industrytypeService,
   intjobpostService,
   intscheduleService,
   jobappliedService,
   jobscategoryService,
   jobtypeService,
   keyskillsService,
   notificationService,
   operatorService,
   resumescoreService,
   salaryService,
   seekerService,
   push_notifyService,
   unrestjobpostService,
   employerService,
   festivalsService
}
