'use strict';
const Unrestjobpost = require('../models/unrestjobpost.model');

class UnrestjobpostService { }
UnrestjobpostService.create = async (payload) => {
   try {
      let saved = new Unrestjobpost(payload);
      await saved.save();
      return saved;
   } catch (error) {
      return error;
   }
};

UnrestjobpostService.findOne = async (findwhere) => {
   try {
      const founded = await Unrestjobpost.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

UnrestjobpostService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Unrestjobpost.count(totalWhere);
      const rows = await Unrestjobpost.aggregate([
         {
            $match: {
               $and: [
                  where,
                  totalWhere
               ]
            }
         },
         { $sort: { posted_date: -1 } },
         {
            $lookup: {
               from: 'col__jobscategory',
               localField: 'unrest_jcat',
               foreignField: 'jcat_id',
               as: 'jobsCat'
            }
         },
         { $unwind: { path: '$jobsCat', preserveNullAndEmptyArrays: false } },
         {
            $lookup: {
               from: 'col__jobscategory',
               localField: 'unrest_jsubcat',
               foreignField: 'jcat_id',
               as: 'subCat'
            }
         },
         { $unwind: { path: '$subCat', preserveNullAndEmptyArrays: false } },
         {
            $lookup: {
               from: 'col__city',
               localField: 'unrest_jloct',
               foreignField: 'city_id',
               as: 'cityData'
            }
         },
         { $unwind: { path: '$cityData', preserveNullAndEmptyArrays: false } },
         {
            $project: {
               _id: 0, unrest_jcode: 1,
               posted_date: 1, posted_pos: 1, unrst_jid: 1, duplicate_from: 1, jcat_type: 1, apply: 1,
               unrest_jcat: 1, unrest_jsubcat: 1, unrest_jname: 1, unrest_jdesc: 1, unrest_jeducat: 1,
               unrest_jquali: 1, quali_type: 1, qualif_txt: 1, age_limit: 1, job_detail: 1, web_url: 1,
               unrest_jrequ: 1, unrest_jallow: 1, job_type: 1, unrest_jemail: 1, no_of_openings: 1,
               key_skills: 1, job_exp: 1, country_id: 1, state: 1, unrest_jloct: 1, unrest_jcompany: 1,
               unrest_jphone: 1, unrest_sal: 1, sec_title: 1, all_india: 1, statename: 1, cityname: 1,
               sec_jobtitle: 1, ip_address: 1, posted_id: 1, posted_by: 1, posted_name: 1, exp_date: 1,
               posted_status: 1, posted_lastupdate: 1, jobsCategory: '$jobsCat.jcat_name', verify: 1, high_qualif: 1,
               SubCategory: '$subCat.jcat_name', location: '$cityData.city_name', high_course: 1, high_special: 1,
               sal_id: 1, jtype_id: 1, jtype_id_new: 1, comp_detail: 1, unrest_jphoneold: 1, unrest_landline: 1, comp_address: 1,
               comp_website: 1, field_exp: 1, nationality: 1, gender: 1,
            }
         }
      ], { allowDiskUse: true }).skip(_start)
         .limit(_limit);
      return { totalCount, count: rows.length, rows };
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.getDupJob = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Unrestjobpost.count(totalWhere);
      const rows = await Unrestjobpost.aggregate([
         {
            $match: {
               $and: [
                  totalWhere,
                  where
               ]
            }
         },
         { $sort: { posted_date: -1 } },
         {
            $lookup: {
               from: 'col__jobscategory',
               localField: 'unrest_jcat',
               foreignField: 'jcat_id',
               as: 'jobsCat'
            }
         },
         { $unwind: { path: '$jobsCat', preserveNullAndEmptyArrays: false } },
         {
            $lookup: {
               from: 'col__jobscategory',
               localField: 'unrest_jsubcat',
               foreignField: 'jcat_id',
               as: 'subCat'
            }
         },
         { $unwind: { path: '$subCat', preserveNullAndEmptyArrays: false } },
         {
            $lookup: {
               from: 'col__city',
               localField: 'unrest_jloct',
               foreignField: 'city_id',
               as: 'cityData'
            }
         },
         { $unwind: { path: '$cityData', preserveNullAndEmptyArrays: false } },
         {
            $project: {
               _id: 0, unrest_jcode: 1,
               posted_date: 1, posted_pos: 1, unrst_jid: 1, duplicate_from: 1, jcat_type: 1, apply: 1,
               unrest_jcat: 1, unrest_jsubcat: 1, unrest_jname: 1, unrest_jdesc: 1, unrest_jeducat: 1,
               unrest_jquali: 1, quali_type: 1, qualif_txt: 1, age_limit: 1, job_detail: 1, web_url: 1,
               unrest_jrequ: 1, unrest_jallow: 1, job_type: 1, unrest_jemail: 1, no_of_openings: 1,
               key_skills: 1, job_exp: 1, country_id: 1, state: 1, unrest_jloct: 1, unrest_jcompany: 1,
               unrest_jphone: 1, unrest_sal: 1, sec_title: 1, all_india: 1, statename: 1, cityname: 1,
               sec_jobtitle: 1, ip_address: 1, posted_id: 1, posted_by: 1, posted_name: 1, exp_date: 1,
               posted_status: 1, posted_lastupdate: 1, jobsCategory: '$jobsCat.jcat_name',
               SubCategory: '$subCat.jcat_name', location: '$cityData.city_name', nationality: 1, gender: 1,
               verify: 1, high_qualif: 1, high_course: 1, high_special: 1, sal_id: 1, jtype_id: 1, jtype_id_new: 1,
               comp_detail: 1, unrest_jphoneold: 1, unrest_landline: 1, comp_address: 1, comp_website: 1, field_exp: 1,
            }
         }
      ], { allowDiskUse: true }).skip(_start).limit(_limit);
      return { totalCount, count: rows.length, rows };
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.find = async () => {
   try {
      const rows = await Unrestjobpost.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.findByPk = async (unrst_jid) => {
   try {
      const findByPk = await Unrestjobpost.findOne({ unrst_jid: unrst_jid });
      return findByPk;
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.update = async (unrst_jid, obj) => {
   try {
      const updateById = await Unrestjobpost.updateOne({ unrst_jid: unrst_jid }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.bulkUpdateStatus = async (unrst_jids, status) => {
   try {
      const updated = await Unrestjobpost.updateMany({ unrst_jid: { $in: unrst_jids } }, { posted_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {};
      positionArray.forEach(async val => {
         update = await Unrestjobpost.updateOne({ unrst_jid: val.unrst_jid }, { posted_pos: val.position });
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await UnrestjobpostService.bulkUpdateLastupdate([Number(val.unrst_jid)]);
         }
      });
      return update;
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.bulkUpdateLastupdate = async (unrst_jids) => {
   try {
      const updated = await Unrestjobpost.updateMany({ unrst_jid: { $in: unrst_jids } }, { posted_lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

UnrestjobpostService.delete = async (unrst_jid) => {
   try {
      const deleted = await Unrestjobpost.deleteOne({ unrst_jid: unrst_jid });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = UnrestjobpostService;