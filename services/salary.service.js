'use strict'
const Salary = require('../models/salary.model')

class SalaryService { }
SalaryService.create = async (payload) => {
   try {
      let saved = new Salary(payload)
      return await saved.save()
   } catch (error) {
      return error
   }
}

SalaryService.findOne = async (findwhere) => {
   try {
      const founded = await Salary.findOne(findwhere)
      return founded
   } catch (error) {
      return error
   }
}

SalaryService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await Salary.count(totalWhere)
      const rows = await Salary.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
         .sort({ sal_pos: 1 })
      const count = rows.length
      return { totalCount, count, rows }
   } catch (err) {
      return err
   }
}

SalaryService.find = async () => {
   try {
      const rows = await Salary.find({}).sort({ $natural: -1 }).limit(1)
      return { rows }
   } catch (err) {
      return err
   }
}

SalaryService.findByPk = async (sal_id) => {
   try {
      const findByPk = await Salary.findOne({ sal_id: sal_id })
      return findByPk
   } catch (err) {
      return err
   }
}

SalaryService.update = async (sal_id, obj) => {
   try {
      const updateById = await Salary.updateOne({ sal_id: sal_id }, obj)
      return updateById
   } catch (err) {
      return err
   }
}

SalaryService.bulkUpdateStatus = async (sal_ids, status) => {
   try {
      const updated = await Salary.updateMany({ sal_id: { $in: sal_ids } }, { sal_status: status })
      return updated
   } catch (err) {
      return err
   }
}

SalaryService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {}
      positionArray.forEach(async val => {
         update = await Salary.updateOne({ sal_id: val.sal_id }, { sal_pos: val.position })
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await SalaryService.bulkUpdateLastupdate([Number(val.sal_id)])
         }
      })
      return update
   } catch (err) {
      return err
   }
}

SalaryService.bulkUpdateLastupdate = async (sal_ids) => {
   try {
      const updated = await Salary.updateMany({ sal_id: { $in: sal_ids } }, { lastupdate: new Date() })
      return updated
   } catch (err) {
      return err
   }
}

SalaryService.delete = async (sal_id) => {
   try {
      const deleted = await Salary.deleteOne({ sal_id: sal_id })
      return deleted
   } catch (err) {
      return err
   }
}

module.exports = SalaryService