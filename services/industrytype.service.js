'use strict';
const IndustryType = require('../models/industrytype.model');

class IndustryTypeService { }
IndustryTypeService.create = async (payload) => {
   try {
      let saved = new IndustryType(payload)
      await saved.save()
      return saved;
   } catch (error) {
      return error;
   }
};

IndustryTypeService.findOne = async (findwhere) => {
   try {
      const founded = await IndustryType.findOne(findwhere);
      return founded;
   } catch (error) {
      return error;
   }
};

IndustryTypeService.findAllAndCount = async (where, _start, _limit, totalWhere) => {
   try {
      const totalCount = await IndustryType.count(totalWhere);
      const rows = await IndustryType.find({
         $and: [
            where,
            totalWhere
         ]
      }).skip(_start)
         .limit(_limit)
         .sort({ indust_pos: 1 });
      const count = rows.length;
      return { totalCount, count, rows };
   } catch (err) {
      return err;
   }
};

IndustryTypeService.find = async () => {
   try {
      const rows = await IndustryType.find({}).sort({ $natural: -1 }).limit(1);
      return { rows };
   } catch (err) {
      return err;
   }
};

IndustryTypeService.findByPk = async (indust_id) => {
   try {
      const findByPk = await IndustryType.findOne({ indust_id: indust_id });
      return findByPk;
   } catch (err) {
      return err;
   }
};

IndustryTypeService.update = async (indust_id, obj) => {
   try {
      const updateById = await IndustryType.updateOne({ indust_id: indust_id }, obj);
      return updateById;
   } catch (err) {
      return err;
   }
};

IndustryTypeService.bulkUpdateStatus = async (indust_ids, status) => {
   try {
      const updated = await IndustryType.updateMany({ indust_id: { $in: indust_ids } }, { indust_status: status });
      return updated;
   } catch (err) {
      return err;
   }
};

IndustryTypeService.bulkUpdatePosition = async (positionArray) => {
   try {
      let update = {};
      positionArray.forEach(async val => {
         update = await IndustryType.updateOne({ indust_id: val.indust_id }, { indust_pos: val.position });
         if (update && update.acknowledged == true && update.matchedCount == 1 && update.modifiedCount == 1) {
            await IndustryTypeService.bulkUpdateLastupdate([Number(val.indust_id)]);
         }
      });
      return update;
   } catch (err) {
      return err;
   }
};

IndustryTypeService.bulkUpdateLastupdate = async (indust_ids) => {
   try {
      const updated = await IndustryType.updateMany({ indust_id: { $in: indust_ids } }, { lastupdate: new Date() });
      return updated;
   } catch (err) {
      return err;
   }
};

IndustryTypeService.delete = async (indust_id) => {
   try {
      const deleted = await IndustryType.deleteOne({ indust_id: indust_id });
      return deleted;
   } catch (err) {
      return err;
   }
};

module.exports = IndustryTypeService;