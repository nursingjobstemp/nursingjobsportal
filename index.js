const express = require("express")
const helmet = require("helmet")
const cors = require("cors")
const http = require("http")
const bodyParser = require("body-parser")
const path = require("path")
require("dotenv").config()
const mongoose = require('mongoose')

const routers = require("./routes")
const dbConfig = require("./config/db.config")()
require('./config')

// Express Application
const app = express()
app.use(helmet())
app.use(cors())
app.use(bodyParser.json({ limit: "3000mb" }))
app.use(bodyParser.urlencoded({ limit: "3000mb", extended: true }))
app.use("/public", express.static(path.join(__dirname, "public")))
routers(app)

// Create Server
const server = http.createServer(app)
mongoose.set('strictQuery', false)
const Database = require("./config/database")
new Database(
	dbConfig.mongo.port,
	dbConfig.mongo.host,
	dbConfig.mongo.dbname,
	dbConfig.mongo.username,
	dbConfig.mongo.password
)

server.listen(process.env.APP_PORT, () => {
	console.log("\nServer Port : ", process.env.APP_PORT)
})
