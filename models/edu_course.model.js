const mongoose = require("mongoose")
const Schema = mongoose.Schema

const Counter = require('./counter.modal')

const edu_course_schema = new Schema(
    {
        ecat_id: {
            type: Number,
            unique: true
        },
        pid: {
            type: Number,
            required: true,
        },
        ecatid_sub: {
            type: Number,
            required: true,
        },
        ecat_type: {
            type: String,
            required: true
        },
        ecat_name: {
            type: String,
            required: true
        },
        ecat_slug: {
            type: String,
            required: true
        },
        ecat_pos: {
            type: Number,
            required: true
        },
        ecat_status: {
            type: String,
            required: true
        },
        ecat_dt: {
            type: Date,
            required: true
        },
        gcat_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__edu_course" }
)

edu_course_schema.pre("save", async function (next) {
    var obj = this
    if (obj.ecat_id) {
        next()
    }
    let counter = await Counter.findOne({ name: 'edu_course' })
    if (!counter) {
        let newCounter = new Counter({ name: 'edu_course', seq: 15272 })
        obj.ecat_id = 15271
        await newCounter.save()
        next()
    }
    obj.ecat_id = counter.seq
    counter.seq = counter.seq + 1
    await counter.save()
    next()
})
module.exports = mongoose.model("Education_Course", edu_course_schema)
