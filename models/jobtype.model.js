
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const jobtype_schema = new Schema(
    {
        jtype_id: {
            type: Number,
            unique: true
        },
        jtype_name: {
            type: String,
            required: true
        },
        jtype_status: {
            type: String,
            required: true
        },
        jtype_pos: {
            type: Number,
            required: true
        },
        jtype_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__jobtype" }
);

jobtype_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.jtype_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'jobtype' });
    if (!counter) {
        let newCounter = new Counter({ name: 'jobtype', seq: 2 });
        obj.jtype_id = 1;
        await newCounter.save();
        next();
    }
    obj.jtype_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});

module.exports = mongoose.model("JobType", jobtype_schema);



