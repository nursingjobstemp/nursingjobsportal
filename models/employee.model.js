const mongoose = require("mongoose")
const Schema = mongoose.Schema

const Counter = require('./counter.modal')

const employee_schema = new Schema(
    {
        emp_id: {
            type: Number,
            unique: true
        },
        emp_name: {
            type: String,
            default: null,
            required: false
        },
        emp_email: {
            type: String,
            default: null,
            required: false
        },
        emp_mobile: {
            type: Number,
            default: null,
            required: false
        },
        emp_pass: {
            type: String,
            default: null,
            required: false
        },
        email_otp: {
            type: String,
            default: null,
            required: false
        },
        email_verify: {
            type: String,
            default: null,
            required: false
        },
        mobile_otp: {
            type: String,
            default: null,
            required: false
        },
        mobile_verify: {
            type: String,
            default: null,
            required: false
        },
        emp_dob: {
            type: String,
            default: null,
            required: false
        },
        emp_gender: {
            type: String,
            default: null,
            required: false
        },
        emp_cat: {
            type: Number,
            default: null,
            required: false
        },
        emp_subcat: {
            type: Number,
            default: null,
            required: false
        },
        cat_name: {
            type: String,
            default: null,
            required: false
        },
        subcat_name: {
            type: String,
            default: null,
            required: false
        },
        emp_desig: {
            type: String,
            default: null,
            required: false
        },
        emp_typeval: {
            type: String,
            default: null,
            required: false
        },
        emp_comp: {
            type: String,
            default: null,
            required: false
        },
        min_sal: {
            type: String,
            default: null,
            required: false
        },
        sal_lakh: {
            type: String,
            default: null,
            required: false
        },
        sal_thousands: {
            type: String,
            default: null,
            required: false
        },
        exp_year: {
            type: String,
            default: null,
            required: false
        },
        exp_month: {
            type: String,
            default: null,
            required: false
        },
        emp_photo: {
            type: String,
            default: null,
            required: false
        },
        emp_resumeheadline: {
            type: String,
            default: null,
            required: false
        },
        emp_country: {
            type: Number,
            default: null,
            required: false
        },
        emp_state: {
            type: Number,
            default: null,
            required: false
        },
        emp_city: {
            type: Number,
            default: null,
            required: false
        },
        city_name: {
            type: String,
            default: null,
            required: false
        },
        high_qualif: {
            type: Number,
            default: null,
            required: false
        },
        high_course: {
            type: Number,
            default: null,
            required: false
        },
        high_special: {
            type: Number,
            default: null,
            required: false
        },
        high_college: {
            type: Number,
            default: null,
            required: false
        },
        colg_name: {
            type: String,
            default: null,
            required: false
        },
        course_type: {
            type: String,
            default: null,
            required: false
        },
        exp_type: {
            type: String,
            default: null,
            required: false
        },
        high_pass_yr: {
            type: String,
            default: null,
            required: false
        },
        emp_resume: {
            type: String,
            default: null,
            required: false
        },
        emp_pincode: {
            type: String,
            default: null,
            required: false
        },
        emp_marital: {
            type: String,
            default: null,
            required: false
        },
        emp_address: {
            type: String,
            default: null,
            required: false
        },
        user_type: {
            type: String,
            default: null,
            required: false
        },
        ipaddress: {
            type: String,
            default: null,
            required: false
        },
        emp_status: {
            type: String,
            default: null,
            required: false
        },
        deviceToken: {
            type: String,
            default: null,
            required: false
        },
        emp_date: {
            type: Date,
            default: null,
            required: false
        },
        lastupdate: {
            type: Date,
            default: null,
            required: false
        }
    },
    { collection: "col__employee" }
)

employee_schema.pre("save", async function (next) {
    var obj = this
    if (obj.emp_id) {
        next()
    }
    let counter = await Counter.findOne({ name: 'employee' })
    if (!counter) {
        let newCounter = new Counter({ name: 'employee', seq: 15272 })
        obj.emp_id = 15271
        await newCounter.save()
        next()
    }
    obj.emp_id = counter.seq
    counter.seq = counter.seq + 1
    await counter.save()
    next()
})
module.exports = mongoose.model("Employee", employee_schema)