const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const empkeyskills_schema = new Schema(
    {
        empkskil_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        keysk_id: {
            type: Number,
            required: true
        },
        keysk_name: {
            type: String,
            required: true
        },
        empkskil_status: {
            type: String,
            required: true
        },
        empkskil_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__empkskills" }
);

empkeyskills_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.empkskil_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'empkskills' });
    if (!counter) {
        let newCounter = new Counter({ name: 'empkskills', seq:15272 });
        obj.empkskil_id = 15271;
        await newCounter.save();
        next();
    }
    obj.empkskil_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Emp_KeySkills", empkeyskills_schema);
