const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const keyskills_schema = new Schema(
    {
        keysk_id: {
            type: Number,
            unique: true
        },
        keysk_name: {
            type: String,
            required: true
        },
        keysk_slug: {
            type: String,
            required: true
        },
        keysk_code: {
            type: String,
            required: true
        },
        keysk_pos: {
            type: Number,
            required: true
        },
        keysk_status: {
            type: String,
            required: true
        },
        keysk_dt: {
            type: Date,
            required: true
        },
        keysk_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__keyskills" }
);

keyskills_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.keysk_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'keyskills' });
    if (!counter) {
        let newCounter = new Counter({ name: 'keyskills', seq: 2 });
        obj.keysk_id = 1;
        await newCounter.save();
        next();
    }
    obj.keysk_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("KeySkills", keyskills_schema);



