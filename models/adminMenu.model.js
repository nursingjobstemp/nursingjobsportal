const mongoose = require("mongoose")
const Schema = mongoose.Schema

const adminMenu_Schema = new Schema(
    {
        menu_id: {
            type: Number,
            unique: true,
            required: true
        },
        menu_title: {
            type: String,
            required: true
        },
        group_id:{
            type: Number,
            required: true
        },
        menu_type: {
            type: String,
            default: null
        },
        pid: {
            type: Number,
            required: true
        },
        menu_link: {
            type: String,
            default: null
        },
        menu_icon: {
            type: String,
            required: true
        },
        menu_home: {
            type: String,
            required: true
        },
        menu_pos: {
            type: String,
            required: true
        },
        menu_status: {
            type: String,
            required: true
        },
        menu_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__adminmenu" }
)

module.exports = mongoose.model("AdminMenu", adminMenu_Schema)
