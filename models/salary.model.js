const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const salary_schema = new Schema(
    {
        sal_id: {
            type: Number,
            unique: true
        },
        sal_name: {
            type: String,
            required: true
        },
        sal_slug: {
            type: String,
            required: true
        },
        min_sal: {
            type: String,
            required: true
        },
        max_sal: {
            type: String,
            required: true
        },
        sal_pos: {
            type: Number,
            required: true
        },
        sal_status: {
            type: String,
            required: true
        },
        sal_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__salary" }
);

salary_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.sal_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'salary' });
    if (!counter) {
        let newCounter = new Counter({ name: 'salary', seq:32 });
        obj.sal_id = 31;
        await newCounter.save();
        next();
    }
    obj.sal_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Salary", salary_schema);



