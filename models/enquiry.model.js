
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const enquiry_schema = new Schema(
    {
        enq_id: {
            type: Number,
            unqiue: true
        },
        enq_name: {
            type: String,
            required: true
        },
        enq_email: {
            type: String,
            required: true
        },
        enq_mobile: {
            type: Number,
            required: true
        },
        enq_msg: {
            type: String,
            required: true
        },
        enq_date: {
            type: String,
            required: true
        },
        enq_altmobile: {
            type: String,
            required: true
        },
        maincat: {
            type: String,
            required: true
        },
        type_home: {
            type: String,
            required: true
        },
        type_bhk: {
            type: String,
            required: true
        },
        enq_loc: {
            type: String,
            required: true
        },
        ipaddress: {
            type: String,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__enquiry" }
);
//enq_id
enquiry_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.enq_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'enquiry' });
    if (!counter) {
        let newCounter = new Counter({ name: 'enquiry', seq: 287414 });
        obj.enq_id = 287413;
        await newCounter.save();
        next();
    }
    obj.enq_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Enquiry", enquiry_schema);



