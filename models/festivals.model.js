
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');
const festivals_schema = new Schema(
    {
        fest_id: {
            type: Number,
            unique: true
        },
        fest_title: {
            type: String,
            required: true
        },
        fest_image: {
            type: String,
            default: null,
            required: false
        },
        fest_pos: {
            type: Number,
            default: null,
            required: false
        },
        fest_status: {
            type: String,
            required: true
        },
        view_type: {
            type: String,
            default: null,
            required: false
        },
        live_status: {
            type: String,
            default: null,
            required: false
        },
        fest_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__festivals" }
);

festivals_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.fest_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'festivals' });
    if (!counter) {
        let newCounter = new Counter({ name: 'festivals', seq: 2 });
        obj.fest_id = 1;
        await newCounter.save();
        next();
    }
    obj.fest_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("Festivals", festivals_schema);

