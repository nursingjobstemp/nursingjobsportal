const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const Intjobpost_schema = new Schema(
    {
        unrst_jid: {
            type: Number,
            unique: true
        },
        duplicate_from: {
            type: Number,
            default: null,
            required: false
        },
        jcat_type: {
            type: String,
            required: true
        },
        unrest_jcat: {
            type: Number,
            required: true
        },
        unrest_jsubcat: {
            type: Number,
            required: true
        },
        unrest_jcode: {
            type: String,
            required: true
        },
        verify: {
            type: String,
            require: true,
        },
        unrest_jname: {
            type: String,
            required: true
        },
        unrest_jdesc: {
            type: String,
            required: true
        },
        unrest_jquali: {
            type: String,
            required: true
        },
        unrest_jrequ: {
            type: String,
            default: null,
            required: false
        },
        high_qualif: {
            type: Number,
            required: true
        },
        high_course: {
            type: Number,
            default: null,
            required: false
        },
        high_special: {
            type: Number,
            required: true
        },
        unrest_jallow: {
            type: String,
            required: true
        },
        sal_id: {
            type: String,
            required: true
        },
        jtype_id: {
            type: String,
            required: true
        },
        verify: {
            type: String,
            required: true
        },
        jtype_id_new: {
            type: String,
            required: true
        },
        job_type: {
            type: String,
            required: true
        },
        key_skills: {
            type: Number,
            default: null,
            required: false
        },
        job_exp: {
            type: String,
            required: true
        },
        country_id: {
            type: Number,
            required: true
        },
        state: {
            type: Number,
            required: true
        },
        unrest_jloct: {
            type: Number,
            required: true
        },
        unrest_jcompany: {
            type: String,
            required: true
        },
        comp_detail: {
            type: String,
            default: null,
            required: false
        },
        unrest_jemail: {
            type: String,
            default: null,
            required: true
        },
        unrest_jphoneold: {
            type: Number,
            required: true
        },
        unrest_jphone: {
            type: String,
            default: null,
            required: false
        },
        unrest_landline: {
            type: String,
            default: null,
            required: false,
        },
        unrest_sal: {
            type: String,
            default: null,
            required: false
        },
        comp_address: {
            type: String,
            default: null,
            required: false
        },
        apply: {
            type: String,
            required: true
        },
        ip_address: {
            type: String,
            required: true
        },
        posted_id: {
            type: Number,
            required: true
        },
        posted_by: {
            type: String,
            required: true
        },
        posted_name: {
            type: String,
            required: true
        },
        posted_pos: {
            type: Number,
            required: true
        },
        exp_date: {
            type: Date,
            required: true
        },
        posted_status: {
            type: String,
            default: null,
            required: false
        },
        comp_website: {
            type: String,
            default: null,
            required: false
        },
        field_exp: {
            type: String,
            required: true,
        },
        nationality: {
            type: Number,
            default: null,
            required: false
        },
        no_of_openings: {
            type: String,
            default: null,
            required: false
        },
        gender: {
            type: String,
            default: null,
            required: false
        },
        posted_date: {
            type: Date,
            required: true
        },
        posted_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__intjobpost" }
);
Intjobpost_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.unrst_jid) {
        next();
    }
    let counter = await Counter.findOne({ name: 'job' });
    if (!counter) {
        let newCounter = new Counter({ name: 'job', seq: 287414 });
        obj.unrst_jid = 287413;
        await newCounter.save();
        next();
    }
    obj.unrst_jid = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("Intjobpost", Intjobpost_schema);