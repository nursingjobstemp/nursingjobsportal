const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const chat_schema = new Schema(
    {
        chat_id: {
            type: Number,
            unique: true
        },
        job_id: {
            type: Number,
            required: true
        },
        applyid: {
            type: Number,
            required: true
        },
        chat_from: {
            type: Number,
            required: true
        },
        chat_to: {
            type: Number,
            required: true
        },
        chat_fname: {
            type: String,
            required: true
        },
        chat_ftype: {
            type: String,
            required: true
        },
        chat_tname: {
            type: String,
            required: true
        },
        chat_ttype: {
            type: String,
            required: true
        },
        chat_msg: {
            type: String,
            required: true
        },
        chat_status: {
            type: String,
            required: true
        },
        chat_date: {
            type: Date,
            required: true
        },
        read_status: {
            type: String,
            required: true
        },
        read_date: {
            type: Date,
            required: true
        },
        ipaddr: {
            type: String,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__chat" }
);

chat_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.chat_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'chat' });
    if (!counter) {
        let newCounter = new Counter({ name: 'chat', seq:15272 });
        obj.chat_id = 15271;
        await newCounter.save();
        next();
    }
    obj.chat_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Chat", chat_schema);
