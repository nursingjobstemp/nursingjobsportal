const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const empjobcat_schema = new Schema(
    {
        mjcat_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        cat_id: {
            type: Number,
            required: true
        },
        subcat_id: {
            type: Number,
            required: true
        },
        mjcat_status: {
            type: String,
            required: true
        },
        mjcat_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__empjobcat" }
);

empjobcat_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.mjcat_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'empjobcat' });
    if (!counter) {
        let newCounter = new Counter({ name: 'empjobcat', seq:15272 });
        obj.mjcat_id = 15271;
        await newCounter.save();
        next();
    }
    obj.mjcat_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Emp_Job_Category", empjobcat_schema);
