const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const emploct_schema = new Schema(
    {
        emplocat_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        emp_country: {
            type: Number,
            required: true
        },
        emp_state: {
            type: Number,
            required: true
        },
        emp_city: {
            type: Number,
            required: true
        },
        locat_status: {
            type: String,
            required: true
        },
        ipaddr: {
            type: String,
            required: true
        },
        locat_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__emploct" }
);

emploct_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.emplocat_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'emploct' });
    if (!counter) {
        let newCounter = new Counter({ name: 'emploct', seq:15272 });
        obj.emplocat_id = 15271;
        await newCounter.save();
        next();
    }
    obj.emplocat_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("EmployeeLocation", emploct_schema);



