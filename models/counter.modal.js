const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var counterSchema = new Schema(
    {
        name: { type: String, required: true },
        seq: { type: Number, required: true },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Counter", counterSchema);