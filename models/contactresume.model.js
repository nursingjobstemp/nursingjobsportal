const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const contactresume_schema = new Schema(
    {
        cont_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        comp_id: {
            type: Number,
            required: true
        },
        cont_type: {
            type: String,
            required: true
        },
        cont_date: {
            type: Date,
            required: true
        },
        ipaddress: {
            type: String,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__contactresume" }
);

contactresume_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.cont_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'contactresume' });
    if (!counter) {
        let newCounter = new Counter({ name: 'contactresume', seq:15272 });
        obj.cont_id = 15271;
        await newCounter.save();
        next();
    }
    obj.cont_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Contact_Resume", contactresume_schema);
