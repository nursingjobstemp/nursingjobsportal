'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const dbConfig = require("../config/db.config");
const db = {};

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

module.exports = db;
