const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const empedudetail_schema = new Schema(
    {
        edu_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        high_qualif: {
            type: Number,
            required: true
        },
        high_course: {
            type: Number,
            required: true
        },
        high_special: {
            type: Number,
            required: true
        },
        high_college: {
            type: Number,
            required: true
        },
        colg_name: {
            type: String,
            required: true
        },
        high_pass_yr: {
            type: Number,
            required: true
        },
        edudate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__empedudetail" }
);
//
empedudetail_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.edu_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'empedudetail' });
    if (!counter) {
        let newCounter = new Counter({ name: 'empedudetail', seq:15272 });
        obj.edu_id = 15271;
        await newCounter.save();
        next();
    }
    obj.edu_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Employee_Education_Detail", empedudetail_schema);
