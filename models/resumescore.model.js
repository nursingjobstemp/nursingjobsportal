const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const resumescore_schema = new Schema(
    {
        resume_id: {
            type: Number,
            unique: true
        },
        resume_title: {
            type: String,
            required: true
        },
        resume_fdesc: {
            type: String,
            default: null,
            required: false
        },
        resume_sdesc: {
            type: String,
            default: null,
            required: false
        },
        first_image: {
            type: String,
            default: null,
            required: false
        },
        seo_title: {
            type: String,
            default: null,
            required: false
        },
        seo_description: {
            type: String,
            default: null,
            required: false
        },
        seo_keywords: {
            type: String,
            default: null,
            required: false
        },
        second_image: {
            type: String,
            default: null,
            required: false
        },
        resume_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__resumescore" }
);
resumescore_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.resume_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'resumescore' });
    if (!counter) {
        let newCounter = new Counter({ name: 'resumescore', seq: 2 });
        obj.resume_id = 1;
        await newCounter.save();
        next();
    }
    obj.resume_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("ResumeScore", resumescore_schema);



