
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const intschedule_schema = new Schema(
    {
        intsch_id: {
            type: Number,
            unique: true
        },
        applied_id: {
            type: Number,
            required: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        company_id: {
            type: Number,
            required: true
        },
        mail_title: {
            type: String,
            required: true
        },
        mail_content: {
            type: String,
            required: true
        },
        intsch_status: {
            type: String,
            required: true
        },
        ipaddress: {
            type: String,
            required: true
        },
        mail_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__intschedule" }
);
intschedule_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.intsch_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'intschedule' });
    if (!counter) {
        let newCounter = new Counter({ name: 'intschedule', seq: 2 });
        obj.intsch_id = 1;
        await newCounter.save();
        next();
    }
    obj.intsch_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("Intschedule", intschedule_schema);



