const mongoose = require('mongoose')
const Schema = require('mongoose').Schema

const Counter = require('./counter.modal')

const push_notifySchema = new Schema(
    {
        notify_id: {
            type: Number,
            unique: true
        },
        notify_title: {
            type: String,
            required: true
        },
        notify_msg: {
            type: String,
            required: true
        },
        seeker_id: {
            type: String,
            default: null,
            required: false
        },
        employer_id: {
            type: String,
            default: null,
            required: false
        },
        user_status: {
            type: String,
            enum: ['A', 'S', 'E'],
            default: 'A',
            required: true
        },
        notify_status: {
            type: String,
            required: true
        },
        notify_time:{
            type: Date,
            required: true
        },
        created_date: {
            type: Date,
            required: true
        },
        last_update: {
            type: Date,
            required: true
        }
    },
    { collection: 'col__push_notify' }
)

push_notifySchema.pre('save', async function (next) {
    var obj = this
    if (obj.notify_id) {
        next()
    }
    let counter = await Counter.findOne({ name: 'push_notify' })
    if (!counter) {
        let newCounter = new Counter({ name: 'push_notify', seq: 2 })
        obj.notify_id = 1
        await newCounter.save()
        next()
    }
    obj.notify_id = counter.seq
    counter.seq = counter.seq + 1
    await counter.save()
    next()
})

module.exports = mongoose.model('push_notify', push_notifySchema)