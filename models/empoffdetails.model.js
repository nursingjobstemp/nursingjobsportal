
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const empoffdetails_schema = new Schema(
    {
        wrk_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        emp_desig: {
            type: String,
            required: true
        },
        emp_org: {
            type: String,
            required: true
        },
        cur_comp: {
            type: String,
            required: true
        },
        exp_yr: {
            type: String,
            required: true
        },
        exp_month: {
            type: String,
            required: true
        },
        exp_yr_to: {
            type: String,
            required: true
        },
        exp_month_to: {
            type: String,
            required: true
        },
        sal_type: {
            type: String,
            required: true
        },
        sal_lakhs: {
            type: String,
            required: true
        },
        sal_thousand: {
            type: String,
            required: true
        },
        emp_detail: {
            type: String,
            required: true
        },
        wrk_status: {
            type: String,
            required: true
        },
        wrk_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__empoffdetails" }
);

empoffdetails_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.wrk_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'empoffdetails' });
    if (!counter) {
        let newCounter = new Counter({ name: 'empoffdetails', seq:15272 });
        obj.wrk_id = 15271;
        await newCounter.save();
        next();
    }
    obj.wrk_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("EmployeeOfficialDetails", empoffdetails_schema);



