//-------------------------- Employer Model Start ------------------------------//

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const employer_schema = new Schema(
   {
      recut_id: {
         type: Number,
         unique: true
      },
      comp_name: {
         type: String,
         required: true
      },
      comp_slug: {
         type: String,
         required: true
      },
      mail_id: {
         type: String,
         required: true
      },
      mobile_no: {
         type: String,
         required: true
      },
      email_otp: {
         type: String,
         required: true
      },
      email_verify: {
         type: String,
         required: true
      },
      mobile_otp: {
         type: String,
         required: true
      },
      mobile_verify: {
         type: String,
         required: true
      },
      comp_logo: {
         type: String,
         required: true
      },
      comp_pass: {
         type: String,
         required: true
      },
      cont_person: {
         type: String,
         required: true
      },
      indust_id: {
         type: Number,
         required: true
      },
      recut_type: {
         type: String,
         required: true
      },
      country_id: {
         type: Number,
         required: true
      },
      state_id: {
         type: Number,
         required: true
      },
      city_id: {
         type: Number,
         required: true
      },
      pincode: {
         type: Number,
         required: true
      },
      emp_strength: {
         type: String,
         required: true
      },
      recut_desc: {
         type: String,
         required: true
      },
      facebook: {
         type: String,
         required: true
      },
      twitter: {
         type: String,
         required: true
      },
      linkedin: {
         type: String,
         required: true
      },
      google: {
         type: String,
         required: true
      },
      recut_status: {
         type: String,
         required: true
      },
      user_type: {
         type: String,
         required: true
      },
      recut_address: {
         type: String,
         required: true
      },
      ipaddress: {
         type: String,
         required: true
      },
      recut_date: {
         type: Date,
         required: true
      },
      lastupdate: {
         type: Date,
         required: true
      }
   },
   { collection: "col__recutcomp" }
);

employer_schema.pre("save", async function (next) {
   var obj = this;
   if (obj.recut_id) {
       next();
   }
   let counter = await Counter.findOne({ name: 'recutcomp' });
   if (!counter) {
       let newCounter = new Counter({ name: 'recutcomp', seq:2 });
       obj.recut_id = 1;
       await newCounter.save();
       next();
   }
   obj.recut_id = counter.seq;
   counter.seq = counter.seq + 1;
   await counter.save();
   next();
});
module.exports = mongoose.model("Employer", employer_schema);


//-------------------------- Employer Model End ------------------------------//
