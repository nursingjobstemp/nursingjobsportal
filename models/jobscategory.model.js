const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const jobscategory_schema = new Schema(
    {
        jcat_id: {
            type: Number,
            unique: true
        },
        pid: {
            type: Number,
            required: true
        },
        jcat_name: {
            type: String,
            required: true
        },
        jcat_slug: {
            type: String,
            required: true
        },
        jcat_code: {
            type: String,
            required: true
        },
        jcat_icon: {
            type: String,
            default: null,
            required: false
        },
        jcat_desc: {
            type: String,
            default: null,
            required: false
        },
        jcat_image: {
            type: String,
            default: null,
            required: false
        },
        jcat_pos: {
            type: Number,
            required: true
        },
        jcat_status: {
            type: String,
            required: true
        },
        foot_status: {
            type: String,
            required: true
        },
        jcat_dt: {
            type: Date,
            required: true
        },
        jcat_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__jobscategory" }
);
jobscategory_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.jcat_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'jobscategory' });
    if (!counter) {
        let newCounter = new Counter({ name: 'jobscategory', seq: 2 });
        obj.jcat_id = 1;
        await newCounter.save();
        next();
    }
    obj.jcat_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("JobsCategory", jobscategory_schema);



