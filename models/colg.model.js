const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const college_schema = new Schema(
    {
        colg_id: {
            type: Number,
            unique: true
        },
        colg_name: {
            type: String,
            required: true
        },
        colg_slug: {
            type: String,
            required: true
        },
        colg_pos: {
            type: Number,
            required: true
        },
        colg_status: {
            type: String,
            required: true
        },
        colg_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__college" }
);
college_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.colg_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'college' });
    if (!counter) {
        let newCounter = new Counter({ name: 'college', seq:15272 });
        obj.colg_id = 15271;
        await newCounter.save();
        next();
    }
    obj.colg_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("College", college_schema);
