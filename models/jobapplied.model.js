
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const jobapplied_schema = new Schema(
    {
        applied_id: {
            type: Number,
            unique: true
        },
        emp_id: {
            type: Number,
            required: true
        },
        job_id: {
            type: Number,
            required: true
        },
        company_id: {
            type: Number,
            required: true
        },
        job_type: {
            type: String,
            required: true
        },
        appl_status: {
            type: String,
            required: true
        },
        ipaddress: {
            type: String,
            required: true
        },
        applied_date: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__jobapplied" }
);
jobapplied_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.applied_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'appliedJob' });
    if (!counter) {
        let newCounter = new Counter({ name: 'appliedJob', seq: 2 });
        obj.applied_id = 1;
        await newCounter.save();
        next();
    }
    obj.applied_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("JobApplied", jobapplied_schema);



