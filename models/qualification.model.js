const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const qualification_schema = new Schema(
    {
        qual_id: {
            type: Number,
            unique: true
        },
        qual_name: {
            type: String,
            required: true
        },
        qual_slug: {
            type: String,
            required: true
        },
        qual_status: {
            type: String,
            required: true
        },
        qual_pos: {
            type: Number,
            required: true
        },
        qual_dt: {
            type: Date,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__qualification" }
);

qualification_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.qual_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'qualification' });
    if (!counter) {
        let newCounter = new Counter({ name: 'qualification', seq: 2 });
        obj.qual_id = 1;
        await newCounter.save();
        next();
    }
    obj.qual_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("Qualification", qualification_schema);



