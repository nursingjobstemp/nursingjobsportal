const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const notification_schema = new Schema(
    {
        notify_id: {
            type: Number,
            unique: true
        },
        noti_title: {
            type: String,
            required: true
        },
        noti_msg: {
            type: String,
            required: true
        },
        chat_id: {
            type: Number,
            required: true
        },
        noti_type: {
            type: String,
            required: true
        },
        job_applyid: {
            type: Number,
            required: true
        },
        noti_from: {
            type: Number,
            required: true
        },
        noti_ftype: {
            type: String,
            required: true
        },
        noti_to: {
            type: Number,
            required: true
        },
        noti_date: {
            type: Date,
            default: null,
            required: true
        },
        noti_status: {
            type: String,
            required: true
        },
        noti_read: {
            type: String,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__notification" }
);

notification_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.notify_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'notification' });
    if (!counter) {
        let newCounter = new Counter({ name: 'notification', seq: 2 });
        obj.notify_id = 1;
        await newCounter.save();
        next();
    }
    obj.notify_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("Notification", notification_schema);



