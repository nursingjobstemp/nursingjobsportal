const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const operator_schema = new Schema(
    {
        op_id: {
            type: Number,
            unique: true
        },
        op_type: {
            type: String,
            required: true
        },
        op_name: {
            type: String,
            required: true
        },
        op_uname: {
            type: String,
            required: true
        },
        op_password: {
            type: String,
            required: true
        },
        feat_id: {
            type: String,
            required: true
        },
        op_dt: {
            type: Date,
            default: null,
            required: true
        },
        op_status: {
            type: String,
            required: true
        },
        op_lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__operator" }
);

operator_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.op_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'operator' });
    if (!counter) {
        let newCounter = new Counter({ name: 'operator', seq: 2 });
        obj.op_id = 1;
        await newCounter.save();
        next();
    }
    obj.op_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
});
module.exports = mongoose.model("Operator", operator_schema);



