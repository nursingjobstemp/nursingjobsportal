const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const city_schema = new Schema(
    {
        city_id: {
            type: Number,
            unique: true
        },
        state_id: {
            type: Number,
            required: true
        },
        country_id: {
            type: Number,
            required: true
        },
        city_name: {
            type: String,
            required: true
        },
        city_slug: {
            type: String,
            required: true
        },
        city_code: {
            type: Number,
            required: true
        },
        city_image: {
            type: String,
            required: true
        },
        city_status: {
            type: String,
            required: true
        },
        foot_status: {
            type: String,
            required: true
        },
        lastupdate: {
            type: Date,
            required: true
        }
    },
    { collection: "col__city" }
);

city_schema.pre("save", async function (next) {
    var obj = this;
    if (obj.city_id) {
        next();
    }
    let counter = await Counter.findOne({ name: 'city' });
    if (!counter) {
        let newCounter = new Counter({ name: 'city', seq:15272 });
        obj.city_id = 15271;
        await newCounter.save();
        next();
    }
    obj.city_id = counter.seq;
    counter.seq = counter.seq + 1;
    await counter.save();
    next();
 });
module.exports = mongoose.model("City", city_schema);
