//-------------------------- IndustryType Model Start ------------------------------//

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Counter = require('./counter.modal');

const industry_type_schema = new Schema(
   {
      indust_id: {
         type: Number,
         unique: true
      },
      indust_name: {
         type: String,
         required: true
      },
      indust_slug: {
         type: String,
         required: true
      },
      indust_pos: {
         type: Number,
         required: true
      },
      indust_status: {
         type: String,
         required: true
      },
      indust_date: {
         type: Date,
         required: true
      },
      lastupdate: {
         type: Date,
         required: true
      }
   },

   { collection: "col__industrytype" }
);

industry_type_schema.pre("save", async function (next) {
   var obj = this;
   if (obj.intsch_id) {
       next();
   }
   let counter = await Counter.findOne({ name: 'industrytype' });
   if (!counter) {
       let newCounter = new Counter({ name: 'industrytype', seq: 2 });
       obj.intsch_id = 1;
       await newCounter.save();
       next();
   }
   obj.intsch_id = counter.seq;
   counter.seq = counter.seq + 1;
   await counter.save();
   next();
});
module.exports = mongoose.model("IndustryType", industry_type_schema);

//-------------------------- IndustryType Model End ------------------------------//
